<?php get_header( 'interactive' ); ?>

<?php if( is_dev_mode() ) : ?>
	<div id="field-placeholder"></div>
<?php else: ?>
	<canvas class="webgl" id="body"></canvas>
 	<div id="modal" class="modal" style="opacity: 0; transform: translateX(-400px);"></div>
<?php endif; ?>

<?php get_footer( 'interactive' ); ?>