<div class="modal-stand modal modal-dialog-scrollable fade show %CLASS%" id="standModal" data-bs-keyboard="true" data-bs-backdrop="false" data-bs-focus="true" tabindex="-1" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-header-content container">
					<div class="row">
						<div class="column col-8 col-lg-9 ifield-stand-modal-title-container">
							<h3 class="modal-title">%TITLE%</h3>
							<h4 class="modal-subtitle">%SUBTITLE%</h4>
						</div>
						<div class="column col-4 col-lg-3 ifield-stand-modal-logo-container">%LOGO%</div>
					</div>
				</div>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="sakataifield modal-body-content">%BODY%</div>				
			</div>
		</div>
	</div>
</div>