<?php if( in_array( 'exhibitor_show_products_gallery', $args['meta']['exhibitor_approved_contents'] ) && true == $args['meta']['exhibitor_show_products_gallery'] ) : ?>
	<div class="row modal-content-products">
		<div class="column col-lg-12">
			<div class="modal-content-container">
				<h2><?php _e( 'Products', 'hello-sakataifield' ); ?></h2>
			</div>
		</div>
		<div class="column col-lg-6">
			<div class="modal-content-container">
				<?php if( in_array( 'exhibitor_products_content', $args['meta']['exhibitor_approved_contents'] ) ) : ?>
					<div class="ifield-stand-products-main-content">
						<h3><?= $args['meta']['exhibitor_products_content'] ?></h3>
					</div>
				<?php endif;?>
				<?php if( ! empty( $args['products'] ) ) : ?>
					<?php $first_product = true; ?>
					<?php foreach( $args['products'] as $product ) : ?>
						<?php $css_class = $first_product == true ? ' active' : ''; ?>
						<?php //if( in_array( 'product_content', $product['meta']['exhibitor_approved_contents'] ) ) : ?>
							<div class="ifield-stand-products-product-content<?= $css_class ?>" data-product-id="<?= $product['ID'] ?>">
								<h3><?= $product['post_title'] ?></h3>
								<?= $product['meta']['product_content'] ?>
							</div>
						<?php //endif; ?>		
						<?php $first_product = false; ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="column col-lg-6">
			<div class="modal-content-container">
				<?php if( ! empty( $args['products'] ) ) : ?>
					<?php 
						$first_product = true; 
						$added_to_wishlist = sakataifield()->wishlist->get_items_ids();
					?>
					<?php foreach( $args['products'] as $product ) : ?>
						<?php //if( in_array( 'product_gallery', $product['meta']['exhibitor_approved_contents'] ) ) : ?>
							<?php 
								$is_new = get_field( 'is_new', $product['ID'] );
								$css_class = $first_product == true ? ' active' : ''; 
								// $wishlist_cssclass = in_array( $product['ID'], $added_to_wishlist ) ? ' on-wishlist' : '';

								$discount = sakataifield_get_exhibitor_discount_for_product( $product['ID'] );

								$on_wishlist  = '';
								$message      = __( 'Add to wishlist', 'sakataifield' );
								$wishlist_ids = sakataifield()->wishlist->get_items_ids();
								if ( in_array( $product['ID'], $wishlist_ids, true ) ) {
									$on_wishlist = ' on-wishlist';
									$message      = __( 'Added to wishlist', 'sakataifield' );
								}

							?>
							<div class="ifield-stand-products-product-slider<?= $css_class ?>" data-product-id="<?= $product['ID'] ?>">
								<h4><?= get_the_title( $product['ID'] ) ?></h4>
								<form class="cart" action="<?php the_permalink( $product['ID'] ); ?>" method="post" enctype='multipart/form-data'>
									<button 
										type="submit" 
										name="add-to-wishlist" 
										value="<?= $product['ID'] ?>" 
										class="add_to_wishlist_button<?php echo $on_wishlist; ?>" 
										aria-label="<?php echo esc_attr( $message ); ?>" 
										data-toggle="tooltip" 
										data-bs-toggle="tooltip" 
										data-placement="left" 
										data-bs-placement="left" 
										title="<?php echo esc_attr( $message ); ?>">
									</button>
								</form>
								<?php if( ! empty( $is_new ) ) : ?>
									<span class="tag-new"><?php _e( 'New', 'hello-sakataifield' ); ?></span>
								<?php endif; ?>
								<?php if( ! empty( $discount['text'] ) ) : ?>
									<span class="tag-discount" data-type="<?= $discount['type'] ?>">
										<?= $discount['text']; ?>
									</span>
								<?php endif; ?>
								<div class="swiper ifield-stand-swiper ifield-stand-product-swiper" data-product-id="<?= $product['ID'] ?>">
									 <div class="swiper-wrapper">
									 	<?php foreach( $product['meta']['product_gallery'] as $slide ) : ?>
									 		<div class="swiper-slide">
									 			<div class="swiper-slide-inner">
									 				<img src="<?= $slide['sizes']['large'] ?>" width="<?= $slide['sizes']['large-width'] ?>" height="<?= $slide['sizes']['large-height'] ?>">
									 			</div>
									 		</div>
									 	<?php endforeach; ?>
									 </div>
									<div class="swiper-button-prev"></div>
					  				<div class="swiper-button-next"></div>
								</div>
							</div>
							<?php $first_product = false; ?>
						<?php //endif; ?>
					<?php endforeach; ?>
					<div class="ifield-stand-other-products">
						<h3><?php _e( 'Browse our products', 'hello-sakataifield' ); ?></h3>
						<div class="swiper ifield-stand-swiper ifield-stand-other-products-swiper" data-swiper='{"slidesPerView":4, "spaceBetween":10, "preventClicks": false, "preventClicksPropagation": false}'>
							<div class="swiper-wrapper">
								<?php foreach( $args['products'] as $product ) : ?>
									 <div class="swiper-slide" data-product-id="<?= $product['ID'] ?>">
									 	<div class="swiper-slide-inner">
									 		<a class="ifield-stand-change-product" href="#" data-exhibitor="<?= $args['ID'] ?>" data-product-id="<?= $product['ID'] ?>"><?= get_the_post_thumbnail( $product['ID'], 'thumbnail' );?></a>
									 	</div>
									 </div>
								<?php endforeach; ?>
							</div>
							<div class="swiper-button-prev"></div>
					  		<div class="swiper-button-next"></div>
						</div>
					</div>
				<?php endif; ?>
				<div class="modal-btn-container">
					<?php 
						if( in_array( 'exhibitor_experts', $args['meta']['exhibitor_approved_contents'] ) ) {

							$customer_data = sakataifield_customer_data();
							
							$whatsapp_text = __( 'My name is %s and I am from %s. I would like more information about your products.', 'hello-sakataifield' );

							$whatsapp_text = sprintf( 
								$whatsapp_text, 
								$customer_data['first_name'],
								$customer_data['city']['formatted']
							); 

							$phone = $args['meta']['exhibitor_experts'][0]['whatsapp'];
							$pic_src = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail']; 
							$pic_width = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail-width']; 
							$pic_height = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail-height'];

							if( ! empty( $phone ) && ! empty( $pic_src ) ) {
								echo ifield_expert_whatsapp( $whatsapp_text, $phone, $pic_src, $pic_width, $pic_height );
							}
							
						}
					?>
				</div>
			</div>
		</div>
	</div>
<?php else:?>
	<div class="row">
		<div class="column col-lg-12">
			<?php _e( 'Nothing to show', 'hello-sakataifield' ) ?>
		</div>
	</div>
<?php endif;?>