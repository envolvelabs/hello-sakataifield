<div class="modal-station modal-station-mini modal-stand-preview modal-stand-preview-placeholder modal modal-dialog-scrollable fade show" id="standModalPreviewPlaceholder" data-bs-keyboard="true" data-bs-backdrop="false" data-bs-focus="true" tabindex="-1" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-body">
				<img src="<?= get_stylesheet_directory_uri() ?>/assets/img/spinner.svg" width="100" height="100" />
			</div>
		</div>
	</div>
</div>