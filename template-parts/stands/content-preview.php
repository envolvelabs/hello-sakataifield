<?php if( in_array( 'exhibitor_init_slider', $args['meta']['exhibitor_approved_contents'] ) && ! empty( $args['meta']['exhibitor_init_slider'] ) ) : ?>
	<div class="swiper ifield-stand-preview-swiper">
		 <div class="swiper-wrapper">
		 	<?php foreach( $args['meta']['exhibitor_init_slider'] as $slide ) : ?>
		 		<div class="swiper-slide">
		 			<img src="<?= $slide['sizes']['large'] ?>" width="<?= $slide['sizes']['large-width'] ?>" height="<?= $slide['sizes']['large-height'] ?>">
		 		</div>
		 	<?php endforeach; ?>
		 </div>
		 <div class="swiper-button-prev"></div>
		<div class="swiper-button-next"></div>
	</div>
<?php endif; ?>
<button type="button" class="button stand-preview-btn" data-stand="<?= $args['ID'] ?>"><?= __( 'Visit our stand', 'hello-sakataifield' ) ?></button>