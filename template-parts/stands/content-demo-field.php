<?php $user_id = get_current_user_id(); ?>

<div class="row modal-content-demo-field">
	<div class="column col-lg-6">
		<div class="modal-content-container">
			<?php if( in_array( 'exhibitor_demo_field_content', $args['meta']['exhibitor_approved_contents'] ) ) : ?>
				<div class="ifield-stand-demo-field-content">
					<?= $args['meta']['exhibitor_demo_field_content'] ?>
				</div>
				<?php if( in_array( 'exhibitor_show_products_gallery', $args['meta']['exhibitor_approved_contents'] ) && true == $args['meta']['exhibitor_show_products_gallery'] ) : ?>
					<a class="ifield-btn ifield-btn-discover-products" data-load-content="products" data-stand="<?= $args['ID']; ?>" href="#discover-products">
						<button class="btn btn-primary" type="button"><?php _e( 'Discover our products', 'hello-sakataifield' ); ?></button>
					</a>
				<?php endif; ?>
				<?php if( ! empty( $args['tvifield'] ) ) : ?>
					<a class="ifield-btn-empty ifield-btn-watch-lectures" href="<?= $args['links']['lectures'] ?>">
						<button class="btn btn-secondary" type="button"><?php _e( 'Watch our lectures', 'hello-sakataifield' ); ?></button>
					</a>
				<?php endif; ?>
			<?php endif;?>
			<?php if( ! empty( $args['tvifield'] ) ) : ?>

				<?php //echo ifield_debug_var( [$args['tvifield'], $user_id] ); ?>

				<div class="demo-field-agenda">

					<h3><?= __( 'Next events', 'hello-sakataifield' ); ?></h3>

					<?php foreach( $args['tvifield'] as $video ) : ?>

						<?php 
							$image = $video['meta']['speecher_thumbnail'];	
							$users = is_array( $video['meta']['users_subscriptions'] ) ? $video['meta']['users_subscriptions'] : []; 
						?>

						<div class="card-agenda">
			                <div class="col-info">
			                    <div class="col-lg-2 col-3 card-agenda-figure">
			                    	<?php if( ! empty( $image ) ) : ?>
			                            <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url( $image['sizes']['medium'] ); ?>"/>
			                    	<?php endif; ?> 
			                    </div>
			                    <div class="col-date-card col-lg-2 col-2">
			                        <h5 class="date-day">
			                        	<?php echo get_the_date( 'd', $video['ID'] ) ?> <br/>
			                            <span class="date-month"><?php echo get_the_date( 'M', $video['ID'] ); ?></span> <br/>
			                            <span class="date-hour"><?php echo get_the_time( 'H', $video['ID'] ); ?>h</span>
			                        </h5>
			                    </div>
			                    <div class="col-lg-8 col-7">
			                        <div class="card-body">
			                            <h5 class="card-title"><?= $video['post_title']; ?></h5>
			                            <p class="card-text">
			                            	<small class="text-muted">
			                            		<?= $video['meta']['speecher_name']; ?>
			                            	</small>
			                            </p>
			                            <div class="col-agendamentos col-agendamentos-large col-lg-4 col-12">
							                <button class="btn-schedule btn-agenda <?= in_array( $user_id, $users ) ? 'cancel' : '' ?>" data-id="<?= $video['ID'] ?>">
							                    <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
							                </button>
							            </div>
			                        </div>
			                    </div>
			                </div>
			                <div class="col-agendamentos col-agendamentos-small col-lg-4 col-12">
				                <button class="btn-schedule btn-agenda <?= in_array( $user_id, $users ) ? 'cancel' : '' ?>" data-id="<?= $video['ID'] ?>">
				                    <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
				                </button>
				            </div>
				        </div>

			        <?php endforeach;?>	

			    </div>

			<?php endif;?>	
		</div>
	</div>
	<div class="column col-lg-6">
		<div class="modal-content-container">
			<?php if( in_array( 'exhibitor_demo_field_gallery', $args['meta']['exhibitor_approved_contents'] ) ) : ?>
				<?php if( ! empty( $args['meta']['exhibitor_demo_field_gallery'] ) ) : ?>
					<div class="ifield-swiper-gallery-container">
						<div class="swiper ifield-swiper-gallery">
							<div class="swiper-wrapper">
								<?php foreach( $args['meta']['exhibitor_demo_field_gallery'] as $row_key => $row ) : ?>
									<div class="swiper-slide">
										<?php if( 'image' == $row['type'] ) : ?>
											<img src="<?= $row['image']['sizes']['large'] ?>" width="<?= $row['image']['sizes']['large-width'] ?>" height="<?= $row['image']['sizes']['large-height'] ?>">
										<?php else: ?>
											
											<?php 
												$video_src = "https://iframe.videodelivery.net/{$row['video']}";
												if( ! empty( $row['image']['sizes']['large'] ) ) {
													$video_src.= '?poster=' . rawurlencode( $row['image']['sizes']['large'] );
												}

												$video_id = "stream-player-{$row_key}";
												$player = "player_{$row_key}";
											?>

											<div class="cs-video">
											  <iframe
											  	id="<?= $video_id ?>"
											    src="<?= $video_src ?>"
											    style="border: none; position: absolute; top: 0; height: 100%; width: 100%"
											    allow="accelerometer; gyroscope; autoplay; encrypted-media; picture-in-picture;"
											    allowfullscreen="true"
											  ></iframe>
											  <script type="text/javascript">
											  	var <?= $player ?> = Stream(document.getElementById( '<?= $video_id ?>' ));
												<?= $player ?>.addEventListener( 'pause', () => {
											    	window.iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
											    	jQuery( '.swiper-button-next, .swiper-button-prev' ).show();
											    	var swiper = jQuery( '.ifield-swiper-gallery' ).get( 0 ).swiper.allowTouchMove = true;
											  	});
											  	<?= $player ?>.addEventListener( 'ended', () => {
											    	window.iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
											    	jQuery( '.swiper-button-next, .swiper-button-prev' ).show();
											    	var swiper = jQuery( '.ifield-swiper-gallery' ).get( 0 ).swiper.allowTouchMove = true;
											  	});
											  	<?= $player ?>.addEventListener( 'play', () => {
											    	var $currentPlaying = jQuery( '#audio-tracks [data-song].playing' );

													if( $currentPlaying.length ) {
														window.iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 0.1, 200 );
													}	

													jQuery( '.swiper-button-next, .swiper-button-prev' ).hide();
													var swiper = jQuery( '.ifield-swiper-gallery' ).get( 0 ).swiper.allowTouchMove = false;
											  	});
											  </script>
											</div>

										<?php endif; ?>		
									</div>
								<?php endforeach; ?>
							</div>
							<div class="swiper-button-next"></div>
		      				<div class="swiper-button-prev"></div>
						</div>
						<div thumbsSlider="" class="swiper ifield-swiper-gallery-thumbs">
							<div class="swiper-wrapper">
								<?php foreach( $args['meta']['exhibitor_demo_field_gallery'] as $row ) : ?>
									<div class="swiper-slide">
										<img src="<?= $row['image']['sizes']['thumbnail'] ?>" width="<?= $row['image']['sizes']['thumbnail-width'] ?>" height="<?= $row['image']['sizes']['thumbnail-height'] ?>">
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				<?php endif; ?>		
			<?php endif; ?>
			<div class="modal-btn-container">
				<?php 
					if( in_array( 'exhibitor_experts', $args['meta']['exhibitor_approved_contents'] ) ) {
						
						$whatsapp_text = __( 'My name is %s and I am from %s. I would like more information about this demonstration field.', 'hello-sakataifield' );

						$whatsapp_text = sprintf( 
							$whatsapp_text, 
							$customer_data['first_name'],
							$customer_data['city']['formatted']
						); 


						$phone = $args['meta']['exhibitor_experts'][0]['whatsapp'];
						$pic_src = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail']; 
						$pic_width = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail-width']; 
						$pic_height = $args['meta']['exhibitor_experts'][0]['pic']['sizes']['thumbnail-height'];

						if( ! empty( $phone ) && ! empty( $pic_src ) ) {
							echo ifield_expert_whatsapp( $whatsapp_text, $phone, $pic_src, $pic_width, $pic_height );
						}
						
					}
				?>
			</div>
		</div>
	</div>
</div>