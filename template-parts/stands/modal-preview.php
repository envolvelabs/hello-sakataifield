<div class="modal-station modal-station-mini modal-stand-preview modal modal-dialog-scrollable fade show %CLASS%" id="standModalPreview" data-bs-keyboard="true" data-bs-backdrop="false" data-bs-focus="true" tabindex="-1" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-header-content container">
					<div class="row">
						<div class="column col-4 ifield-stand-modal-logo-container">%LOGO%</div>
						<div class="column col-8 ifield-stand-modal-title-container">
							<h3 class="modal-title">%TITLE%</h3>
							<h4 class="modal-subtitle">%SUBTITLE%</h4>
						</div>
					</div>
				</div>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="sakataifield modal-body-content">%BODY%</div>				
			</div>
		</div>
	</div>
</div>