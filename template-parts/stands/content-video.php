<div class="row">
	<div class="column col-12">
		<div class="modal-content-container">
			<?php if( in_array( 'exhibitor_stand_video_original', $args['meta']['exhibitor_approved_contents'] ) ) : ?>

				<?php 
					$video_src = "https://iframe.videodelivery.net/{$args['meta']['exhibitor_stand_video']}";
					if( in_array( 'exhibitor_stand_video_cover', $args['meta']['exhibitor_approved_contents'] ) && ! empty( $args['meta']['exhibitor_stand_video_cover'] ) ) {
						$video_src.= '?poster=' . rawurlencode( $args['meta']['exhibitor_stand_video_cover']['sizes']['large']);
					}
					$video_src.= '&autoplay';
				?>

				<div class="cs-video">
				  <iframe
				  	id="stream-player"
				    src="<?= $video_src ?>"
				    style="border: none; position: absolute; top: 0; height: 100%; width: 100%"
				    allow="accelerometer; gyroscope; autoplay; encrypted-media; picture-in-picture;"
				    allowfullscreen="true"
				  ></iframe>
				  <script type="text/javascript">
				  	var player = Stream(document.getElementById( 'stream-player' ));
				  	player.play();
					player.addEventListener( 'pause', () => {
				    	window.iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
				  	});
				  	player.addEventListener( 'ended', () => {
				    	window.iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
				  	});
				  	player.addEventListener( 'play', () => {
				    	var $currentPlaying = jQuery( '#audio-tracks [data-song].playing' );

						if( $currentPlaying.length ) {
							window.iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 0.1, 200 );
						}	
				  	});
				  	jQuery( '#standModal' ).on( 'hidden.bs.modal', function (e) {
						player.pause();
					});
				  </script>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>