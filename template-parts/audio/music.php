<div id="audio-tracks">
	<audio data-song="ifield-1" loop preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/audio/forest.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="ifield-2" loop preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/audio/worship-music-contemporary-christian-pop-music-2782.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="ifield-3" loop preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/audio/dance-around-the-campfire-acoustic-guitar-and-drum-2085.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="tvifield" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/audio/the-epic-trailer-12955.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="notification" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/notification-sound-7062.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="retrieve" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/snd_fragment_retrievewav-14728.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="start" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/start-13691.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="spin" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/mixkit-lighter-wheel-spin-2641.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="spin2" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/mixkit-lighter-wheel-spin-2641-2.mp3" type="audio/mpeg">
	</audio>
	<audio data-song="click" preload="auto">
	  	<source src="<?= get_stylesheet_directory_uri() ?>/assets/sfx/mixkit-mouse-click-close-1113.mp3" type="audio/mpeg">
	</audio>
</div>