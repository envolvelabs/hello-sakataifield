<?php /* [TV IFIELD] */?>
<div id="header-tvifield">
    <div class="modal-header">
        <div class="modal-title">
            <h3><?php echo get_the_title();?></h3>
            <p><?php _e( 'Watch now', 'hello-sakataifield' ); ?></p>
        </div>
        <a href="<?php echo get_permalink( apply_filters( 'wpml_object_id', 512, 'page', true ) );?>">
            <button id="btn-close-tvifield" type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </a>
    </div>
</div>

<div class="modal-body-player">

        <div class="row">
            <div class="video-content col-lg-8 col-12">
            <?php

                $wp_query = new WP_Query(
                    array( 
                        'post_type' => 'tvifield', 
                        'posts_per_page' => 1,
                        'post_status' => 'publish',
                        'order_by' =>'publish_date',
                    ) 
                );

                while ( $wp_query->have_posts() ) : $wp_query->the_post(); global $post; ?> 

                    <?php
                        $customer_data = sakataifield_customer_data();
                        $whatsapp_text = __( 'My name is %s and I am from %s, I would like to talk to an expert', 'hello-sakataifield' ); 
                        $whatsapp_text = sprintf( 
                            $whatsapp_text, 
                            $customer_data['first_name'],
                            $customer_data['city']['formatted'],
                        ); 
                        $chat_url = 'https://api.whatsapp.com/send?phone=' . sakataifield_parse_whatsapp( get_field('expert')[0]['whatsapp'] ) . '&text=' . rawurlencode( $whatsapp_text );

                        $current_time = current_time( 'U' );
                        $video_pub_time = get_the_time( 'U', $post->ID );

                        $video_duration = get_field( 'video_duration', $post->ID );
                        $video_duration = $video_duration[0];

                        if( $video_duration['minutes'] + $video_duration['seconds'] <= 0 ){
                            $start_time = 0;    
                        } else {
                            $duration = ( $video_duration['minutes'] * 60 ) + $video_duration['seconds'];
                            $diff = $current_time - $video_pub_time;

                            if( $diff >= $duration ) {
                                $start_time = 0;
                            } else {
                                $start_time = $diff;
                            }
                        }

                        $exhibitor_id = get_field('exhibitor_id');

                        ifield_add_report_entry([
                            'action' => 'view',
                            'object_type' => 'tvifield',
                            'object_ID' => $post->ID,
                            'entity' => empty( $exhibitor_id ) ? 'sakata' : 'exhibitor',
                            'entity_ID' => empty( $exhibitor_id ) ? 0 : $exhibitor_id
                        ]);
                        
                    ?>

                    <div class="video-header">
                        <h4 class="video-title"><?php _e( 'On view now', 'hello-sakataifield' ); ?>:</h4>
                        <p class="palestrante-title" data-js="palestrante-title"><?php the_field('speecher_name');?></p>
    
                    </div>
                    <div id="video-player" data-entity="<?= empty( $exhibitor_id ) ? 'sakata' : 'exhibitor' ?>" data-entity-id="<?= empty( $exhibitor_id ) ? 0 : $exhibitor_id ?>" class="video-player embed-responsive embed-responsive-16by9" data-video-id="<?= $post->ID ?>" data-time="<?= the_time( 'Y-m-d H:i:s' ) ?>">
                        <iframe
                            id="stream-player"
                            class="embed-responsive-item"
                            src="<?= 'https://iframe.videodelivery.net/'. get_field('video_url'); ?>?startTime=<?= $start_time ?>&preload&controls&autoplay"
                            style="border: none"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen="true"
                            data-js="video-iframe"
                        ></iframe>
                        <script type="text/javascript">

                            const script = document.createElement("script");
                            script.src = "https://embed.videodelivery.net/embed/sdk.latest.js";
                            script.addEventListener( "load", () => {
                                const player = Stream( document.getElementById( 'stream-player' ) );
                                player.volume = 1;
                                player.addEventListener( 'play', () => {
                                    jQuery( '.content-box-agenda' ).on( 'click', function(){
                                        player.pause();
                                    });
                                    window.addReportEntry({
                                        "object_type" : 'tvifield_play',
                                        "object_ID" : jQuery( '#video-player' ).data( 'video-id' ),
                                        "entity" : jQuery( '#video-player' ).data( 'entity' ),
                                        "entity_ID" : jQuery( '#video-player' ).data( 'entity-id' )
                                    }, 1 );
                                });
                                player.addEventListener( 'ended', () => {
                                    window.addReportEntry({
                                        "object_type" : 'tvifield_ended',
                                        "object_ID" : jQuery( '#video-player' ).data( 'video-id' ),
                                        "entity" : jQuery( '#video-player' ).data( 'entity' ),
                                        "entity_ID" : jQuery( '#video-player' ).data( 'entity-id' )
                                    }, 1 );
                                });
                            });
                            document.head.appendChild( script );

                        </script>
                    </div>
                
                    <div class="content-box-agenda">
                        <img class="img-agenda" alt="icone agenda" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/agenda.png" />
                        <h5 id="btn-agenda" class="agenda-title">
                            <?php _e( 'Click to see the schedule of upcoming lectures', 'hello-sakataifield' ); ?>
                        </h5>
                    </div>
                    <div class="content-box-call-expert">
                        <div class="expert-content col-lg-6 col-xl-6 col-12">
                            <h5 class="expert-title"><?php _e( 'Are you still have questions?', 'hello-sakataifield' ); ?></h5>
                            <p class="expert-description">
                                <?php _e( 'Talk to an expert now', 'hello-sakataifield' ); ?>
                            </p>
                        </div>
                        <?php if( have_rows('expert') ): ?>
                            <?php while( have_rows('expert') ): the_row(); ?>
                            <a class="btn-call-expert col-lg-6 col-xl-6 col-12" target="_blank" id="link-whatsapp" data-js="whatsapp-expert" href="<?= $chat_url ?>">
                                <img
                                    class="img-expert"
                                    alt="Foto especialista"
                                    src=" <?=  get_field('expert')[0]['pic']['url'] ?>"
                                    data-js="img-expert"
                                />
                               
                                <h5 class="name-expert" data-js="name-expert"><?php the_sub_field('name');?></h5>
                            </a>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
               
            </div>

            <div class="all-video-list col-lg-4 col-12">
                <div class="video-list-header">
                    <h4 class="video-list-title"><?php _e( 'Previous episodes', 'hello-sakataifield' ); ?></h4>
                </div>
                
                <div class="video-list">
                    <?php if( isset( $_GET['filter_exhibitor'] ) ) : ?>
                        <?php 
                            $exhibitor_id = apply_filters( 'wpml_object_id', $_GET['filter_exhibitor'], 'exhibitor', true  );
                        ?>
                        <a href="#" class="tvifield-remove-filters">
                            <?= __( 'Filtering by', 'hello-sakataifield' ); ?>:
                            <?= get_the_title( $exhibitor_id ); ?> <span>× <?php _e( 'Remove filter', 'hello-sakataifield' ); ?></span>
                        </a>
                    <?php endif; ?>
                    <?php

                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

                    $args = array(
                        'post_type' => 'tvifield',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'paged' => $paged,
                    );

                    // if( isset( $_GET['filter_exhibitor'] ) ) {
                    //     $args['meta_key'] = 'exhibitor_id';
                    //     $args['meta_value'] = absint( $_GET['filter_exhibitor'] );
                    // }

                    $wp_query = new WP_Query( $args );

                    
                    while ( $wp_query->have_posts() ) : $wp_query->the_post(); global $post; ?>


                        <?php 
                            $customer_data = sakataifield_customer_data();
                            $whatsapp_text = __( 'My name is %s and I am from %s, I would like to talk to an expert', 'hello-sakataifield' ); 
                            $whatsapp_text = sprintf( 
                                $whatsapp_text, 
                                $customer_data['first_name'],
                                $customer_data['city']['formatted'],
                            ); 
                            $chat_url = 'https://api.whatsapp.com/send?phone=' . sakataifield_parse_whatsapp( get_field('expert')[0]['whatsapp'] ) . '&text=' . rawurlencode( $whatsapp_text );
                            $exhibitor_id = get_field('exhibitor_id');
                            $filter_class = isset( $_GET['filter_exhibitor'] ) && $_GET['filter_exhibitor'] == $exhibitor_id ? '' : ' hide';
                            $filter_class = isset( $_GET['filter_exhibitor'] ) ? $filter_class : '';

                        ?>

                        <a
                            href="#"
                            class="video-item text-dark<?= $filter_class ?>"
                            style="text-decoration: none;"
                            data-speaker="<?= get_field('speecher_name') ?>"
                            data-time="<?= the_time( 'Y-m-d H:i:s' ) ?>"
                            data-video="<?= get_field('video_url') ?>"
                            data-video_id="<?php the_ID() ?>"
                            data-speecher-name="<?= get_field('expert')[0]['name'] ?>"
                            data-speecher-whatsapp="<?= $chat_url ?>"
                            data-speecher-picture="<?= get_field('expert')[0]['pic']['url'] ?>"
                            data-js="link-video"
                            data-exhibitor=<?= $exhibitor_id ?>
                        >

                            <div class="video-box">
                                <img class="img-video-thumb" src="<?php the_post_thumbnail_url(); ?>"/>
                                <div class="content-video-list">
                                    <h4 class="video-list-name">
                                        <?php the_title(); ?>
                                    </h4>
                                    <p class="palestrante-name">
                                        <?php the_field('speecher_name'); ?>
                                    </p>
                                </div>

                            </div>
                        </a>
                    <?php endwhile;?>
                    
                </div>
                    
                <?php wp_reset_query();?>
            </div>
        </div>
    </div>
</div>
<?php/* [/TV IFIELD] */?>