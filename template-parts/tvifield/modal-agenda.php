<?php /* [TV IFIELD] */?> 

<div id="header-tvifield" class="">
    <div class="modal-header">
        <div class="modal-title">
            <h3><?php _e( 'Agenda', 'hello-sakataifield' ); ?></h3>
            <p><?php _e( 'Stay on top of the schedule', 'hello-sakataifield' ); ?></p>
        </div>
        <button id="btn-close-agenda" type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
<div class="modal-body-agenda">
    <div class="container">
        <div class="row">
            <div class="content-agenda col-12">
                <div class="agenda-header">
                    <img class="img-icon-agenda" alt="icone agenda" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/agenda.png" />
                    <div class="title-agenda">
                        <h4><?php _e( 'Agenda', 'hello-sakataifield' ); ?></h4>
                        <p><?php _e( 'Stay on top of the schedule', 'hello-sakataifield' ); ?></p>
                    </div>
                </div>
                <div class="list-events">
                    <h4 class="title-list-events"><?php _e( 'Next events', 'hello-sakataifield' ); ?></h4>
                    <div class="row">
                        <div class="col-12">
                            <?php if( isset( $_GET['filter_exhibitor'] ) ) : ?>
                                <?php 
                                    $exhibitor_id = apply_filters( 'wpml_object_id', $_GET['filter_exhibitor'], 'exhibitor', true  );
                                ?>
                                <a href="#" class="tvifield-remove-filters" data-source="agenda">
                                    <?= __( 'Filtering by', 'hello-sakataifield' ); ?>:
                                    <?= get_the_title( $exhibitor_id ); ?> <span>× <?php _e( 'Remove filter', 'hello-sakataifield' ); ?></span>
                                </a>
                                <style type="text/css">
                                    .card-agenda[data-exhibitor]{
                                        display: none!important;
                                    }
                                    .card-agenda[data-exhibitor="<?= $exhibitor_id ?>"]{
                                        display: flex!important;
                                    }
                                </style>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php
                    

                        $wp_query = new WP_Query(
                            array( 
                                'post_type' => 'tvifield', 
                                'posts_per_page' => -1,
                                'post_status' => 'future',
                                'order' => 'asc'
                                
                            ) 
                        ); 
                        while ( $wp_query->have_posts() ) : $wp_query->the_post(); global $post; ?>

                            <?php $exhibitor_id = get_field('exhibitor_id'); ?>

                            <div class="card-agenda" data-exhibitor="<?= $exhibitor_id ?>">
                                <div class="col-info">
                                    <div class="col-lg-2 col-3">
                                    <?php 
                                        $image = get_field('speecher_thumbnail');
                                        if( !empty( $image ) ): ?>
                                            <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>"/>
                                    <?php endif; ?> 
                                    </div>
                                    <div class="col-date-card col-lg-2 col-1">
                                        <h5 class="date-day">
                                        <?php echo get_the_date('d', get_the_ID()) ?> <br />
                                            <span class="date-month"><?php echo get_the_date('M', get_the_ID()) ?></span> <br />
                                            <span class="date-hour"><?php echo get_the_time( 'H', $post->ID ); ?>h</span>
                                        </h5>
                                    </div>
                                    <div class="col-lg-8 col-8">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php the_title(); ?></h5>
                                            <p class="card-text"><small class="text-muted"> <?php the_field('speecher_name')?></small></p>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php $user_id = get_current_user_id(); ?>
                            <?php $users = is_array(get_field('users_subscriptions')) ? get_field('users_subscriptions') : []; ?>

                            <div class="col-agendamentos col-lg-4 col-12">
                                <button id="btn-schedule" href="#" class="btn-agenda <?= in_array($user_id, $users) ? 'cancel' : '' ?>" data-id="<?= $post->ID ?>">
                                    <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
                                </button>
                            </div>
                        </div>

                    <?php endwhile; ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php /*  [/TV IFIELD] */?> 