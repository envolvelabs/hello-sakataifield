<div class="foryou-wishlist">
    <h3 class="title-list-interest">
        <?php _e( 'See your interest list', 'hello-sakataifield' ); ?>
    </h3>

    <div class="list-interest">
        <div class="row">
            <div class="col-12">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-8 col-sm-8 col-12">
                            <div class="card-info">
                                <div class="icone-interest">
                                    <img class="icon-heart-cicle" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/heart-red.png" />
                                </div>
                                <h4 class="card-body-title"><?php _e( "Don't forget to submit your list!", 'hello-sakataifield' ); ?></h4>
                            </div>
                        </div>
                        <div class="column-acess-list col-xl-4 col-sm-4 col-12">
                            <button id="btn-list-interest" href="#" class="btn-list-interest">
                                <?php _e( 'Access my list', 'hello-sakataifield' ); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>