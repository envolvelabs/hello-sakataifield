<?php 

$user_id = get_current_user_id(); 

$customer_data = sakataifield_customer_data();

?>

<div class="foryou-interest-lectures">

	<h3 class="title-list-speeches">
	    <?php _e( 'Lectures that might interest you', 'hello-sakataifield' ); ?>
	</h3>

	<div class="list-scheduling">
		
        <?php 

            $min_posts = 2;
            $post__not_in = [];

            $args =[
                'post_type' => 'tvifield', 
                'posts_per_page' => $min_posts,
                'post_status' => 'future', 
                'order' => 'asc'
            ];

            if( ! empty( $customer_data['species_of_interest'] ) ) {

                $args['meta_query'] = [];
                $args['meta_query']['relation'] = 'OR';

                foreach( $customer_data['species_of_interest'] as $term_id ) {

                    $args['meta_query'][] = [
                        'key'  => 'tax',
                        'value'     => "\"" . $term_id . "\"",
                        'compare'     => 'LIKE', 
                    ];

                }
                
            }   

            $wp_query = new WP_Query( $args ); 

        ?>
        
        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

            <?php 
                $users_subscriptions = get_field( 'users_subscriptions' );
                $users = is_array( $users_subscriptions ) ? $users_subscriptions : []; 
                $post__not_in[] = $post->ID;
            ?>

            <div class="card-agenda" data-id="<?= $post->ID ?>">
                <div class="col-info">
                    <div class="col-xl-2 col-sm-2 col-2">
                        <?php $image = get_field( 'speecher_thumbnail' ); ?>
                        <?php if( !empty( $image ) ): ?>
                            <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="col-date-card col-xl-2 col-sm-1 col-1">
                        <h5 class="date-day">
                            <?php echo get_the_date('d', get_the_ID()) ?><br/>
                            <span class="date-month"><?php echo get_the_date( 'M', get_the_ID() ) ?></span> <br/>
                            <span class="date-hour"><?php echo get_the_time( 'h', $post->ID ); ?>h</span>
                        </h5>
                    </div>
                    <div class="col-xl-8 col-sm-4 col-8">
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <p class="card-text">
                                <small class="text-muted"> <?php the_field( 'speecher_name' )?></small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-agendamentos col-xl-4 col-sm-6 col-12">
                    <button class="btn-schedule btn-agenda <?= in_array( $user_id, $users ) ? 'cancel' : '' ?>" data-id="<?= $post->ID ?>">
                        <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
                    </button>
                </div>
          </div>

        <?php endwhile; ?>
        <?php wp_reset_query();?>

        <?php 

            if( $wp_query->post_count < $min_posts ) {
                
                $args =[
                    'post_type' => 'tvifield', 
                    'posts_per_page' => -1,
                    'post_status' => 'future', 
                    'order' => 'asc',
                    'post__not_in' => $post__not_in
                ];

                $wp_query = new WP_Query( $args ); 
                
            }

        ?>

        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

            <?php 
                $users_subscriptions = get_field( 'users_subscriptions' );
                $users = is_array( $users_subscriptions ) ? $users_subscriptions : []; 

                if( in_array( $user_id, $users ) ) {
                    continue;
                }
            ?>

            <div class="card-agenda" data-id="<?= $post->ID ?>">
                <div class="col-info">
                    <div class="col-xl-2 col-sm-2 col-2">
                        <?php $image = get_field( 'speecher_thumbnail' ); ?>
                        <?php if( !empty( $image ) ): ?>
                            <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="col-date-card col-xl-2 col-sm-1 col-1">
                        <h5 class="date-day">
                            <?php echo get_the_date('d', get_the_ID()) ?><br/>
                            <span class="date-month"><?php echo get_the_date( 'M', get_the_ID() ) ?></span> <br/>
                            <span class="date-hour"><?php echo get_the_time( 'h', $post->ID ); ?>h</span>
                        </h5>
                    </div>
                    <div class="col-xl-8 col-sm-4 col-8">
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <p class="card-text">
                                <small class="text-muted"> <?php the_field( 'speecher_name' )?></small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-agendamentos col-xl-4 col-sm-6 col-12">
                    <button class="btn-schedule btn-agenda <?= in_array( $user_id, $users ) ? 'cancel' : '' ?>" data-id="<?= $post->ID ?>">
                        <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
                    </button>
                </div>
          </div>

          <?php break; ?>

        <?php endwhile; ?>
        <?php wp_reset_query();?>

    </div>

</div>