<div class="modal-dialog-scrollable fade show" id="foryouModal" data-bs-keyboard="true" data-bs-backdrop="false" data-bs-focus="true" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <div class="img-header-foryou">
                    <img class="icon-person" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/person.png" />
                </div>
                <div class="modal-title">
                    <h3><?php _e( 'For you', 'hello-sakataifield' ); ?></h3>
                    <p><?php _e( "See what we've got especially for you", 'hello-sakataifield' ); ?></p>
                </div>
                <button id="btn-close-foryou" onclick="location.href='<?php echo get_home_url();?>'" type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="content-foryou col-xl-8 col-12">
                            <h3 class="title-list-products">
                                <?php _e( 'Check out some products of your interest with special conditions', 'hello-sakataifield' ); ?>
                            </h3>
                            <div class="list-products">
                                <?php 

                                    $wp_query = new WP_Query( array( 
                                        'post_type' =>'product', 
                                        'posts_per_page' => 3, 
                                        'tax_query' => [
                                            [
                                            'taxonomy'  => 'product_cat',
                                            'field'     => 'term_id',
                                            'terms'     => [], 
                                            ]
                                        ],
                                        'post_status' => 'publish',
                                    )); 

                                ?>

                                <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                                    <div class="card-product">

                                        <div class="col-xl-2 col-sm-2 col-2">
                                            <img class="img-product-list" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" />
                                        </div>

                                        <div class="container">
                                            <div class="row">
                                                <div class="column-title-product col-xl-6 col-sm-6 col-12">
                                                    <div class="content-product">
                                                    <h4 class="title-product"><?php the_title();?></h4>
                                                    <small class="author-product"><?php the_title();?></small>
                                                    </div>
                                                </div>

                                                <div class="column-discount col-xl-6 col-sm-6 col-12">
                                                    <div class="discount-product">
                                                      <img class="icon-discount" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/discount.png" />
                                                      <a href="" class="discount-text">10% de desconto</a>
                                                    </div>
                                                </div>
                                          </div>

                                        </div>
                                        <div class="column-btn-send col-xl-2 col-sm-2 col-3">
                                            <img class="btn-send" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/heart-round.png" />
                                        </div>
                                    </div>

                                <?php endwhile; ?>

                                <?php wp_reset_query();?>

                            </div>

                            <h3 class="title-list-interest">
                                <?php _e( 'See your interest list', 'hello-sakataifield' ); ?>
                            </h3>

                            <div class="list-interest">
                                <div class="card-body">
                                    <div class="col-xl-6 col-sm-8 col-12">
                                        <div class="card-info">
                                            <div class="icone-interest">
                                                <img class="icon-heart-cicle" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/heart-red.png" />
                                            </div>
                                            <h4 class="card-body-title"><?php _e( "Don't forget to submit your list!", 'hello-sakataifield' ); ?></h4>
                                        </div>
                                    </div>
                                    <div class="column-acess-list col-xl-6 col-sm-8 col-12">
                                        <button id="btn-list-interest" href="#" class="btn-list-interest">
                                            <?php _e( 'Access my list', 'hello-sakataifield' ); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <h3 class="title-list-speeches">
                                <?php _e( 'Lectures that might interest you', 'hello-sakataifield' ); ?>
                            </h3>

                            <div class="list-scheduling">
                                <?php 

                                    $wp_query = new WP_Query( array(
                                        'post_type' => 'tvifield', 
                                        'posts_per_page' => 2,
                                        'post_status' => 'future', 
                                    )); 
                                ?>
                                
                                <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                                    <div class="card-agenda">
                                        <div class="col-info">
                                            <div class="col-xl-2 col-sm-2 col-2">
                                                <?php $image = get_field( 'speecher_thumbnail' ); ?>
                                                <?php if( !empty( $image ) ): ?>
                                                    <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>" />
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-date-card col-xl-2 col-sm-1 col-1">
                                                <h5 class="date-day">
                                                    <?php echo get_the_date('d', get_the_ID()) ?><br/>
                                                    <span class="date-month"><?php echo get_the_date( 'M', get_the_ID() ) ?></span> <br/>
                                                    <span class="date-hour"><?php echo get_the_time( 'h', $post->ID ); ?>h</span>
                                                </h5>
                                            </div>
                                            <div class="col-xl-8 col-sm-4 col-8">
                                                <div class="card-body">
                                                    <h5 class="card-title"><?php the_title(); ?></h5>
                                                    <p class="card-text">
                                                        <small class="text-muted"> <?php the_field( 'speecher_name' )?></small>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-agendamentos col-xl-4 col-sm-6 col-12">
                                            <button id="btn-schedule" href="#" class="btn-agenda">
                                                <?php _e( 'Let me know when to start', 'hello-sakataifield' ); ?>
                                            </button>
                                        </div>
                                  </div>

                                <?php endwhile; ?>
                                <?php wp_reset_query();?>
                            </div>
                        </div>

                        <div class="content-foryou check-product col-xl-4 col-12">
                            <div class="more-info-product">
                                <h4 class="title-products-info">
                                    <?php _e( 'Check out more information about our products', 'hello-sakataifield' ); ?>
                                </h4>

                                <div class="row modules-slider">

                                    <?php 

                                        $wp_query = new WP_Query(array( 
                                            'post_type' =>'product', 
                                            'posts_per_page' => 10, 
                                            'post_status' => 'publish',
                                        )); 

                                    ?>
                                    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                                        <div class="col-xl-12 col-12">
                                            <div class="product-modules">
                                                <div class="header-product">
                                                    <h4 class="title-product"><?php the_title();?></h4>
                                                </div>
                                                <div class="body-product">
                                                    <img class="img-product-list" onclick="location.href='<?php echo get_permalink();?>'" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" />
                                                </div>
                                                <div class="btn-product-list">
                                                    <button id="btn-product" onclick="location.href='<?php echo get_permalink();?>'" href="" class="btn-product"><?php _e( 'Learn more about this product', 'hello-sakataifield' ); ?></button>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endwhile; ?>
                                    <?php wp_reset_query();?>

                                </div>
                            </div>

                            <div class="scheduled-lectures">
                                <h4 class="title-schedules-lectures">
                                    <?php _e( 'Scheduled lectures:', 'hello-sakataifield' ); ?>
                                </h4>
                                <div class="scheduled-list">

                                    <?php
                                        $wp_query = new WP_Query( array( 
                                            'post_type' =>'tvifield', 
                                            'posts_per_page' => 2, 
                                            'post_status' => 'future', 
                                        ));
                                    ?>

                                    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                                        <div class="card-agenda">
                                            
                                            <div class="col-info">
                                                <div class="col-xl-2 col-2">
                                                    <?php $image = get_field('speecher_thumbnail'); ?>
                                                    <?php if( !empty( $image ) ): ?>
                                                        <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>" />
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col-date-card col-xl-2 col-1">
                                                    <h5 class="date-day">
                                                        <?php echo get_the_date('d', get_the_ID()) ?><br />
                                                        <span class="date-month"><?php echo get_the_date('M', get_the_ID()) ?></span> <br />
                                                        <span class="date-hour"><?php echo get_the_time( 'h', $post->ID ); ?>h</span>
                                                    </h5>
                                                </div>
                                                <div class="col-xl-8 col-8">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><?php the_title(); ?></h5>
                                                        <p class="card-text">
                                                            <small class="text-muted"> <?php the_field('speecher_name')?></small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-agendamentos col-xl-4 col-12">
                                                <button id="btn-schedule" href="#" class="btn-agenda"><?php _e( 'Let me know when to start', 'hello-sakataifield' ); ?></button>
                                            </div>
                                            
                                        </div>

                                    <?php endwhile; ?>
                                    <?php wp_reset_query();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
?>