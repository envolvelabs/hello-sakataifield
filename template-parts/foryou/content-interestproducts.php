<div class="more-info-product">
    <h4 class="title-products-info">
        <?php _e( 'Check out more information about our products', 'hello-sakataifield' ); ?>
    </h4>

    <div class="modules-slider">

        <?php   

            global $exclude_products;

            $products = [];
            $posts_per_page = 7;

            if( empty( $exclude_products ) ) {
                $exclude_products = [];
            }

            $customer_data = sakataifield_customer_data();

            $args = [
                'post_type' =>'product', 
                'posts_per_page' => $posts_per_page,
                'post_status' => 'publish',
                'suppress_filters' => false,
                'order' => 'asc',
                'orderby' => 'rand',
                'post__not_in' => $exclude_products,
                'meta_query' => [
                    [
                        'key' => 'exhibitor',
                        'value' => ''
                    ]
                ]
            ];

            if( ! empty( $customer_data['species_of_interest'] ) ) {

                $args['tax_query'] = [
                    [
                        'taxonomy'  => 'product_cat',
                        'field'     => 'term_id',
                        'terms'     => $customer_data['species_of_interest'], 
                    ]
                ];
            }   

            $posts = get_posts( $args ); 

            if( ! empty( $posts ) ) {

                $products = array_merge( $products, $posts );

            }


            if( count( $products ) < $posts_per_page ) {

                $exclude_products = array_merge( $exclude_products, wp_list_pluck( $posts, 'ID' ) );

                unset( $args['tax_query'] );
                $args['posts_per_page'] = $posts_per_page - count( $posts );
                $args['post__not_in'] = $exclude_products;

                $posts = get_posts( $args );

                if( ! empty( $posts ) ) {

                    $products = array_merge( $products, $posts );

                }

            }
            

        ?>

        <div class="swiper ifield-foryou-slider" data-swiper='{"slidesPerView":1, "spaceBetween":0, "preventClicks": false, "preventClicksPropagation": false}'>
            <div class="swiper-wrapper">

                <?php if( ! empty( $products ) ) : ?>

                    <?php foreach( $products as $product ) : ?>

                        <?php $product_link = sakataifield_get_product_link( $product->ID ); ?>

                        <div class="swiper-slide">
                            <div class="product-modules clicable-product" data-product-link="<?= $product_link; ?>">
                                <div class="header-product">
                                    <h4 class="title-product"><?= get_the_title( $product->ID );?></h4>
                                </div>
                                <div class="body-product">
                                    <img class="img-product-list" src="<?= get_the_post_thumbnail_url( $product, 'large' ); ?>" alt="<?= get_the_title( $product->ID );?>" />
                                </div>
                                <div class="btn-product-list">
                                    <button class="btn-product">
                                        <?php _e( 'Learn more about this product', 'hello-sakataifield' ); ?>
                                    </button>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; ?>

                <?php endif; ?>

            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>

        <script type="text/javascript">
            ( function( $ ){
                $( window ).on( 'load', function() {
                    var $swiperContainer = $( '.ifield-foryou-slider' );
                    var swiperConfig = typeof $swiperContainer.data( 'swiper' ) === 'undefined' ? {} : $swiperContainer.data( 'swiper' );
                    var swiperDefaults = {
                        navigation: {
                            nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
                            prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
                        }
                    };

                    var swiper = new Swiper( $swiperContainer.get( 0 ), {...swiperConfig, ...swiperDefaults } );
                });
            })( jQuery );
        </script>

    </div>
</div>