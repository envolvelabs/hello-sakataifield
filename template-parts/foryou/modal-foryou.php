<div class="modal-foryou modal fade" id="foryouModal" data-bs-keyboard="true" data-bs-backdrop="true" data-bs-focus="true" tabindex="-1" aria-modal="true" role="dialog">
	<div class="modal-dialog modal-xl">
		<div class="modal-content">
			<div class="modal-header">
				<div class="img-header-foryou">
                    <img class="icon-person" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/person.png" />
                </div>
                <div class="modal-title">
                    <h3><?= __( 'For you', 'hello-sakataifield' ); ?></h3>
                    <p><?= __( "See what we've got especially for you", 'hello-sakataifield' ); ?></p>
                </div>
                <button id="btn-close-foryou" type="button" class="close btn-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
			</div>
			<div class="modal-body">
				<div class="sakataifield modal-body-content">
					<div class="container">
	                    <div class="row">
	                        <div class="content-foryou col-xl-8 col-12">
	                        	<?php get_template_part( 'template-parts/foryou/content', 'listproducts' ); ?>
	                        	<?php get_template_part( 'template-parts/foryou/content', 'wishlist' ); ?>
	                        </div>
	                        <div class="content-foryou check-product col-xl-4 col-12">
	                        	<?php get_template_part( 'template-parts/foryou/content', 'interestproducts' ); ?>
	                        </div>
                       	</div>
                       	<div class="row">
	                        <div class="content-foryou col-xl-8 col-12">
	                        	<?php get_template_part( 'template-parts/foryou/content', 'interestlectures' ); ?>
	                        </div>
	                        <div class="content-foryou check-product col-xl-4 col-12">
	                        	<?php get_template_part( 'template-parts/foryou/content', 'subscriptedlectures' ); ?>
	                        </div>
                       	</div>
                    </div>
				</div>				
			</div>
		</div>
	</div>
</div>