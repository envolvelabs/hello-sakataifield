<?php $user_id = get_current_user_id(); ?>

<?php
    $wp_query = new WP_Query( array( 
        'post_type' =>'tvifield', 
        'posts_per_page' => -1, 
        'post_status' => 'future', 
        'order' => 'asc'
    ));
?>

<div class="scheduled-lectures">
    <h4 class="title-schedules-lectures">
        <?php _e( 'Scheduled lectures', 'hello-sakataifield' ); ?>
    </h4>
    <div class="scheduled-list">

        <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

            <?php 
                $users_subscriptions = get_field( 'users_subscriptions' );
                $users = is_array( $users_subscriptions ) ? $users_subscriptions : []; 

                if( in_array( $user_id, $users ) ) {
                    $sub_class = ' subscribed';
                } else {
                    $sub_class = '';
                }
            ?>

            <div class="card-agenda<?= $sub_class; ?>" data-id="<?= $post->ID ?>">
                
                <div class="col-info">
                    <div class="col-xl-2 col-2">
                        <?php $image = get_field('speecher_thumbnail'); ?>
                        <?php if( !empty( $image ) ): ?>
                            <img class="img-thumb-agenda img-fluid rounded-center" src="<?php echo esc_url($image['sizes']['medium']); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="col-date-card col-xl-2 col-1">
                        <h5 class="date-day">
                            <?php echo get_the_date('d', get_the_ID()) ?><br />
                            <span class="date-month"><?php echo get_the_date('M', get_the_ID()) ?></span> <br />
                            <span class="date-hour"><?php echo get_the_time( 'h', $post->ID ); ?>h</span>
                        </h5>
                    </div>
                    <div class="col-xl-8 col-8">
                        <div class="card-body">
                            <h5 class="card-title"><?php the_title(); ?></h5>
                            <p class="card-text">
                                <small class="text-muted"> <?php the_field('speecher_name')?></small>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-agendamentos col-12">
                   <button class="btn-schedule btn-agenda <?= in_array( $user_id, $users ) ? 'cancel' : '' ?>" data-id="<?= $post->ID ?>">
                        <?= in_array( $user_id, $users ) ? __( 'Cancel scheduling', 'hello-sakataifield' ) : __( 'Let me know when to start', 'hello-sakataifield' ) ?>
                    </button>
                </div>
                
            </div>

        <?php endwhile; ?>
        <?php wp_reset_query();?>
    </div>
</div>
<script type="text/javascript">
    ( function( $ ){

        if( $( '.scheduled-lectures .card-agenda.subscribed' ).length >= 1 ) {
            $( '.scheduled-lectures' ).addClass( 'has-subscriptions' );
        }

    })( jQuery );
</script>