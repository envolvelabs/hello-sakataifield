<div class="foryou-list-products">
	<h3 class="title-list-products">
	    <?php _e( 'Check out some products of your interest with special conditions', 'hello-sakataifield' ); ?>
	</h3>
	<div class="list-products">
	    <?php 

	    	global $exclude_products;

	    	$exclude_products = [];

	    	$added_to_wishlist = sakataifield()->wishlist->get_items_ids();
	    	$customer_data = sakataifield_customer_data();

	    	$args = [
	            'post_type' =>'product', 
	            'posts_per_page' => 3,
	            'post_status' => 'publish',
	            'suppress_filters' => false,
	            'order' => 'asc',
	            'orderby' => 'rand',
	            'meta_query' => [
	            	[
		            	'key' => 'exhibitor',
		            	'value' => ''
		            ]
	            ]
	        ];

	        if( ! empty( $customer_data['species_of_interest'] ) ) {

	            $args['tax_query'] = [
	                [
		                'taxonomy'  => 'product_cat',
		                'field'     => 'term_id',
		                'terms'     => $customer_data['species_of_interest'], 
	                ]
	            ];
	        }	

	        $wp_query = new WP_Query( $args ); 

	    ?>

	    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

	    	<?php 

	    		$exclude_products[] = $post->ID;

	    		$promotional = sakataifield_get_customer_discount_for_product( $post->ID );

	    		$on_wishlist  = '';
				$message      = __( 'Add to wishlist', 'sakataifield' );
				$wishlist_ids = sakataifield()->wishlist->get_items_ids();
				if ( in_array( $product['ID'], $wishlist_ids, true ) ) {
					$on_wishlist = ' on-wishlist';
					$message      = __( 'Added to wishlist', 'sakataifield' );
				}

				$product_link = sakataifield_get_product_link( $post->ID );

	    	?>

	        <div class="card-product">
	            <div class="col-xl-2 col-sm-2 col-2 clicable-product" data-product-link="<?= $product_link; ?>">
	                <img class="img-product-list" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo the_title(); ?>" />
	            </div>
	            <div class="col-xl-8 col-sm-8 col-7">
	            	<div class="row">
		                <div class="column-title-product col-xl-6 col-sm-6 col-12 clicable-product" data-product-link="<?= $product_link; ?>">
		                    <div class="content-product">
		                    <h4 class="title-product"><?php the_title();?></h4>
		                    <small class="author-product">Sakata</small>
		                    </div>
		                </div>

		                <div class="column-discount col-xl-6 col-sm-6 col-12">
		                	<?php if( 'discount' == $promotional['type'] ) : ?>
		                        <div class="discount-product">
		                          <span class="discount-text"><?= $promotional['text']; ?></span>
		                        </div>
		                    <?php elseif( 'condition' == $promotional['type'] && ! empty( $promotional['text'] ) ): ?>
		                    	<div class="condition-product">
		                          <span class="discount-text"><?= $promotional['text']; ?></span>
		                        </div>
		                    <?php endif; ?>
		            	</div>
		            </div>
	            </div>
	            <div class="column-btn-send col-xl-2 col-sm-2 col-3">
	                <form class="cart" action="<?php the_permalink( $post->ID ); ?>" method="post" enctype='multipart/form-data'>
						<button 
							type="submit" 
							name="add-to-wishlist" 
							value="<?= $post->ID ?>" 
							class="add_to_wishlist_button<?php echo $on_wishlist; ?>" 
							aria-label="<?php echo esc_attr( $message ); ?>" 
							data-toggle="tooltip" 
							data-bs-toggle="tooltip" 
							data-placement="left" 
							data-bs-placement="left" 
							title="<?php echo esc_attr( $message ); ?>">
						</button>
					</form>
	            </div>
	        </div>

	    <?php endwhile; ?>

	    <?php wp_reset_query();?>
	</div>
</div>