<div class="missing-step" data-step="species">
	<h3><?= __( 'Species of interest', 'hello-sakataifield' ); ?></h3>
	<p><?= __( 'Select at least one item from the list bellow', 'hello-sakataifield' ); ?></p>
	<form class="species-selection-form" type="post">
		<div class="species-selection">
			<?php
				$user_country = strtolower( sakataifield()->customer->get_country() );
				$place_tax = get_term_by( 'slug', $user_country, 'distributor_place' );

				$terms0 = get_terms(
					array(
						'taxonomy'   => 'product_cat',
						'parent'  => 0,
						'fields'     => 'ids',
						'meta_query'  => [
							[
								'key' => 'place_tax',
								'compare' => 'LIKE',
								'value' => '"' . $place_tax->term_id . '"'
							]
						]
					)
				);

				$terms1 = array();
				foreach ( $terms0 as $parent ) {
					$terms1 = array_merge(
						$terms1,
						get_terms(
							array(
								'taxonomy'   => 'product_cat',
								'parent'  => $parent,
								'fields'     => 'ids',
							)
						)
					);
				}
				$terms = array();
				foreach ( $terms1 as $parent ) {
					$terms = array_merge(
						$terms,
						get_terms(
							array(
								'taxonomy'   => 'product_cat',
								'parent'  => $parent,
							)
						)
					);
				}

				$order_terms = array();
				foreach ( $terms as $term ) {
					$slug = sanitize_title( $term->name );
					$order_terms[ $slug ] = $term;
				}
				ksort( $order_terms );
				$terms = $order_terms;

				$terms = wp_list_pluck( $terms, 'name', 'term_id' );

				$value = array_map( 'intval', array_filter( (array) $value ) );
				if ( ! empty( $terms ) ) {
					
					foreach ( $terms as $term_id => $term_name ) {

						$field = '';
						
						$slug = sanitize_title( $term_name );
						if ( preg_match( '/^porta-enxerto/', $slug ) ) {
							continue;
						}

						$thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );

						$field .= '<span class="form-check">';
						$field .= '<input type="checkbox" class="form-check-input input-checkbox" value="' . esc_attr( $term_id ) . '" name="species" id="specie' .  '_' . esc_attr( $term_id ) . '"' . checked( in_array( $term_id, $value ), true, false ) . ' />';
						$field .= '<label for="specie' . '_' . esc_attr( $term_id ) . '" class="checkbox">';
						if ( $thumbnail_id ) {
							$field .= wp_get_attachment_image( $thumbnail_id );
						}
						$field .= esc_html( $term_name ) . '</label>';
						$field .= '</span>';

						echo $field;

					}
					
				}

			?>
		</div>
		<div class="ifield-btn-wrapper">
			<button id="btn-missing-species" disabled="disabled" type="submit" class="ifield-btn"><?= __( 'Continue', 'hello-sakataifield'); ?></button>
		</div>
	</form>
</div>
