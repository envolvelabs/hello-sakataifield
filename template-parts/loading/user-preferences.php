<div id="ifield-user-preferences">
	<table>
		<tbody>
			<tr>
				<td><?php _e( 'Music', 'hello-sakataifield' ); ?></td>
				<td>
					<button class="song-btn btn btn-success" data-action="play-song" data-song="ifield-1"><?php _e( 'Sounds of nature', 'hello-sakataifield' ); ?></button>
					<button class="song-btn btn btn-light" data-action="play-song" data-song="ifield-2"><?php _e( 'Exciting song', 'hello-sakataifield' ); ?></button>
					<button class="song-btn btn btn-light" data-action="play-song" data-song="ifield-3"><?php _e( 'Farm song', 'hello-sakataifield' ); ?></button>
					<button class="song-btn btn btn-light" data-action="deactivate-music" data-song="off"><?php _e( 'Deactivate', 'hello-sakataifield' ); ?></button>
				</td>
			</tr>
			<tr>
				<td><?php _e( 'Sound effects', 'hello-sakataifield' ); ?></td>
				<td>
					<button class="song-btn btn btn-success" data-action="activate-sfx" data-sfx="on"><?php _e( 'Activate', 'hello-sakataifield' ); ?></button>
					<button class="song-btn btn btn-light" data-action="deactivate-sfx" data-sfx="off"><?php _e( 'Deactivate', 'hello-sakataifield' ); ?></button>
				</td>
			</tr>
		</tbody>
	</table>
	<small class="song-instructions"><?php _e( 'You can change this settings later', 'hello-sakataifield' ); ?></small>
</div>