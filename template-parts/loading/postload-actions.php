<?php $missing_data = ifield_get_user_missing_data(); ?>

<div id="fix-user-data-screen">
	<h2><?= __( 'For a better experience, please confirm your personal data bellow', 'hello-sakataifield' ); ?></h2>
	<div id="missing-board">
		<?php 

			$missing_data = ifield_get_user_missing_data(); 

			if( in_array( 'missing_address', $missing_data ) ) {
				get_template_part( 'template-parts/loading/missing', 'address', ['missing_data' => $missing_data] );
			}

			if( in_array( 'species', $missing_data ) ) {
				get_template_part( 'template-parts/loading/missing', 'species', ['missing_data' => $missing_data] );
			}

		?>
	</div>
</div>
<div id="ifield-post-loading-actions">
	<h2 style="text-align: center;"><?php _e( 'All set, click to start, enjoy!', 'hello-sakataifield' ); ?></h2>
	<button id="start-ifield" class="btn"><?php _e( 'Start', 'hello-sakataifield' ); ?></button>
	<p style="text-align: center;">
		<a id="toggle-user-preferences" href="#"><?php _e( 'Change preferences', 'hello-sakataifield' ); ?></a>
	</p>
	<?php get_template_part( 'template-parts/loading/user', 'preferences' ); ?>
</div>