<img id="loading-logo" src="<?= get_stylesheet_directory_uri() ?>/assets/img/logo-ifield.svg" width="146" height="72" />
<div class="loading-progress loading-item">
	<div class="loading-progress-bar"></div>
</div>
<h2 class="blinker loading-item"><?php _e( 'Loading experience', 'hello-sakataifield' ); ?></h2>