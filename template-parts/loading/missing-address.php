<div class="missing-step" data-step="address">
	<?php 

		$country = get_user_meta( get_current_user_id(), 'billing_country', true );
		$location_type = 'BR' == $country ? __( 'state', 'hello-sakataifield' ) : __( 'region', 'hello-sakataifield' );
		$country_id = get_term_by( 'slug', strtolower( $country ), 'distributor_place');

		if( ! empty( $country_id ) ) {
			$country_id = $country_id->term_id;
		}

		$choices = get_terms([
			'taxonomy' => 'distributor_place',
			'parent' => $country_id
		]);

	?>

	<h3><?= __( 'Confirm your location', 'hello-sakataifield' ); ?></h3>
	<p><?= sprintf( __( 'Inform your %s', 'hello-sakataifield' ), $location_type ); ?></p>

	<form class="missing-address-form" type="post">

		<?php if( ! empty( $choices ) ) : ?>
			<select class="state-selection" name="region">
				<option value=""><?= __( 'Select a choice', 'hello-sakataifield' ) ?></option>
				<?php foreach( $choices as $choice ) : ?>
					<option value="<?= $choice->slug ?>"><?= $choice->name ?></option>
				<?php endforeach; ?>
			</select>
		<?php endif; ?>
		<input type="hidden" name="is_species_missing" value="<?= in_array( 'species', $args['missing_data'] ) ?>">
		<input type="hidden" name="country" value="<?= $country ?>">
		<div class="ifield-btn-wrapper">
			<button id="btn-missing-location" disabled="disabled" type="submit" class="ifield-btn"><?= __( 'Continue', 'hello-sakataifield'); ?></button>
		</div>
	</form>
</div>