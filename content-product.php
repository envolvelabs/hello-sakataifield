<?php
/**
 * Template used to display post content.
 *
 * @package hello-sakataifield
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	if ( function_exists( 'sakataifield_add_to_wishlist' ) ) :
		sakataifield_add_to_wishlist();
	endif;
	?>

</article><!-- #post-## -->
