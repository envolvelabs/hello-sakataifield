<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>	
	<nav id="ifield-sakatito-help">
		<?= ifield_sakatito_widget(); ?>
	</nav>
	</div><!-- #content -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
