<?php
/**
 * Theme functions and definitions
 *
 * @package HelloElementorChild
 */

/**
 * Load child theme css and optional scripts
 *
 * @return void
 */


function hello_sakataifield_setup() {
	
	load_theme_textdomain( 'hello-sakataifield', get_stylesheet_directory_uri() . '/languages' );
}
add_action( 'after_setup_theme', 'hello_sakataifield_setup' );

add_filter('show_admin_bar', '__return_false');

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page([
        'page_title'    => __( 'Field settings', 'hello-sakataifield' ),
        'menu_title'    => __( 'Field settings', 'hello-sakataifield' ) ,
        'menu_slug'     => 'general-settings'
    ]);
}

function hello_elementor_child_enqueue_scripts() {
	wp_enqueue_style(
		'hello-elementor-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		[
			'hello-elementor-theme-style',
		],
		'1.0.0'
	);
}
add_action( 'wp_enqueue_scripts', 'hello_elementor_child_enqueue_scripts', 20 );

require get_theme_file_path( 'inc/ajax.php' );
require get_theme_file_path( 'inc/hooks.php' );
require get_theme_file_path( 'inc/helpers.php' );
require get_theme_file_path( 'inc/shortcodes.php' );
require get_theme_file_path( 'inc/google-fonts.php' );

/*
 * Prevent 3D from loading to ease development
 */
function is_dev_mode() {

	if( defined( 'DEV_MODE' ) && isset( $_GET['dev_mode'] ) ) {
		return true;
	} else {
		return false;
	}
	

}

function sakata_interactive_footer() {

	$post = get_post();

	$ifield_page = get_field( 'page_ifield', 'options' );

	$pages = [];

	if( ! empty( $ifield_page ) ) {
		$pages[] = $ifield_page->ID;
	} else {
		//Legacy
		$post_id = apply_filters( 'wpml_object_id', 512, 'page', true  );
		$ifield_page = get_post( $post_id );
		$pages[] = $post_id;
	}

	if( ! is_page() && ! in_array( $post->ID, $pages ) ) {
		return;
	}

	if( $ifield_page->ID == $post->ID ) {
		get_template_part( 'template-parts/audio/music' );
		get_template_part( 'template-parts/stands/modal-preview', 'placeholder' );
		get_template_part( 'template-parts/foryou/modal', 'foryou' );
		echo do_shortcode( '[sakatito]' );
	}	

	?>
	<?php  if( ! is_dev_mode() && $ifield_page->ID == $post->ID ) : ?>


		<?php $missing_data = ifield_get_user_missing_data(); ?>

		<a class="exhibitor-nav exhibitor-nav-left"></a>
		<a class="exhibitor-nav exhibitor-nav-right"></a>
		<span id="stand-specialist-tooltip"></span>
		<div id="ifield-loading">
			<span><?php _e( 'Loading...', 'hello-sakataifield' ) ?></span>
			<img src="<?= get_stylesheet_directory_uri() ?>/assets/img/spinner.svg" width="40" height="40" />
		</div>
		<div id="ifield-3D-loading">
			<?php get_template_part( 'template-parts/loading/basic', 'loader' ); ?>
			<?php get_template_part( 'template-parts/loading/postload', 'actions' ); ?>
		</div>
	<?php endif; ?>

	<?php  if( is_dev_mode() && $ifield_page->ID == $post->ID ) : ?>

		<?php $missing_data = ifield_get_user_missing_data(); ?>

		<div id="ifield-3D-loading" class="<?= empty( $missing_data ) ? '' : ' has-missing-data'; ?>">
			<img id="loading-logo" src="<?= get_stylesheet_directory_uri() ?>/assets/img/logo-ifield.svg" width="146" height="72" />
			<div id="fix-user-data-screen">
				<h2><?= __( 'For a better experience, please confirm your personal data bellow', 'hello-sakataifield' ); ?></h2>
				<div id="missing-board">
					<?php 

						$missing_data = ifield_get_user_missing_data(); 

						if( in_array( 'missing_address', $missing_data ) ) {
							get_template_part( 'template-parts/loading/missing', 'address', ['missing_data' => $missing_data] );
						}

						if( in_array( 'species', $missing_data ) ) {
							get_template_part( 'template-parts/loading/missing', 'species', ['missing_data' => $missing_data] );
						}

					?>
				</div>
			</div>
		</div>

	<?php endif; ?>
	
	<?php if( $ifield_page->ID == $post->ID && $tvifield_page = get_field( 'page_tv_ifield', 'options' ) ) : ?>
		<a class="btn btn-primary jumping" id="enter-ifield-btn" href="<?= get_permalink( $tvifield_page->ID ) ?>"><?php _e( 'Enter', 'hello-sakataifield' ); ?></a>
	<?php endif; ?>

	<?php

}

add_action( 'wp_footer', 'sakata_interactive_footer', 10 );

function sakata_ifield_load_scripts() {

	global $post, $current_user, $tvifield_page, $customer_data;

	// $post = get_post();

	if( empty( $post ) ) {
		return;
	}

	$customer_data = sakataifield_customer_data();

	// $distributors = ifield_get_distributors_for_customer( get_current_user_id() );

	// if( ! empty( $distributors ) ) {
	// 	foreach( $distributors as $distributor_id ) {
	// 		$distributor = sakataifield_get_distributor( $distributor_id );
	// 		echo ifield_debug_var( $distributor );
	// 	}
	// }

	// echo ifield_debug_var( $customer_data );die;

	$ifield_page = get_field( 'page_ifield', 'options' );
	$tvifield_page = get_field( 'page_tv_ifield', 'options' );

	$pages = [];

	if( ! empty( $ifield_page ) ) {
		$pages[] = $ifield_page->ID;
	} else {
		//Legacy
		$post_id = apply_filters( 'wpml_object_id', 512, 'page', true  );
		$ifield_page = get_post( $post_id );
		$pages[] = $post_id;
	}

	if( ! empty( $tvifield_page ) ) {
		$pages[] = $tvifield_page->ID;
	}

	// echo ifield_debug_var( $pages );die;

	if ( ! is_page() && ! in_array( $post->ID, $pages ) ) {
		return;
	}

	$ver = '1.0.0';

	if( $post->ID  == $ifield_page->ID ) {

		wp_enqueue_style( 'ifield', get_stylesheet_directory_uri() . '/assets/css/ifield.css', [], $ver );
		wp_enqueue_style( 'foryou', get_stylesheet_directory_uri() . '/assets/css/foryou.css', [], $ver );
		wp_enqueue_script( 'ifield', get_stylesheet_directory_uri() . '/assets/js/ifield.js', ['jquery'], $ver, true );

		wp_enqueue_style( 'sakataifield-plugin', get_stylesheet_directory_uri() . '/assets/css/sakataifield-plugin.css', [], $ver );

		$onboarding = [
			'init' => apply_filters( 'wpml_object_id', get_field( 'page_onboarding', 'options' ), 'elementor_library', true  ),
			'steps' => get_field( 'page_onboarding_steps', 'options' ),
		];

		if( ! empty( $onboarding['steps'] ) ) {
			foreach( $onboarding['steps'] as $key => $popup ) {
				$onboarding['steps'][$key] = apply_filters( 'wpml_object_id', $popup['ID'], 'elementor_library', true  );
			}
		}

		wp_localize_script(
			'ifield',
			'iFieldData',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'wpnonce' => wp_create_nonce( 'iField' ),
				'isMobile' => wp_is_mobile(),
				'standsInfo' => json_encode( ifield_get_stands_data() ),
				'stationsMap' => ifield_get_stations_map(),
				'standsMap' => ifield_get_stands_map(),
				'modalAboutID' => apply_filters( 'wpml_object_id', 382, 'elementor_library', true  ),
				'modalFAQID' => apply_filters( 'wpml_object_id', 428, 'elementor_library', true  ),
				'modalOnboardingInit' =>  $onboarding['init'],
				'modalOnboardingSteps' => $onboarding['steps'],
				'onboardingComplete' => (bool) get_user_meta( $current_user->ID, 'onboarding_complete', true ),
				'onboardingVideos' => apply_filters( 'sakataifield_onboarding_videos', [] ),
				'userPreferences' => get_user_meta( $current_user->ID, 'ifield_user_preferences', true ),
				'customerData' => $customer_data,
				'userMissingData' => count( ifield_get_user_missing_data() ),
				'i18n' => [
					'exhibitor' => __( 'Exhibitor', 'sakataifield' ),
					'wishlist' => __( 'Wishlist', 'sakataifield' ),
					'checkitout' => __( 'Check it out', 'sakataifield' ),
					'yourlisthasbeensent' => __( 'Your list has been sent.', 'sakataifield' ),
					'sentlists' => __( 'Sent lists', 'sakataifield' ),
					'checkyourhistory' => __( 'Check your history', 'sakataifield' ),
					'cancelscheduling' => __( 'Cancel scheduling', 'hello-sakataifield' ), 
					'letmeknowwhentostart' => __( 'Let me know when to start', 'hello-sakataifield' ), 
					'foryou' => __( "For you", 'hello-sakataifield' ),
					'seewhatwevegotespeciallyforyou' => __( "See what we've got especially for you", 'hello-sakataifield' ),
					'tvifield' => __( "TV iField", 'hello-sakataifield' ),
					'watchnow' => __( "Watch now", 'hello-sakataifield' ),
					'errortryingtosave' => __( "Error trying to save, please try again", 'hello-sakataifield' ),
				]
			)
		);

		if( ! is_dev_mode() ) {
		    wp_enqueue_script( '3d-ifield-script', get_stylesheet_directory_uri() . '/3d/field/dist/script.bundle.js', [], $ver );
		    wp_enqueue_script( '3d-ifield-import', get_stylesheet_directory_uri() . '/3d/field/dist/imports.bundle.js', [], $ver );
		}

	}

	if( $post->ID == $tvifield_page->ID ) {

		wp_enqueue_style( 'tvifield', get_stylesheet_directory_uri() . '/assets/css/tvifield.css', [], $ver );
		wp_enqueue_script( 'tvifield', get_stylesheet_directory_uri() . '/assets/js/tvifield.js', ['jquery'], $ver, true );

		$videos = get_posts( 'post_type=tvifield&posts_per_page=1&post_status=publish,future&order=asc&suppress_filters=0' );

		$show_scheduled_events = 'no';

		if( ! empty( $videos ) ) {

			$first_event_time = strtotime( $videos[0]->post_date );
			$current_time = current_time( 'U' );

			if( $first_event_time > $current_time ) {
				$show_scheduled_events = 'yes';
			}
		}

		// $show_scheduled_events = 'no';

		wp_localize_script(
			'tvifield',
			'iFieldData',
			array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'wpnonce' => wp_create_nonce( 'iField' ),
				'showScheduledEvents' => $show_scheduled_events,
				'fieldUrl' => get_permalink( apply_filters( 'wpml_object_id', 512, 'page', true ) )
			)
		);
	}

}
add_action('wp_enqueue_scripts', 'sakata_ifield_load_scripts');

function sakata_enqueue_admin_script( $hook ) {

    wp_enqueue_style( 'admin', get_stylesheet_directory_uri() . '/assets/css/admin.css', [], $ver );
}
add_action( 'admin_enqueue_scripts', 'sakata_enqueue_admin_script' );

function sakata_defer_scripts( $tag, $handle, $src ) {

	$defer = array(
   		'3d-ifield-script',
    	'3d-ifield-import'
  	);
  	if ( in_array( $handle, $defer ) ) {
    	return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
  	}

    return $tag;
}
add_filter( 'script_loader_tag', 'sakata_defer_scripts', 10, 3 );

function sakata_register_menus(){
    register_nav_menus( array(
        'ifield' => __( 'Interactive menu', 'hello-sakataifield' ),
        'products'  => __( 'Station menu', 'hello-sakataifield' ),
        'exhibitor'  => __( 'Exhibitor menu', 'hello-sakataifield' ),
    ) );
}
add_action( 'after_setup_theme', 'sakata_register_menus', 0 );

function sakata_generate_nav_images( $item_output, $item, $depth, $args ){
	if( $args->theme_location == 'products' ) {
		
		$thumbnail_id = get_post_thumbnail_id( $item->object_id );
		// $thumbnail_id = get_field( 'icon', 'product_cat_' . $item->object_id );

	    if( ! empty( $thumbnail_id ) ) {
	        $dom = new DOMDocument();
	        $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $item_output );
	        $img = $dom->createDocumentFragment();
	        $img->appendXML( wp_get_attachment_image( $thumbnail_id ) );
	        $dom->getElementsByTagName( 'a' )->item( 0 )->insertBefore( $img );
	        $item_output = $dom->saveHTML();
	    }
	} else if( $args->theme_location == 'exhibitor' ) {
		$thumbnail_id = get_field( 'exhibitor_logo', $item->object_id );
		
		$website = get_post_meta( $item->object_id, 'exhibitor_website', true );
		$stand_type = get_post_meta( $item->object_id, 'exhibitor_stand_type', true );

	    if( ! empty( $thumbnail_id ) ) {
	        $dom = new DOMDocument();
	        $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $item_output );

	        $dom->getElementsByTagName( 'a' )->item( 0 )->nodeValue = '';

	        $img = $dom->createDocumentFragment();
	        $img->appendXML( wp_get_attachment_image( $thumbnail_id, 'medium' ) );
	        $dom->getElementsByTagName( 'a' )->item( 0 )->insertBefore( $img );
	        
	        $li_class = $dom->getElementsByTagName( 'a' )->item( 0 )->getAttribute( 'class' );
	        $li_class.= ' exhibitor-tag ' . $stand_type . '-exhibitor';
	        $dom->getElementsByTagName( 'a' )->item( 0 )->setAttribute( 'class', $li_class );

	        if( 'pocket' == $stand_type ) {
	        	$dom->getElementsByTagName( 'a' )->item( 0 )->setAttribute( 'target', '_blank' );
	        	if( empty( $website ) ) {
	        		$dom->getElementsByTagName( 'a' )->item( 0 )->setAttribute( 'href', '#' );
	        	} else {
	        		$dom->getElementsByTagName( 'a' )->item( 0 )->setAttribute( 'href', $website );
	        	}
	        }

	        $data_slug = $dom->createAttribute( 'data-slug' );
	        $data_slug->value = get_post_field( 'post_name', $item->object_id );
	        $dom->getElementsByTagName( 'a' )->item( 0 )->appendChild( $data_slug );
	        $item_output = $dom->saveHTML();
	    }
	}
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'sakata_generate_nav_images', 20, 4 );
