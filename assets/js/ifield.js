
window.standsInfo = JSON.parse( iFieldData.standsInfo );

console.log( JSON.parse( iFieldData.standsInfo ) );

( function( $ ){

	var preventUserBadBehavior = null;
	var xhr = null;

	function playStream() {

		var player = null;

		player = Stream( $( '#standModal .swiper-slide-active iframe' ).get( 0 ) );

		player.pause();

	}

	function HUD( action ) {

		if( 'hide' == action ) {

			$( 'body' ).addClass( 'hud-hide' );

		} else {

			$( 'body' ).removeClass( 'hud-hide' );

		}

	}

	function wishlistMenu( subtitle ) {

		var $container = $( '#ifield-logo' );

		if( $container.find( '.ifield-back-btn' ).length ) {
			$container.find( '.ifield-back-btn' ).off().remove();
		} 

		$container.parent().find( 'nav' ).addClass( 'ifield-inside-menu-active' );

		var $backButton = $( '<a href="#" class="ifield-back-btn" data-view="wishlist"></a>' );

		$backButton.append( '<span class="btitle">' + iFieldData.i18n.wishlist + '</span><br/>' );
		$backButton.append( '<span class="bsubtitle">' + subtitle + '</span>' );

		$backButton.on( 'click', function( e ){

			e.preventDefault();

			console.log( 'Exit from wishlist menu' );

			switch( $( this ).attr( 'data-view' ) ){
				case 'archive':
					$( '.wishlist-load-view' ).trigger( 'click' );
					break;
				case 'sent':
					$( '.wishlist-load-view' ).trigger( 'click' );
					break;
				case 'view':
					$( '.wishlist-load-view' ).trigger( 'click' );
					break;
				default:
					$( '#wishlistModal' ).modal( 'hide' );
					break;
			}

		});

		$container.append( $backButton );

		$backButton.show();

	}

	function exhibitorMenu( subtitle ) {

		var $container = $( '#ifield-logo' );

		if( $container.find( '.ifield-back-btn' ).length ) {
			$container.find( '.ifield-back-btn' ).off().remove();
		}

		$container.parent().find( 'nav' ).addClass( 'ifield-inside-menu-active' );

		var $backButton = $( '<a href="#" class="ifield-back-btn"></a>' );

		$backButton.append( '<span class="btitle">' + iFieldData.i18n.exhibitor + '</span><br/>' );
		$backButton.append( '<span class="bsubtitle">' + subtitle + '</span>' );

		$backButton.on( 'click', function( e ){

			e.preventDefault();

			if( $( 'body' ).hasClass( 'modal-open' ) ) {

				$( '#standModal' ).modal( 'hide' );
				console.log( 'Close from exhibitor menu' );

			} else {
				console.log( 'exit' );
				window.findBulletByAction( 'exit' );
				console.log( 'Exit from exhibitor menu' );

			}

		});

		$container.append( $backButton );

		$backButton.show();

	}

	function foryouMenu() {

		var $container = $( '#ifield-logo' );

		if( $container.find( '.ifield-back-btn' ).length ) {
			$container.find( '.ifield-back-btn' ).off().remove();
		}

		$container.parent().find( 'nav' ).addClass( 'ifield-inside-menu-active' );

		var $backButton = $( '<a href="#" class="ifield-back-btn"></a>' );
 
		$backButton.append( '<span class="btitle">' + iFieldData.i18n.foryou + '</span><br/>' );
		$backButton.append( '<span class="bsubtitle">' + iFieldData.i18n.seewhatwevegotespeciallyforyou + '</span>' );

		$backButton.on( 'click', function( e ){

			e.preventDefault();

			$( '#foryouModal' ).modal( 'hide' );
			console.log( 'Exit from foryou menu' );

		});

		$container.append( $backButton );

		$backButton.show();

	}

	function tvifieldMenu() {

		var $container = $( '#ifield-logo' );

		if( $container.find( '.ifield-back-btn' ).length ) {
			$container.find( '.ifield-back-btn' ).off().remove();
		}

		$container.parent().find( 'nav' ).addClass( 'ifield-inside-menu-active' );

		var $backButton = $( '<a href="#" class="ifield-back-btn"></a>' );
 
		$backButton.append( '<span class="btitle">' + iFieldData.i18n.tvifield + '</span><br/>' );
		$backButton.append( '<span class="bsubtitle">' + iFieldData.i18n.watchnow + '</span>' );

		$backButton.on( 'click', function( e ){

			e.preventDefault();

			window.findBulletByAction( 'exit' );
			console.log( 'Exit from TV iField menu' );

		});

		$container.append( $backButton );

		$backButton.show();

	}

	function restoreDefaultMenu() {

		var $container = $( '#ifield-logo' );

		$container.find( '.ifield-back-btn' ).off().remove();

		$container.parent().find( 'nav' ).removeClass( 'ifield-inside-menu-active' );

	}

	function addReportEntry( entry ) {

		entry = typeof entry == 'undefined' ? {} : entry;

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce' : iFieldData.wpnonce,
                'action' : 'sakata_ajax_add_report_entry',
                'entry' : entry
            }
        });

	}

	function openTutorial() {
		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'onboarding_complete' : true,
                'action': 'sakata_ajax_user_onboarding_completed'
            },
            success : function() {

            	const popup = elementorFrontend.documentsManager.documents[iFieldData.modalOnboardingInit];
			    const modal = popup.getModal();
			    modal.hide();

            	if( iFieldData.modalOnboardingSteps.length ) {
				    elementorProFrontend.modules.popup.showPopup( { id: iFieldData.modalOnboardingSteps[0] } );
            	}
            }
        });
	}

	function removeHash () { 
	    history.pushState( '', document.title, window.location.pathname + window.location.search );
	}

	function iFieldPlaySFX( sfxname ) {

		if( ! $( 'body' ).hasClass( 'sfx-enabled' ) ) {
			return;
		}

		var $sfx = $( '.sfx-enabled #audio-tracks audio[data-song="' + sfxname + '"]' );

		if( ! $sfx.length ) {
			return;
		}

		$sfx[0].volume = 1;
		$sfx[0].currentTime = 0;
		$sfx[0].play();
	}

	function iFieldPlaySong( songName, volFrom, volTo, volRatio, interval, startTime ) {

		var vol = volFrom;
		var volControl = null;
		var $song = $( '#audio-tracks audio[data-song="' + songName + '"]' );

		if( ! $song.length ) {
			return;
		}

		volRatio = typeof volRatio == "undefined" ? 0.075 : volRatio;
		interval = typeof interval == "undefined" ? 500 : interval;

		if( typeof startTime !== "undefined" ){
			$song[0].currentTime = 0;	
		}
		
		$song[0].volume = volFrom;
		$song[0].play();
		$song.addClass( 'playing' );

		volControl = setInterval( function(){

			if( volTo < volFrom ) {
				
				vol-= volRatio;

				if( vol <= 0 ) {
					vol = 0;
				}

				if( vol <= volTo ) {
					$song[0].volume = vol;
					$song[0].pause();
					$song.removeClass( 'playing' );
					clearInterval( volControl );
				}

			} else {

				vol+= volRatio;

				if( vol >= volTo ) {
					vol = volTo;
					clearInterval( volControl );
				}

				$song[0].volume = vol;

			}

		}, interval );

	}

	window.iFieldPlaySong = iFieldPlaySong;

	function iFieldShowStandPreview( index ) {

		//$( '#standModalPreviewPlaceholder' ).modal( 'show' );
		$( '#standModalPreview' ).modal( 'dispose' ).remove();
		$( 'body' ).addClass( 'ifield-loading' );

		xhr = $.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'post_id': iFieldData.standsMap['index_' + index].id,
                'base' : 'preview',
                'content' : 'preview',
                'action': 'sakata_ajax_stand_load'
            },
            success : function( content ) {

            	$( 'body' ).addClass( 'dispose-modal-backdrop' ).append( content );

            	const swiper = new Swiper( '.ifield-stand-preview-swiper', {
					navigation: {
				    	nextEl: '.swiper-button-next',
				    	prevEl: '.swiper-button-prev',
				  	}
				});

            	$( '#standModalPreview' ).modal( 'show' );
				//$( '#standModalPreviewPlaceholder' ).modal( 'hide' );
				$( 'body' ).removeClass( 'ifield-loading' );

            }
        });

	}

	function ifieldLoadStandContent( standID, content ) {

		$( '#standModal' ).modal( 'dispose' ).remove();
		$( 'body' ).addClass( 'ifield-loading' );

		xhr = $.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'post_id': standID,
                'content' : content,
                'action': 'sakata_ajax_stand_load'
            },
            success : function( html ) {

            	$( '#standModal' ).modal( 'dispose' ).remove();

            	$( 'body' ).append( html );

            	$( '.ifield-stand-swiper' ).each( function(){

                	var $swiperContainer = $( this );
                	var swiperConfig = typeof $swiperContainer.data( 'swiper' ) === 'undefined' ? {} : $swiperContainer.data( 'swiper' );
                	var swiperDefaults = {
						navigation: {
					    	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
					    	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
					  	}
					};

                	var swiper = new Swiper( $swiperContainer.get( 0 ), {...swiperConfig, ...swiperDefaults } );
                });

                $( '.ifield-swiper-gallery-container' ).each( function(){

                	var $swiperContainer = $( this );

                	var swiper = new Swiper( '.ifield-swiper-gallery-thumbs', {
				        spaceBetween: 10,
				        slidesPerView: 4,
				        freeMode: true,
				        watchSlidesProgress: true,
				    });
				    var swiper2 = new Swiper( '.ifield-swiper-gallery', {
				        spaceBetween: 10,
				        navigation: {
				        	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
				          	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
				        },
				        thumbs: {
				        	swiper: swiper,
				        },
				    });

				    swiper2.on( 'transitionEnd', function () {
						playStream();
					});

					// swiper2.on( 'beforeTransitionStart', function () {
					// 	pauseStream();
					// });

					

                });

            	const swiper = new Swiper( '.ifield-stand-about-swiper', {
					navigation: {
				    	nextEl: '.swiper-button-next',
				    	prevEl: '.swiper-button-prev',
				  	}
				});

				$( '#standModalPreview' ).modal( 'hide' );

                $( '#standModal' ).modal( 'show' );

                $( '#standModal .add_to_wishlist_button' ).tooltip();

                $( 'body' ).removeClass( 'ifield-loading' );

                if( 'products' == content ) {
                	setTimeout( function(){
                		addReportEntry({
							"object_type" : 'product',
							"object_ID" : $( '.ifield-stand-other-products-swiper .swiper-slide-active .ifield-stand-change-product' ).data( 'product-id' ),
							"entity" : 'exhibitor',
							"entity_ID" : standID
						});
                	}, 500 );
                }

            }
        });

	}

	function closeMainMenu() {

		var $container = $( '.ifield-interactive-menu' );

		$( '.menu-produtos-container' ).hide();
		$( '.menu-produtos-espanhol-container' ).hide();
		$container.removeClass( 'products-menu-active' );

		$( '.menu-expositores-container' ).hide();
		$container.removeClass( 'exhibitor-menu-active' );

		$( '.menu-ifield-container' ).hide();
		$( '.menu-ifield-espanhol-container' ).hide();
		$container.removeClass( 'active' );

		$( 'body' ).removeClass( 'main-menu-active' );

		$( 'body' ).removeClass( 'clear-field' );

	}

	$( document ).on( 'click', '.btn-agenda', function( e ) {

		e.preventDefault();
		
		const btnScheduling = e.target

		btnScheduling.classList.add( 'loading' );
		btnScheduling.disabled = true;
		
		var data = {
			action: 'ajax_users_subscriptions',
			post_id: $( this ).data( 'id' ),
		};

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'post_id': $( this ).data( 'id' ),
                'action': 'sakata_ajax_users_subscriptions'
            },
            success : function( response ) {
            	if( response.success ) {
					$( '#standModal .col-agendamentos .btn-agenda' ).addClass( 'cancel' );
					btnScheduling.classList.add( 'cancel' );
					$( window ).trigger( 'event_subscribed', ['subscribed', data.post_id] );
				} else {
					$( '#standModal .col-agendamentos .btn-agenda' ).removeClass( 'cancel' );
					btnScheduling.classList.remove( 'cancel' );
					$( window ).trigger( 'event_subscribed', ['unsubscribed', data.post_id] );
				}
            },
            complete : function( jqXHR, textStatus ) {
            	btnScheduling.disabled = false;
				btnScheduling.classList.remove( 'loading' );
				btnScheduling.textContent = jqXHR.responseJSON.data.message;
				$( '#standModal .col-agendamentos .btn-agenda' ).text( jqXHR.responseJSON.data.message );
            }
        });

	});

	$( document ).on( 'on_wishlist_load_view', function( e, view ){

		switch( view ){
			case 'archive':
				$( '.ifield-back-btn .bsubtitle' ).text( iFieldData.i18n.sentlists );
				break;
			case 'sent':
				$( '.ifield-back-btn .bsubtitle' ).text( iFieldData.i18n.yourlisthasbeensent );
				break;
			case 'view':
				$( '.ifield-back-btn .bsubtitle' ).text( iFieldData.i18n.checkyourhistory );
				break;
			default:
				$( '.ifield-back-btn .bsubtitle' ).text( iFieldData.i18n.checkitout );
				break;
		}

		$( '.ifield-back-btn' ).attr( 'data-view', view );

	});

	$( document ).on( 'click', '.return-btn', function(){

		findBulletByAction( 'exit' );

		$( 'body' ).attr( 'data-modal', '' );

		if( xhr ) {
			xhr.abort();
			$( 'body' ).removeClass( 'ifield-loading' );
		}

	});

	$( document ).on( 'click', '.btn-wishlist-buy', function() {

		var $btn = $( this );

		if( $btn.data( 'wishlist-id' ) ) {

			addReportEntry({
				"action" : 'click_buy',
				"object_type" : 'product',
				"object_ID" : $btn.data( 'product-id' )
			});

		}

	});

	$( document ).on( 'click', '.btn-wishlist-email-station', function() {

		var $btn = $( this );

		if( $btn.data( 'station-id' ) ) {

			addReportEntry({
				"action" : 'view_expert',
				"object_type" : 'station',
				"object_ID" : $btn.data( 'station-id' )
			});

		}

		if( $btn.data( 'product-id' ) ) {

			addReportEntry({
				"action" : 'view_expert',
				"object_type" : 'product',
				"object_ID" : $btn.data( 'product-id' )
			});

		}

	});

	$( window ).on( 'interface3Dclickevent', function( e, type, index ){

		if( $( 'body' ).hasClass( 'dialog-container' ) ) {
			return;
		}

		$( '#enter-ifield-btn' ).removeClass( 'show-up' );

		if( xhr ) {
			xhr.abort();
			$( 'body' ).removeClass( 'ifield-loading' );
		}

		switch( type ) {
			case 'stand':

				//iFieldShowStandPreview( index );
				$( '#standModal' ).modal( 'dispose' ).remove();
				$( '#standModalPreview' ).modal( 'dispose' ).remove();

				$( '#enter-ifield-btn' ).removeClass( 'show-up' );

				addReportEntry({
					"object_type" : 'stand',
					"object_ID" : iFieldData.standsMap['index_' + index].id,
					"entity" : 'exhibitor',
					"entity_ID" : iFieldData.standsMap['index_' + index].id
				});

				exhibitorMenu( iFieldData.standsMap['index_' + index].title );

				HUD( 'hide' );

				$( 'body' ).attr( 'data-modal', 'stand' );

				break;
			case 'demField':

				ifieldLoadStandContent( iFieldData.standsMap['index_' + index].id, 'demo-field' );

				exhibitorMenu( iFieldData.standsMap['index_' + index].title );

				$( 'body' ).attr( 'data-modal', 'demo-field' );

				HUD( 'hide' );

				break;
			case 'tviField':

				$( '#standModal' ).modal( 'dispose' ).remove();
				$( '#standModalPreview' ).modal( 'dispose' ).remove();

				$( '#enter-ifield-btn' ).addClass( 'show-up' );

				$( 'body' ).attr( 'data-modal', 'tv-ifield' );

				tvifieldMenu();

				HUD( 'hide' );

				break;
			default: //station

				window.location = '#' + iFieldData.stationsMap['index_' + index].url_frament;

				$( 'body' ).attr( 'data-modal', 'station' );

				HUD( 'hide' );

				break;
		}

	});

	$( window ).on( 'standinterface3Dhoverevent', function( e, id, action ){

		if( iFieldData.isMobile ) {
			return;
		}

		var $body = $( 'body' );

		if( $body.data( 'standsfx' ) != action + '_' + id ) {
			iFieldPlaySFX( 'spin' );
			$body.data( 'standsfx', action + '_' + id );
		}

	});

	$( window ).on( 'interface3Dhoverevent', function( e, type, index ){

		var $body = $( 'body' );

		if( $body.data( 'sfx' ) != type + '_' + index ) {
			iFieldPlaySFX( 'spin' );	
			$body.data( 'sfx', type + '_' + index );
		}
		
		if( $body.hasClass( 'ifield-loading' ) ) {
			return;
		}

		if( $body.hasClass( 'dialog-container' ) ) {
			return;
		}

		if( 'stand' != type ) {
			return;
		}

		if( $body.data( 'last-stand-loaded' ) == index && $( '#standModalPreview' ).is( ':visible' ) ) {
			return;
		}
		
		if( ! $( 'body' ).hasClass( 'camera-animating' ) ) {
			iFieldShowStandPreview( index );
			$body.data( 'last-stand-loaded', index );
		}

	});

	$( document ).on( 'click', '#enter-ifield-btn', function(){

		$( '#ifield-3D-loading' ).removeClass( 'disable' );
		setTimeout( function(){
			$( '#loading-logo' ).addClass( 'blinker' ).fadeIn( 400 );
		}, 400 );

	});

	$( window ).on( 'standinterface3Dclickevent', function( e, index, action ){

		switch( action ) {
			case 'logo':

				ifieldLoadStandContent( iFieldData.standsMap['index_' + index].id, 'about' );

				$( 'body' ).attr( 'data-modal', 'about' );
				$( 'body' ).attr( 'data-preview-modal', 'stand' );

				break;
			case 'main_img':

				if( 'gold' == iFieldData.standsMap['index_' + index].type ) {
					ifieldLoadStandContent( iFieldData.standsMap['index_' + index].id, 'demo-field' );
				}

				$( 'body' ).attr( 'data-modal', 'demo-field' );
				$( 'body' ).attr( 'data-preview-modal', 'stand' );

				break;
			case 'tv_img':

				ifieldLoadStandContent( iFieldData.standsMap['index_' + index].id, 'video' );

				$( 'body' ).attr( 'data-modal', 'tv-img' );
				$( 'body' ).attr( 'data-preview-modal', 'stand' );

				addReportEntry({
					"action" : 'view_video',
					"object_type" : 'stand',
					"object_ID" : iFieldData.standsMap['index_' + index].id,
					"entity" : 'exhibitor',
					"entity_ID" : iFieldData.standsMap['index_' + index].id
				});

				break;
			case 'specialist_img':

				window.open( iFieldData.standsMap['index_' + index].stand.specialist_chat_url, '_blank' );

				addReportEntry({
					"action" : 'view_expert',
					"object_type" : 'stand',
					"object_ID" : iFieldData.standsMap['index_' + index].id,
					"entity" : 'exhibitor',
					"entity_ID" : iFieldData.standsMap['index_' + index].id
				});

				break;
			default: //main_product

				ifieldLoadStandContent( iFieldData.standsMap['index_' + index].id, 'products' );

				$( 'body' ).attr( 'data-modal', 'exhibitor-products' );
				$( 'body' ).attr( 'data-preview-modal', 'stand' );

				break;
		}

	});

	$( document ).on( 'click', 'a[href="#tv-ifield"]', function( e ){

		window.findBulletByAction( 'bulletPoint_tviField_0' );

		closeMainMenu();

	});

	$( document ).on( 'click', '.ifield-wishlist-menu a, #btn-list-interest', function( e ){
		e.preventDefault();
		closeMainMenu();
		$( '#wishlistModal' ).modal( 'show' );

	});

	$( document ).on( 'click', '.ifield-help-menu a, .faq-link', function( e ){

		elementorProFrontend.modules.popup.showPopup( { id: iFieldData.modalFAQID } );

		addReportEntry({
			"object_type" : 'page',
			"object_ID" : iFieldData.modalFAQID
		});

		closeMainMenu();
		window.findBulletByAction( 'exit' );

	});

	$( document ).on( 'click', '.ifield-about-menu a', function( e ){

		elementorProFrontend.modules.popup.showPopup( { id: iFieldData.modalAboutID } );

		addReportEntry({
			"object_type" : 'page',
			"object_ID" : iFieldData.modalAboutID
		});

		closeMainMenu();
		window.findBulletByAction( 'exit' );

	});

	$( document ).on( 'click', '.menu-item-object-station a', function( e ){

		e.preventDefault();

		var $self = $( this );

		var url = $self.attr( 'href' );
		var slug = url.split( '#' ).pop().split( '/' ).pop();
		var index = iFieldData.stationsMap['slug_' + slug].index;

		$( 'body' ).data( 'last-interaction', url.split( '#' ).pop() );
		
		window.findBulletByAction( 'bulletPoint_station_' + index );
		closeMainMenu();

	});

	$( document ).on( 'click', '.menu-item-object-exhibitor a:not(.pocket-exhibitor)', function( e ){

		e.preventDefault();

		var $self = $( this );

		var slug = $self.data( 'slug' );
		var index = iFieldData.standsMap['slug_' + slug].index;

		$( 'body' ).data( 'last-interaction', slug );

		$( '#standModalPreview' ).modal( 'dispose' ).remove();
		
		window.findBulletByAction( 'bulletPoint_stand_' + index );

		closeMainMenu();

	});

	$( document ).on( 'mouseenter', '.exhibitor-menu-ready .menu-item-object-exhibitor a', function( e ){

		e.preventDefault();

		clearTimeout( preventUserBadBehavior );

		var $self = $( this );

		preventUserBadBehavior = setTimeout( function(){

			var url = $self.attr( 'href' );

			if( '/' == url.substr( url.length - 1 ) ) {
				url = url.slice( 0, -1 ); 
			}

			var slug = url.split( '/' ).pop();
			var index = iFieldData.standsMap['slug_' + slug].index;

			$( 'body' ).data( 'last-interaction', slug );

			$( '#standModalPreview' ).modal( 'dispose' ).remove();
			
			iFieldShowStandPreview( index );

			$( 'body' ).data( 'last-stand-loaded', index );

		}, 500 );

	});

	$( document ).on( 'mouseout', '.exhibitor-menu-ready .menu-item-object-exhibitor a', function( e ){
		clearTimeout( preventUserBadBehavior );
	});

	$( document ).on( 'click', '.stand-preview-btn', function( e ){

		e.preventDefault();
		e.stopPropagation();

		var index = iFieldData.standsMap['id_' + $( this ).data( 'stand' )].index;

		window.findBulletByAction( 'bulletPoint_stand_' + index );

		$( '#standModalPreview' ).modal( 'dispose' ).remove();

		closeMainMenu();

	});

	$( document ).on( 'click', '.stand-preview-open-btn', function(){

		var $btn = $( this );

    	const swiper = new Swiper( '.ifield-stand-preview-swiper', {
			navigation: {
		    	nextEl: '.swiper-button-next',
		    	prevEl: '.swiper-button-prev',
		  	}
		});

        $( '#standModalPreview' ).modal( 'show' );

	});

	$( document ).on( 'click', '[data-load-content]', function( e ){

		e.preventDefault();

		var $btn = $( this );

		$( '#standModal .modal-body-content' ).addClass( 'loading' );
		$( 'body' ).addClass( 'ifield-loading' );

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'post_id': $btn.data( 'stand' ),
                'content' : $btn.data( 'load-content' ),
                'action': 'sakata_ajax_stand_load_content'
            },
            success : function( content ) {

            	$( '.modal-body' ).scrollTop( 0 );

                $( '#standModal .modal-body-content' ).removeClass( 'loading' ).html( content );

                $( '.ifield-stand-swiper' ).each( function(){

                	var $swiperContainer = $( this );
                	var swiperConfig = typeof $swiperContainer.data( 'swiper' ) === 'undefined' ? {} : $swiperContainer.data( 'swiper' );
                	var swiperDefaults = {
						navigation: {
					    	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
					    	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
					  	}
					};

                	var swiper = new Swiper( $swiperContainer.get( 0 ), {...swiperConfig, ...swiperDefaults } );
                });

                $( '.ifield-swiper-gallery-container' ).each( function(){

                	var $swiperContainer = $( this );

                	var swiper = new Swiper( '.ifield-swiper-gallery-thumbs', {
				        spaceBetween: 10,
				        slidesPerView: 4,
				        freeMode: true,
				        watchSlidesProgress: true,
				    });
				    var swiper2 = new Swiper( '.ifield-swiper-gallery', {
				        spaceBetween: 10,
				        navigation: {
				        	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
				          	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
				        },
				        thumbs: {
				        	swiper: swiper,
				        },
				    });

				    swiper2.on( 'transitionEnd', function () {
						playStream();
					});

					// swiper2.on( 'beforeTransitionStart', function () {
					// 	pauseStream();
					// });

                });

                const swiper = new Swiper( '.ifield-stand-about-swiper', {
					navigation: {
				    	nextEl: '.swiper-button-next',
				    	prevEl: '.swiper-button-prev',
				  	}
				});

                if( typeof $btn.data( 'product-id' ) !== 'undefined' ) {

                	$( '.ifield-stand-change-product[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).trigger( 'click' );

                }

                $( '#standModal .add_to_wishlist_button' ).tooltip();

                $( 'body' ).removeClass( 'ifield-loading' );

            }
        });

	});

	$( document ).on( 'click', '.ifield-stand-change-product', function( e ){

		e.preventDefault();

		var $btn = $( this );

		$( '.ifield-stand-products-product-content.active, .ifield-stand-products-product-slider.active' ).removeClass( 'active' );
		$( '.ifield-stand-products-product-content[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).addClass( 'active' );
		$( '.ifield-stand-products-product-slider[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).addClass( 'active' );

		$( '#standModal .modal-body' ).animate({
	        scrollTop: parseInt( $( '.ifield-stand-products-product-slider' ).offset().top )
	    }, 200 );

		addReportEntry({
			"object_type" : 'product',
			"object_ID" : $btn.data( 'product-id' ),
			"entity" : 'exhibitor',
			"entity_ID" : $btn.data( 'exhibitor' )
		});

	});

	/*
	 * Controls main menu behavior
	 */
	$( '.ifield-interactive-menu-btn' ).on( 'click', function(){

		var $container = $( this ).closest( '.ifield-interactive-menu' );

		$( 'body' ).addClass( 'main-menu-active' );
		$( 'body' ).removeClass( 'clear-field' );

		if( $container.hasClass( 'active' ) ) {

			if( $container.hasClass( 'products-menu-active' ) ) {
				$( '.menu-produtos-container' ).hide( 200 );
				$( '.menu-produtos-espanhol-container' ).hide( 200 );
				$container.removeClass( 'products-menu-active' );
			}else if( $container.hasClass( 'exhibitor-menu-active' ) ) {
				$( '.menu-expositores-container' ).hide( 200 );
				$container.removeClass( 'exhibitor-menu-active' );
				$( 'body' ).removeClass( 'exhibitor-menu-ready' );
			} else {
				$( '.menu-ifield-container' ).hide( 200 );
				$( '.menu-ifield-espanhol-container' ).hide( 200 );
				$container.removeClass( 'active' );
				$( 'body' ).removeClass( 'main-menu-active' );
				$( 'body' ).addClass( 'clear-field' );
			}

		} else {

			$( '.menu-ifield-container' ).show( 200 );
			$( '.menu-ifield-espanhol-container' ).show( 200 );
			$container.addClass( 'active' );

		}

	});

	window.addEventListener( 'keydown', function( event ) {
		if( event.defaultPrevented ) {
	    	return;
	  	}
	  	switch (event.key) {
		    case 'Esc':
		    case 'Escape':

		    	removeHash();
		    	
		    	var $container = $( '.ifield-interactive-menu' );

				if( $container.hasClass( 'active' ) ) {
		      		$( '.ifield-interactive-menu-btn' ).trigger( 'click' );
		      	}

		      	$( '#enter-ifield-btn' ).removeClass( 'show-up' );

		      	if( xhr ) {
					xhr.abort();
					$( 'body' ).removeClass( 'ifield-loading' );
				}

				if( $( 'body' ).attr( 'data-modal' ) && 'stand' == $( 'body' ).attr( 'data-modal' ) ) {
					$( 'body' ).removeAttr( 'data-modal' );
				}

		    	break;
		    default:
		      	return;
	  	}

	  	// Cancel the default action to avoid it being handled twice
	  	event.preventDefault();

	}, true );

	$( window ).on( 'interface3Donexit', function(){

		removeHash();

		restoreDefaultMenu();

		HUD( 'show' );

		$( 'body' ).addClass( 'clear-field' );

		$( '#enter-ifield-btn' ).removeClass( 'show-up' );

	});

	$( window ).on( 'tvifieldBeforeAnimate', function(){
		
		if( iFieldData.userPreferences.music == 'off' || iFieldData.userPreferences.music == 'ifield-1' ){
			return;
		}

		var $currentPlaying = $( '#audio-tracks [data-song].playing' );

		if( $currentPlaying.length ) {
			iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 0.1, 200 );
		}				
		setTimeout( function(){
			iFieldPlaySong( 'tvifield', 0, 0.4, 0.1, 500, 0 );	
		}, 800 );

	});

	$( window ).on( 'tvifieldExitBeforeAnimate', function(){

		if( iFieldData.userPreferences.music == 'off' || iFieldData.userPreferences.music == 'ifield-1' ){
			return;
		}
		
		var $currentPlaying = $( '#audio-tracks [data-song].playing' );

		if( $currentPlaying.length ) {
			iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 0.1, 200 );
		}		

		setTimeout( function(){
			iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
		}, 800 );

		HUD( 'show' );

	});

	$( document ).on( 'click', '.btn-finish-onboarding, .dialog-widget .dialog-close-button', function(){

		$( 'body' ).removeClass( 'block-canvas' );
		$( 'body' ).removeClass( 'dialog-body' );
		$( 'body' ).removeClass( 'dialog-lightbox-body' );
		$( 'body' ).removeClass( 'dialog-container' );
		$( 'body' ).removeClass( 'dialog-lightbox-container' );

		if( iFieldData.userPreferences.music != 'off' ) {
			setTimeout( function(){
				iFieldPlaySong( iFieldData.userPreferences.music, 0, 0.4, 0.1, 500 );	
			}, 400 );
		}
		if( iFieldData.userPreferences.sfx == 'on' ) {
			$( 'body' ).addClass( 'sfx-enabled' );
		}

		$( 'body' ).addClass( 'clear-field' );

	});

	/*
	 * Open the product menu
	 */
	$( '.ifield-tour-menu' ).on( 'click', function( e ){
		e.preventDefault();
		$( this ).closest( '.ifield-interactive-menu' ).addClass( 'products-menu-active' );
		$( '.menu-produtos-container' ).show( 200 );
		$( '.menu-produtos-espanhol-container' ).show( 200 );
	});

	/*
	 * Open the product menu
	 */
	$( '.ifield-exhibitor-menu' ).on( 'click', function( e ){
		e.preventDefault();
		$( this ).closest( '.ifield-interactive-menu' ).addClass( 'exhibitor-menu-active' );
		$( '.menu-expositores-container' ).show( 200 );
		setTimeout( function(){
			$( 'body' ).addClass( 'exhibitor-menu-ready' );
		}, 400 );		
	});

	$( document ).on( 'click', '.modal-station-mini .btn-close', function( e ) {
		findBulletByAction( 'exit' );
	});

	$( document ).on( 'ready', function(){

		const script = document.createElement("script");
	    script.src = "https://embed.videodelivery.net/embed/sdk.latest.js"
	    document.head.appendChild( script );

		if( $( '.loading-progress-bar' ).length ) {

			var loading = setInterval( function(){

				$( '.loading-progress-bar' ).css( 'width', window.assetsLoadingPercentage + '%' );

				if( window.assetsLoadingPercentage >= 100 ) {

					clearInterval( loading );

					if( iFieldData.userPreferences.hasOwnProperty( 'music' ) ) {
						$( '#ifield-user-preferences [data-song]' ).removeClass( 'btn-success' ).addClass( 'btn-light' );
						$( '#ifield-user-preferences [data-song="' + iFieldData.userPreferences.music + '"]' ).addClass( 'btn-success' ).removeClass( 'btn-light' );
					}

					if( iFieldData.userPreferences.hasOwnProperty( 'sfx' ) ) {
						$( '#ifield-user-preferences [data-sfx]' ).removeClass( 'btn-success' ).addClass( 'btn-light' );
						$( '#ifield-user-preferences [data-sfx="' + iFieldData.userPreferences.sfx + '"]' ).addClass( 'btn-success' ).removeClass( 'btn-light' );
					}
					
					setTimeout( function(){	
						console.log( iFieldData.userMissingData, 'here' )
						if( iFieldData.userMissingData > 0 ) {
							$( '#ifield-3D-loading .loading-item' ).hide();
							$( '#ifield-3D-loading' ).addClass( 'has-missing-data' );
							$( '#fix-user-data-screen' ).slideDown( 400 );
						} else {
							$( '#ifield-3D-loading .loading-item' ).slideUp( 400 );
							$( '#ifield-post-loading-actions' ).slideDown( 400 );
						}
					}, 500 );
					
				}

			}, 500 );

		}

	});

	$( document ).on( 'click', '#toggle-user-preferences', function( e ) {

		e.preventDefault();

		$( '#ifield-user-preferences' ).slideToggle( 400 );

	});

	$( document ).on( 'click', '.btn-onboarding-start .elementor-button-link, .onboarding-link', function(){

		openTutorial();

	});

	$( document ).on( 'click', '.onboarding-next' , function() {
		const id = $( this ).closest( '[data-elementor-type="popup"]' ).data( 'elementor-id' );
		const popup = elementorFrontend.documentsManager.documents[id];
	    const modal = popup.getModal();
	    modal.hide();
	});

	$( document ).on( 'click', '#start-ifield', function() {

		$( '#ifield-post-loading-actions, #loading-logo' ).fadeOut( 800, function(){
			setTimeout( function(){	
				var $currentPlaying = $( '#audio-tracks [data-song].playing' );

				if( ! $currentPlaying.length ) {
					iFieldPlaySong( $( '#ifield-user-preferences [data-song].btn-success' ).data( 'song' ), 0, 0.4, 0.1, 200, 0 );	
				}

				$( '#ifield-3D-loading' ).addClass( 'disable' );

				if( ! iFieldData.onboardingComplete ) {

					var $currentPlaying = $( '#audio-tracks [data-song].playing' );

					if( $currentPlaying.length ) {
						iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 1, 200 );
					}

					$( 'body' ).removeClass( 'sfx-enabled' );
					$( 'body' ).addClass( 'block-canvas' );

					elementorProFrontend.modules.popup.showPopup( { id: iFieldData.modalOnboardingInit } );

					var loadOnboardingVideo = function( videoIndex ) {

						videoIndex = typeof videoIndex == "undefined" ? 0 : videoIndex;

						$.ajax({
				            type: 'post',
				            url: iFieldData.ajaxurl,
				            data: {
				                '_ajax_nonce': iFieldData.wpnonce,
				                'video_index' : videoIndex,
				                'action': 'sakata_ajax_load_onboarding_video'
				            },
				            success : function( html ) {
				            	$( '#onboarding-video-container .elementor-widget-container' ).html( html );
				            	$( '#onboarding-video-container .elementor-widget-wrap' ).css( 'background-color', 'transparent' );
				            	var player = Stream( document.getElementById( 'onboarding-stream' ) );
								player.addEventListener( 'ended', () => {
									videoIndex++;
									if( videoIndex < iFieldData.onboardingVideos.length ) {
										loadOnboardingVideo( videoIndex );	
									} else {
										$( 'body' ).addClass( 'onboarding-videos-watched' );
										openTutorial();
									}
								});
				            }
				        });
				    }
				    loadOnboardingVideo( 0 );
				} else {

					setTimeout( function(){

						$( 'body' ).addClass( 'clear-field' );	

					}, 400 );
					
				}

			}, 500 );
		});

		if( 'on' == $( '#ifield-user-preferences .btn-success[data-sfx]' ).data( 'sfx' ) ) {
			$( 'body' ).addClass( 'sfx-enabled' );
		}

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce': iFieldData.wpnonce,
                'user_preferences' : {
                	"music" : $( '#ifield-user-preferences .btn-success[data-song]' ).data( 'song' ),
                	"sfx" : $( '#ifield-user-preferences .btn-success[data-sfx]' ).data( 'sfx' )
                },
                'action': 'sakata_ajax_save_user_preference'
            },
            success : function() {
            	iFieldData.userPreferences.music = $( '#ifield-user-preferences .btn-success[data-song]' ).data( 'song' );
            	iFieldData.userPreferences.sfx = $( '#ifield-user-preferences .btn-success[data-sfx]' ).data( 'sfx' );
            }
        });

	});

	$( document ).on( 'click', '.song-btn[data-action]', function(){

		var $self = $( this );
		var $currentPlaying = $( '#audio-tracks [data-song].playing' );

		if( $self.hasClass( 'btn-success' ) ) {
			return;
		}

		$self.closest( 'tr' ).find( '.btn-success' ).removeClass( 'btn-success' ).addClass( 'btn-light' );
		$self.removeClass( 'btn-light' ).addClass( 'btn-success' );

		switch( $self.data( 'action' ) ) {
			case 'deactivate-music': 
				if( $currentPlaying.length ) {
					iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 1, 200 );
				}
				break;
			case 'activate-sfx':
				break;
			case 'deactivate-sfx':
				break;
			default:
				if( $currentPlaying.length ) {
					iFieldPlaySong( $currentPlaying.data( 'song' ), 0.4, 0, 0.1, 200 );
				}				
				setTimeout( function(){
					iFieldPlaySong( $self.data( 'song' ), 0, 0.4, 0.1, 200, 0 );	
				}, 1000 );
				break;
		}

	});	

	$( document ).on( 'hide.bs.modal', '.modal-station-mini', function( e ) {

		setTimeout( function() {

			if( window.hasOwnProperty( 'findBulletByAction' ) && ! $( 'body' ).hasClass( 'sakataifield-loading' ) ) {
				window.findBulletByAction( 'exit' );	
			}

		}, 1000 );
		
	}); 

	$( document ).on( 'show.bs.modal', '.modal-station-products', function( e ) {
		$( 'body' ).removeClass( 'clear-field' );
		$( 'body' ).attr( 'data-modal', 'station' );
		$( 'body' ).addClass( 'hud-hide' );
	});

	$( document ).on( 'hide.bs.modal', '.modal-station-products', function( e ) {

		setTimeout( function() {

			if( window.hasOwnProperty( 'findBulletByAction' ) && ! $( 'body' ).hasClass( 'sakataifield-loading' ) ) {
				window.findBulletByAction( 'exit' );	
				$( 'body' ).removeAttr( 'data-modal' );
				$( 'body' ).removeClass( 'hud-hide' );
			}

		}, 1000 );
		
	}); 

	$( document ).on( 'show.bs.modal', '.modal-station-product', function( e ) {
		$( 'body' ).removeClass( 'clear-field' );
		$( 'body' ).attr( 'data-modal', 'station' );
		$( 'body' ).addClass( 'hud-hide' );
	});

	$( document ).on( 'hide.bs.modal', '.modal-station-product', function( e ) {

		setTimeout( function() {

			if( window.hasOwnProperty( 'findBulletByAction' ) && ! $( 'body' ).hasClass( 'sakataifield-loading' ) ) {
				window.findBulletByAction( 'exit' );	
				$( 'body' ).removeAttr( 'data-modal' );
				$( 'body' ).removeClass( 'hud-hide' );
			}

		}, 1000 );
		
	}); 


	$( document ).on( 'show.bs.modal', '#wishlistModal', function (e) {

		wishlistMenu( iFieldData.i18n.checkitout );

		HUD( 'hide' );

		$( 'body' ).removeClass( 'clear-field' );

	});

	$( document ).on( 'hide.bs.modal', '#wishlistModal', function (e) {

		restoreDefaultMenu();

		HUD( 'show' );

		$( 'body' ).addClass( 'clear-field' );

	});

	jQuery( '.btn' ).on( 'click', function(){

		var $btn = jQuery( this );

		setTimeout( function(){

			$( 'select[option="Matriz Tal"]' ).attr( 'selected', true );

		}, 1000 );

	});

	$( document ).on( 'hide.bs.modal', '#standModal', function (e) {

		var $body = $( 'body' );

		if( ! $body.attr( 'data-preview-modal' ) && $body.attr( 'data-modal' ) ) {

			if( 'demo-field' == $body.attr( 'data-modal' ) ) {

				window.findBulletByAction( 'exit' );

			}

		}

		if( $body.attr( 'data-preview-modal' ) && 'stand' == $body.attr( 'data-preview-modal' ) ) {

			$body.attr( 'data-modal', 'stand' );
			$body.removeAttr( 'data-preview-modal' );

		} else {

			$body.attr( 'data-modal', '' );

		}

	});

	$( document ).on( 'hidden.bs.modal', '#standModal', function (e) {
		$( '#stream-player' ).remove();
	});

	$( document ).on( 'show.bs.modal', '#foryouModal', function (e) {

		foryouMenu();

		HUD( 'hide' );

		$( 'body' ).attr( 'data-modal', 'foryou' );

		$( 'body' ).removeClass( 'clear-field' );

	});

	$( document ).on( 'hide.bs.modal', '#foryouModal', function (e) {

		restoreDefaultMenu();

		HUD( 'show' );

		$( 'body' ).removeAttr( 'data-modal' );

		$( 'body' ).addClass( 'clear-field' );

	});

	$( document ).on( 'click', '.ifield-foryou-menu a', function( e ){

		e.preventDefault();

		$( '#foryouModal' ).modal( 'show' );

		closeMainMenu();

	});

	$( document ).on( 'click', '#btn-close-foryou', function(){

		$( '#foryouModal' ).modal( 'hide' );

	});

	$( document ).on( 'click', '.clicable-product', function( e ){

		var $self = $( this );

		if( '' == $self.data( 'product-link' ) ) {
			return;
		}

		$( '#foryouModal' ).modal( 'hide' );

		window.open( $self.data( 'product-link' ), '_self' );

	});

	$( document ).on( 'wishlist_counter_updated', function( e, count ){

		if( count > 0 ) {
			$( '.foryou-wishlist' ).slideDown();
		} else {
			$( '.foryou-wishlist' ).slideUp();
		}

	});

	$( window ).on( 'event_subscribed', function( e, action, postID ){

		var $btn = $( '.scheduled-list .card-agenda[data-id="' + postID + '"] .btn-agenda' );
		var $btn2 = $( '.foryou-interest-lectures .card-agenda[data-id="' + postID + '"] .btn-agenda' );

		if( 'subscribed' == action ) {

			$btn.addClass( 'cancel' ).text( iFieldData.i18n.cancelscheduling );
			$btn2.addClass( 'cancel' ).text( iFieldData.i18n.cancelscheduling );
			$( '.scheduled-list .card-agenda[data-id="' + postID + '"]' ).addClass( 'subscribed' );

		} else {

			$btn.removeClass( 'cancel' ).text( iFieldData.i18n.letmeknowwhentostart );
			$btn2.removeClass( 'cancel' ).text( iFieldData.i18n.letmeknowwhentostart );
			$( '.scheduled-list .card-agenda[data-id="' + postID + '"]' ).removeClass( 'subscribed' );

		}

		if( $( '.scheduled-lectures .card-agenda.subscribed' ).length >= 1 ) {
			$( '.scheduled-lectures' ).addClass( 'has-subscriptions' );
		} else {
			$( '.scheduled-lectures' ).removeClass( 'has-subscriptions' );
		}

	});

	$( document ).on( 'click', '#the-sakatito', function( e ){

		e.preventDefault();

		var $sakatitoContainer = $( this ).closest( '.ifield-sakatito' );

		if( $sakatitoContainer.hasClass( 'active' ) ) {
			$sakatitoContainer.find( '.sakatito-menu' ).slideToggle( 300, function(){
				$sakatitoContainer.toggleClass( 'active' );
				$sakatitoContainer.find( '.sakatito-message' ).fadeIn( 100 );
			}); 
		} else {
			$sakatitoContainer.find( '.sakatito-message' ).hide();
			$sakatitoContainer.toggleClass( 'active' );
			$sakatitoContainer.find( '.sakatito-menu' ).slideToggle( 300 ); 
		}

	});

	$( document ).on( 'click', '.close-sakatito-link', function( e ){

		e.preventDefault();

		$( '#the-sakatito' ).trigger( 'click' );

	});

	$( document ).on( 'click', '#ifield-logo a.custom-logo-link', function( e ){

		e.preventDefault();

		window.findBulletByAction( 'exit' );

	});

	$( document ).on( 'click', '.exhibitor-nav', function(){

		var $self = $( this );

		if( $self.hasClass( 'exhibitor-nav-left' ) ) {
			window.moveBetweenStands( -1 );
		} else {
			window.moveBetweenStands( 1 );
		}

	});

	$( document ).on( 'click', '.species-selection .form-check', function(){

		var $self = $( this );

		if( $self.find( 'input' ).is( ':checked' ) ) {

			if( $self.parent().find( '.form-check.checked' ).length >= 3 ) {
				return;
			}

			$self.addClass( 'checked' );

		} else {

			$self.removeClass( 'checked' );

		}

		console.log($self.parent().find( '.form-check.checked' ).length)

		if( $self.parent().find( '.form-check.checked' ).length == 0 ) {
			$( '#btn-missing-species' ).attr( 'disabled', true );
		} else {
			$( '#btn-missing-species' ).attr( 'disabled', false );
		}

	});

	$( document ).on( 'change', '.state-selection', function( e ){

		if( $( this ).val() == "" ) {
			$( '#btn-missing-location' ).attr( 'disabled', true );
		} else {
			$( '#btn-missing-location' ).attr( 'disabled', false );
		}

	});
	

	$( document ).on( 'submit', '.missing-address-form', function( e ){

		e.preventDefault();

		var $form = $( this );

		var fields = $form.serializeArray();

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce' : iFieldData.wpnonce,
                'action' : 'sakata_ajax_save_user_location',
                'location' : fields[0].value,
                'country' : fields[2].value
            },
            success: function(){
            		
            	$( '.missing-step[data-step="address"]' ).hide();

            	if( fields[1].value == 1 ) {
            		$( '.missing-step[data-step="species"]' ).slideDown();
            	} else {
            		$( '#fix-user-data-screen' ).hide();
            		$( '#ifield-3D-loading' ).removeClass( 'has-missing-data' );
            		$( '#ifield-post-loading-actions' ).slideDown();
            	}

            },
            error: function(){
            	alert( iFieldData.i18n.errortryingtosave );
            }
        });

	});

	$( document ).on( 'submit', '.species-selection-form', function( e ){

		e.preventDefault();

		var $form = $( this );

		var species = $form.serializeArray();
		var speciesArray = [];

		for( var i in species ) {
			speciesArray.push( species[i].value );
		}

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            data: {
                '_ajax_nonce' : iFieldData.wpnonce,
                'action' : 'sakata_ajax_save_user_species',
                'species' : speciesArray
            },
            success: function(){
            	$( '#fix-user-data-screen' ).hide();
            	$( '#ifield-3D-loading' ).removeClass( 'has-missing-data' );
            	$( '#ifield-post-loading-actions' ).slideDown();
            },
            error: function(){
            	alert( iFieldData.i18n.errortryingtosave );
            }
        });

	});
	
})( jQuery );