( function( $ ){

	// const script = document.createElement("script");
 //    script.src = "https://embed.videodelivery.net/embed/sdk.latest.js";
 //    document.head.appendChild( script );

 	function addReportEntry( entry, skipDuplicated ) {

		entry = typeof entry == 'undefined' ? {} : entry;
		skipDuplicated = typeof skipDuplicated == 'undefined' ? false : skipDuplicated;

		$.ajax({
            type: 'post',
            url: iFieldData.ajaxurl,
            dataType: 'json',
            data: {
                '_ajax_nonce' : iFieldData.wpnonce,
                'onboarding_complete' : true,
                'action' : 'sakata_ajax_add_report_entry',
                'entry' : entry,
                'skip_duplicated' : skipDuplicated
            }
        });

	}

	window.addReportEntry = addReportEntry;

	$( document ).on( 'ready', function(){

		$('#img-header-tvifield').hide();
		$("#modal-tvifield").hide();
		$("#modal-agenda-tvifield").hide();
		$("#img-load-ifieldtv").addClass( 'active' );

		setTimeout(function(){
			
			$('#load-page').hide();
			
			if( 'yes' == iFieldData.showScheduledEvents ) {

				$( "#modal-agenda-tvifield" ).fadeIn( 2000 );

				$( '#btn-close-agenda' ).off();

				$( '#btn-close-agenda' ).wrap( '<a href="' + iFieldData.fieldUrl + '"></a>')

			} else {

				$('#modal-tvifield').fadeIn( 2000 );	
				
			}
			
			$(" #header-tvifield .modal-header").css("display", "block");

			if( $( '.tvifield-remove-filters' ).length && $( '.video-list .video-item:visible' ).length ) {
				$( '.video-list .video-item:first:visible' ).trigger( 'click' );
			}
			
		}, 3000);

	});

	$( '.tvifield-remove-filters' ).on( 'click', function(e){
		e.preventDefault();
		$( this ).remove();
		if( $( this ).data( 'source') ){
			$( '.card-agenda' ).removeAttr( 'data-exhibitor' );
		} else {
			$( '.video-list .hide' ).removeClass( 'hide' );
		}
	});	

	$( '.content-box-agenda' ).on( 'click', function(){
		$("#modal-tvifield").hide();
		$("#modal-agenda-tvifield").fadeIn(500);
		$("#header-tvifield").css("display", "block");
	});

	$( '#btn-close-agenda' ).on( 'click', function(){
		$("#modal-tvifield").fadeIn(500);
		$("#modal-agenda-tvifield").hide();
	});

	$( '#btn-close-tvifield' ).on( 'click', function(){
		$("#modal-tvifield").fadeOut(3000);
	});

	$( '#stepone-onboarding' ).on( 'click', function(){
		$("#modal-tvifield").fadeOut(3000);
	});

	$( '#play_onboarding' ).on( 'click', function(){
		$(".onboarding-home").hide();
	});

	//Onboarding

	$("body").on('click', ".onboarding-one" , function() {
		$('#elementor-popup-modal-1191').hide();
	
		})
	
		$("body").on('click', ".onboarding-two" , function() {
			$('#elementor-popup-modal-1166').hide();
	
		})
	
		$("body").on('click', ".onboarding-three" , function() {
			$('#elementor-popup-modal-1253').hide();
	
		})
	
		$("body").on('click', ".onboarding-four" , function() {
			$('#elementor-popup-modal-1275').hide();
	
		})
	
		$("body").on('click', ".onboarding-five" , function() {
			$('#elementor-popup-modal-1287').hide();
	
		})
	
	

	// Video Toggle
	const linkVideos = document.querySelectorAll('[data-js=link-video]')
	const videoIframe = document.querySelector('[data-js=video-iframe]')
	const whatsappExpert = document.querySelector('[data-js=whatsapp-expert]')
	const nameExpert = document.querySelector('[data-js=name-expert]')
	const imgExpert = document.querySelector('[data-js=img-expert]')
	const dataTime = document.querySelector('[data-time]')
	const nameSpeaker = document.querySelector('[data-js=palestrante-title]')


	linkVideos.forEach(link => {
		link.addEventListener('click', (e) => {

			e.preventDefault()
			videoIframe.src = `https://iframe.videodelivery.net/${(link.dataset.video)}?autoplay=1`
			nameExpert.textContent = link.dataset.speecherName
			whatsappExpert.href = link.dataset.speecherWhatsapp
			nameSpeaker.textContent = link.dataset.speaker
			dataTime.dataset.time = link.dataset.time
			imgExpert.src = link.dataset.speecherPicture || 'http://appsisecommerces3.s3.amazonaws.com/clientes/cliente3260/layout/1/img/user-placeholder.jpg?t=1500658590'

			addReportEntry({
				"object_type" : 'tvifield',
				"object_ID" : link.dataset.video_id,
				"entity" : link.dataset.exhibitor ? 'exhibitor' : 'sakata',
				"entity_ID" : link.dataset.exhibitor ? link.dataset.exhibitor : 0
			});
		});
	})

	// Scheduling
	if ($(".btn-agenda").length) {
		$(".btn-agenda").on("click", function (e) {
			e.preventDefault();
			const btnScheduling = e.target
			btnScheduling.classList.add('loading')
			btnScheduling.disabled = true
			
			var data = {
				action: "ajax_users_subscriptions",
				post_id: $(this).data("id"),
			};

			$.ajax({
	            type: 'post',
	            url: iFieldData.ajaxurl,
	            data: {
	                '_ajax_nonce': iFieldData.wpnonce,
	                'post_id': $( this ).data( 'id' ),
	                'action': 'sakata_ajax_users_subscriptions'
	            },
	            success : function( response ) {
	            	if( response.success ) {
						btnScheduling.classList.add( 'cancel' );
					} else {
						btnScheduling.classList.remove( 'cancel' );
					}
	            },
	            complete : function( jqXHR, textStatus ) {
	            	btnScheduling.disabled = false
					btnScheduling.textContent = jqXHR.responseJSON.data.message
					btnScheduling.classList.remove( 'loading' )
	            }
	        });

		});
	}

})( jQuery );