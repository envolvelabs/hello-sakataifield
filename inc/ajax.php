<?php

function set_locale_for_frontend_ajax_calls() {

    if ( is_admin() && defined( 'DOING_AJAX' ) && DOING_AJAX
        && substr( $_SERVER['HTTP_REFERER'], 0, strlen( admin_url() ) ) != admin_url() ) {

        load_theme_textdomain( 'hello-sakataifield', get_stylesheet_directory_uri() . '/languages' );
    }
}

add_action( 'admin_init', 'set_locale_for_frontend_ajax_calls' );

add_action('wp_ajax_sakata_ajax_add_report_entry', 'sakata_ajax_add_report_entry');

function sakata_ajax_add_report_entry() {

	check_ajax_referer( 'iField' );

	$data = [];
	$skip_duplicated = false;

	if( ! empty( $_POST['entry']['action'] ) ) {
		$data['action'] = sanitize_text_field( $_POST['entry']['action'] );
	}

	if( ! empty( $_POST['entry']['object_type'] ) ) {
		$data['object_type'] = sanitize_text_field( $_POST['entry']['object_type'] );
	}

	if( ! empty( $_POST['entry']['object_ID'] ) ) {
		$data['object_ID'] = absint( $_POST['entry']['object_ID'] );
	}

	if( ! empty( $_POST['entry']['user_ID'] ) ) {
		$data['user_ID'] = absint( $_POST['entry']['user_ID'] );
	}

	if( ! empty( $_POST['entry']['entity'] ) ) {
		$data['entity'] = sanitize_text_field( $_POST['entry']['entity'] );
	}

	if( ! empty( $_POST['entry']['entity_ID'] ) ) {
		$data['entity_ID'] = absint( $_POST['entry']['entity_ID'] );
	}

	if( isset( $_POST['skip_duplicated'] ) ) {
		$skip_duplicated = absint( $_POST['skip_duplicated'] );
	}

	ifield_add_report_entry( $data, $skip_duplicated );

	exit;

}

add_action( 'wp_ajax_sakata_ajax_save_user_species', 'sakata_ajax_save_user_species' );

function sakata_ajax_save_user_species() {

	check_ajax_referer( 'iField' );

	$species = $_POST['species'];

	if( ! empty( $species ) ) {

		foreach( $species as $k => $v ){
			$species[$k] = absint( $v );
		}

		$species = array_unique( $species );

		update_user_meta( get_current_user_id(), 'species', $species );

	}

	exit;

}

add_action( 'wp_ajax_sakata_ajax_save_user_location', 'sakata_ajax_save_user_location' );

function sakata_ajax_save_user_location() {

	check_ajax_referer( 'iField' );

	$country = sanitize_text_field( $_POST['country'] );
	$location = sanitize_text_field( $_POST['location'] );

	if( ! empty( $location ) ) {
		
		if( 'BR' == $country ) {
			$location = strtoupper( ltrim( $location, 'br-' ) );
			update_user_meta( get_current_user_id(), 'billing_state', $location );
		} else {
			update_user_meta( get_current_user_id(), 'billing_region', $location );
		}

	}

	exit;

}

add_action('wp_ajax_sakata_ajax_load_onboarding_video', 'sakata_ajax_load_onboarding_video');

function sakata_ajax_load_onboarding_video() {

	check_ajax_referer( 'iField' );

	$videos = apply_filters( 'sakataifield_onboarding_videos', [] );
	$video_index = absint( $_POST['video_index'] );
	$video_id = $videos[$video_index];

	echo do_shortcode( '[ifield_cloud_stream start_time="0" html_id="onboarding-stream" id="' . $video_id . '" controls="0"]' );

	exit;

}

add_action('wp_ajax_sakata_ajax_user_onboarding_completed', 'sakata_ajax_user_onboarding_completed');

function sakata_ajax_user_onboarding_completed() {

	check_ajax_referer( 'iField' );

	$user_id = get_current_user_id();

	update_user_meta( $user_id, 'onboarding_complete', true );

	exit;

}

add_action('wp_ajax_sakata_ajax_users_subscriptions', 'sakata_ajax_users_subscriptions');

function sakata_ajax_users_subscriptions() {

	check_ajax_referer( 'iField' );

	$post_id = absint( $_POST['post_id'] );
	$user_id = get_current_user_id();
	$user = get_user_by( 'id', $user_id );
	$field = is_array( get_field( 'users_subscriptions', $post_id ) ) ? get_field( 'users_subscriptions', $post_id ) : [];

	$user_notification_history = get_user_meta( $user_id, 'lecture_notification_history', true );
	$user_notification_history = empty( $user_notification_history ) ? [] : $user_notification_history;

	if( ! in_array( $user_id, $field ) ) {
		
		array_push( $field, $user_id );
		$r = update_field('users_subscriptions', $field, $post_id);

		if( ! in_array( $post_id, $user_notification_history ) ) {

			$fields = ['first_name','title', 'datetime', 'speecher_name'];
	        $headers = ['Content-Type: text/html; charset=UTF-8'];
	        $user_country = get_user_meta( $user_id, 'billing_country', true );
	        $user_country = empty( $user_country ) ? 'BR' : $user_country;
	        $options_key = 'BR' == $user_country ? 'options_' : 'options_es_';

	        $subject = get_option( $options_key . 'email_lecture_subscription_subject' );
	        $message = get_option( $options_key . 'email_lecture_subscription_content' );
	        $message = apply_filters( 'the_content', $message );

	        $replace_data = [];
	        $replace_data['first_name'] = get_user_meta( $user_id, 'first_name', true );
	        $replace_data['title'] = get_the_title( $post_id );
	        $replace_data['datetime'] = get_the_time( 'd/m/Y H:i', $post_id );
	        $replace_data['speecher_name'] = get_field( 'speecher_name', $post_id );

	        foreach( $fields as $field_key ) {
	            $subject = str_replace( '{{' . $field_key . '}}', $replace_data[$field_key], $subject );
	            $message = str_replace( '{{' . $field_key . '}}', $replace_data[$field_key], $message );
	        }

	        $mail = wp_mail( $user->user_email, $subject, $message, $headers );

	        if( $mail ) {
	        	array_push( $user_notification_history, $post_id );
	        	update_user_meta( $user_id, 'lecture_notification_history', $user_notification_history );
	        }

	    }

		return wp_send_json_success([
			'message' => __( 'Cancel scheduling', 'hello-sakataifield' ), 
			'data' => $field, 
			'post_id' => $post_id
		]);
	} else {
		$users_updated = array_diff( $field, [$user_id] );
		$r = update_field( 'users_subscriptions', $users_updated, $post_id );
		return wp_send_json_error([
			'message' => __( 'Let me know when to start', 'hello-sakataifield' ), 
			'data' => $users_updated
		]);
	}

	exit;
}

add_action( 'wp_ajax_sakata_ajax_save_user_preference', 'sakata_ajax_save_user_preference' );

function sakata_ajax_save_user_preference(){

	check_ajax_referer( 'iField' );

	global $current_user;

	$user_preferences = [];

	foreach( $_POST['user_preferences'] as $key => $value ) {
		$user_preferences[$key] = sanitize_text_field( $value );
	}

	update_user_meta( $current_user->ID, 'ifield_user_preferences', $user_preferences );

	exit;

}

add_action( 'wp_ajax_sakata_ajax_stand_load_content', 'sakata_ajax_stand_load_content' );

function sakata_ajax_stand_load_content(){

	check_ajax_referer( 'iField' );

	$post_id = absint( $_POST['post_id'] );
	$content = sanitize_text_field( $_POST['content'] );
	$bypass = absint( $_POST['bypass_approved_contents'] );

	echo ifield_render_stand_content( $post_id, $content, $bypass );

	if( in_array( $content, ['demo-field', 'about', 'products'] ) ) {
		ifield_add_report_entry([
			'action' => 'view_' . str_replace( '-', '_', $content ),
			'object_type' => 'stand',
			'object_ID' => $post_id,
			'entity' => 'exhibitor',
			'entity_ID' => $post_id
		]);
	} 

	exit;

}

add_action( 'wp_ajax_sakata_ajax_stand_load', 'sakata_ajax_stand_load' );

function sakata_ajax_stand_load(){

	check_ajax_referer( 'iField' );

	$post_id = absint( $_POST['post_id'] );
	$base = isset( $_POST['base'] ) ? sanitize_text_field( $_POST['base'] ) : '';
	$content = isset( $_POST['content'] ) ? sanitize_text_field( $_POST['content'] ) : '';
	$bypass = absint( $_POST['bypass_approved_contents'] );

	$sh_str = '[ifield_stand_modal id="' . $post_id . '"';

	if( ! empty( $base ) ) {
		$sh_str.= ' base="' . $base . '"';
	}

	if( ! empty( $content ) ) {
		$sh_str.= ' content="' . $content . '"';
	}

	if( ! empty( $bypass ) ) {
		$sh_str.= ' bypass_approved_contents="' . $bypass . '"';
	}

	$sh_str.= ']';
	
	echo do_shortcode( $sh_str );

	if( in_array( $content, ['demo-field', 'about', 'products'] ) ) {
		ifield_add_report_entry([
			'action' => 'view_' . str_replace( '-', '_', $content ),
			'object_type' => 'stand',
			'object_ID' => $post_id,
			'entity' => 'exhibitor',
			'entity_ID' => $post_id
		]);
	} 

	exit;

}