<?php

add_action( 'login_init', 'nextend_register_error' );

function nextend_register_error() {

    if( isset( $_GET['registration'], $_GET['nsl-notice'] ) && 'disabled' == $_GET['registration'] ) {

        wp_redirect( sakataifield_get_country_url() );
        exit;

    }

}

// add_action( 'template_redirect', function(){

//     // $customer_data = sakataifield_customer_data();

//     // echo ifield_debug_var( $customer_data );die;

//     // echo ifield_debug_var( ifield_get_stand_data( 25438 ) );

//     // $distributor_ids = sakataifield_get_distributors_for_customer();

//     // foreach( $distributor_ids as $id ) {
//     //     echo '<p>' . $id . ' ' . get_the_title( $id ) . '</p>';
//     // }

//     // $salesagent = sakataifield_get_customer_salesagent_data();

//     // echo ifield_debug_var( $salesagent );

//     // $discounts = sakataifield_get_me_discount_list();
//     // echo ifield_debug_var( $discounts );
//     $discount = sakataifield_get_customer_discount_for_product( 25259 );
//     //25235

//     echo ifield_debug_var( $discount );

//     exit;

// });

// add_action( 'template_redirect', function(){

//     if( ! isset( $_GET['pebolim'] ) ) {
//         return;
//     }

//     $skus = get_sku_distributors_json();
//     $count = 1;
//     $skus_to_exclude = [
//         'H.16.1.1332', 
//         'H.30.1.2033',
//         'H.37.10.1997',
//         'H.1.4.2120',
//         'H.19.1.1926',
//         'H.29.1.1900',
//         'H.3.1.2025',
//         'H.23.1.2009',
//         'H.29.1.1895',
//         'H.9.1.1968',
//         'H.1.1.2064',
//         'H.29.1.1896',
//         'H.9.1.2055',
//         'H.3.1.2028',
//         'H.3.1.2087'
//     ];

//     foreach( $skus as $sku => $distributor_base_ids ) {

//         if( in_array( $sku, $skus_to_exclude ) ) {
//             continue;
//         }

//         $products = get_posts([
//             'post_type'  => 'product',
//             'posts_per_page' => -1,
//             'suppress_filters' => false,
//             'meta_query' => [
//                 [
//                     'key'   => '_sku',
//                     'value' => $sku,
//                 ]
//             ]
//         ]);

//         if( ! empty( $products ) ) {

//             $distributor_ids = [];

//             $product = reset( $products ); 

//             foreach( $distributor_base_ids as $base_id ) {

//                 $distributor_id = sakataifield_get_distributor_id_by_base_id( $base_id );

//                 if( ! empty( $distributor_id ) ) {
//                     $distributor_ids[] = $distributor_id;
//                 }

//             }

//             update_post_meta( $product->ID, 'distributor_ids', $distributor_ids );

//             echo '<p> ' . $count . ' ' . $sku  . ' ' . $product->post_title . ' - ' . implode( ',', $distributor_ids ) . '</p>';

//             $count++;

//         }

//     }

//     die;
// });

function logout_menu_link( $items, $args ) {

    // echo ifield_debug_var( $args );

    if( ! in_array( $args->theme_location, ['ifield'] ) ) {
        return $items;
    }
    
    if( is_user_logged_in() ) {

        global $current_user;

        $items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-logout">
        <a class="elementor-item" href="'. wp_logout_url( get_bloginfo( 'url' ) ) .'">'. __( 'Log Out', 'hello-sakataifield') .'</a></li>';

    }

    return $items;
}

add_filter( 'wp_nav_menu_items', 'logout_menu_link', 10, 2 );

function force_customer_redirect_on_init() {

    global $post;

    if ( 
        ( ! is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) && 
        is_user_logged_in() &&
        in_array( $post->ID, [512,33707] )
    ) {

        global $current_user;

        if( current_user_can( 'customer' ) ) {

            $current_country = get_user_meta( $current_user->ID, 'billing_country', true );

            if( false !== strpos( get_bloginfo( 'url' ), 'sakataifield.com.br' ) ) {
                $redirect_to = 'https://sakataifield.com.br/';
            } else {
                $redirect_to = 'https://sakatadev.test/';
            }

            if( 'BR' == $current_country && 33707 == $post->ID ) {

                wp_redirect( $redirect_to . 'interativo/' );
                remove_action( 'template_redirect', 'force_customer_redirect_on_init' );
                exit;

            } elseif( 'BR' != $current_country &&  512 == $post->ID ) {

                wp_redirect( $redirect_to . 'es/interactivo/' );
                remove_action( 'template_redirect', 'force_customer_redirect_on_init' );
                exit;

            }

        }

    }

}

add_action( 'template_redirect', 'force_customer_redirect_on_init' );

function redirect_to_interactive( $redirect_to, $request, $user ) {

    if( ! is_wp_error( $user ) && user_can( $user, 'customer' ) ) {

        $current_country = get_user_meta( $user->ID, 'billing_country', true );

        if( false !== strpos( get_bloginfo( 'url' ), 'sakataifield.com.br' ) ) {
            $redirect_to = 'https://sakataifield.com.br/';
        } else {
            $redirect_to = 'https://sakatadev.test/';
        }

        $redirect_to.= 'BR' == $current_country ? 'interativo/' : 'es/interactivo/';

    }

    return $redirect_to;

}

add_filter( 'login_redirect', 'redirect_to_interactive', 10, 3 );

add_action( 'rest_api_init', function () {
    register_rest_route( 'ifield/v1', '/lectures_notifyer', array(
        'methods' => 'GET',
        'callback' => 'sakataifield_lectures_notifyer',
        'permission_callback' => function () {
            return current_user_can( 'edit_posts' );
        }
  ));
});

function sakataifield_lectures_notifyer() {

    $slice_lenght = 30;
    $interval_to_notify = 900; //600 seconds = 15 minutes
    // echo $from_date;die;
    // $from_date = current_time( 'U' );
    // update_post_meta( 30212, 'users_subscriptions', [19, 1024, 1029] );
    // delete_post_meta( 30212, 'users_notified' );
    // delete_post_meta( 30212, 'notification_completed' );
    // exit;

    $args = [
        'post_type' => 'tvifield',
        'posts_per_page' => 1,
        'post_status' => 'future',
        'order' => 'asc',
        'meta_key' => 'notification_completed',
        'meta_compare' => 'NOT EXISTS'
    ];

    $videos = get_posts( $args );

    if( ! empty( $videos ) ) {

        $next_event = $videos[0];
        $current_time = current_time( 'U' );
        $next_event_date = strtotime( $next_event->post_date );
        // $current_time = strtotime(      '2022-07-04 18:46:00' );
        // $next_event_date = strtotime(   '2022-07-04 19:00:00' );

        if( $current_time > $next_event_date ) {

            //echo 'Past event';
            update_post_meta( $next_event->ID, 'notification_completed', true );
            exit;

        } elseif( ( $next_event_date - $current_time ) <= $interval_to_notify ) {
            
            $users_subscriptions = get_field( 'users_subscriptions', $next_event->ID );
            $users_already_notified = get_field( 'users_notified', $next_event->ID );
            $users_already_notified = empty( $users_already_notified ) ? [] : $users_already_notified;
            $users_notified_now = [];
            $debug_info = [];

            $users_to_notify = array_slice( $users_subscriptions, count( $users_already_notified ), $slice_lenght );

            if( empty( $users_to_notify ) ) {

                update_post_meta( $next_event->ID, 'notification_completed', true );

            } else {

                foreach( $users_to_notify as $user_id ) {

                    $user = get_user_by( 'id', $user_id );

                    if( ! empty( $user ) ) {

                        $fields = ['first_name','title', 'datetime', 'speecher_name'];
                        $headers = ['Content-Type: text/html; charset=UTF-8'];
                        $user_country = get_user_meta( $user->ID, 'billing_country', true );
                        $user_country = empty( $user_country ) ? 'BR' : $user_country;
                        $options_key = 'BR' == $user_country ? 'options_' : 'options_es_';

                        $subject = get_option( $options_key . 'email_lecture_subject' );
                        $message = get_option( $options_key . 'email_lecture_content' );
                        $message = apply_filters( 'the_content', $message );

                        $replace_data = [];
                        $replace_data['first_name'] = get_user_meta( $user->ID, 'first_name', true );
                        $replace_data['title'] = get_the_title( $next_event->ID );
                        $replace_data['datetime'] = get_the_time( 'd/m/Y H:i', $next_event );
                        $replace_data['speecher_name'] = get_field( 'speecher_name', $next_event->ID );

                        foreach( $fields as $field_key ) {
                            $subject = str_replace( '{{' . $field_key . '}}', $replace_data[$field_key], $subject );
                            $message = str_replace( '{{' . $field_key . '}}', $replace_data[$field_key], $message );
                        }

                        wp_mail( $user->user_email, $subject, $message, $headers );

                        // $debug_info[] = [
                        //     'user_id' => $user_id,
                        //     'user_email' => $user->user_email,
                        //     'subject' => $subject,
                        //     'message' => $message
                        // ];

                    }

                    $users_notified_now[] = $user_id;

                }

                update_post_meta( $next_event->ID, 'users_notified', array_merge( $users_already_notified, $users_notified_now ) );

            }

            // print_r( [$users_notified_now, $debug_info] );

        } else {

            echo "Event `{$next_event->post_title}` is in the future";

        }

    }

}

function sakataifield_redirections() {

    if( is_tax( 'product_cat' ) || is_singular( 'product' ) ) {
        wp_redirect( get_permalink( apply_filters( 'wpml_object_id', 512, 'page', true  ) ) );
        exit;
    }

}

add_action( 'template_redirect', 'sakataifield_redirections' );

function sakataifield_fix_locale() {

    global $post;

    if ( ! empty( $post ) && in_array( $post->ID, [512,33707]) && ! is_admin() && is_user_logged_in() ) {

        $user_id = get_current_user_id();

        $current_country = get_user_meta( $user_id, 'billing_country', true );
        $country = sakataifield()->countries->get_country();

        if( $current_country != $country ) {

            if ( sakataifield()->countries->country_exists( $current_country ) ) {

                $locale = sakataifield()->countries->get_locale( $current_country );

                sakataifield()->customer->set_country( $current_country );
                sakataifield()->customer->set_locale( $locale );

                if( false !== strpos( get_bloginfo( 'url' ), 'sakataifield.com.br' ) ) {
                    $page_link = 'https://sakataifield.com.br/';
                } else {
                    $page_link = 'https://sakatadev.test/';
                }

                $page_link.= 'BR' == $current_country ? 'interativo/' : 'es/interactivo/';

                wp_redirect( $page_link );

                remove_action( 'wp', 'sakataifield_fix_locale' );

                exit;

            }

        } else {

            $locale = sakataifield()->countries->get_locale( $current_country );

            sakataifield()->customer->set_country( $current_country );
            sakataifield()->customer->set_locale( $locale );

        }

    }

}
add_action( 'wp', 'sakataifield_fix_locale' );

function my_custom_something( $query ) {

    if( isset( $_GET['action'] ) && 'elementor' == $_GET['action'] ) {
        remove_action( 'pre_get_posts', 'my_custom_something' );
        return;
    }

    if ( ! is_admin() && ! $query->is_main_query() ) {
        if( function_exists( 'sakataifield' ) && null !== sakataifield()->customer ) {
            do_action( 'wpml_switch_language', sakataifield()->customer->get_locale() );
        }
    }
}
add_action( 'pre_get_posts', 'my_custom_something' );


function sakataifield_user_update_locale( $user_id, $obj_old_user_data, $arr_user_data ) {

    if( ! ( is_admin() && ! defined( 'DOING_AJAX' ) ) ) {
        return;
    }

    $country = '';

    if( isset( $_POST['acf'] ) && is_array( $_POST['acf'] ) ) {

        foreach( $_POST['acf'] as $acf_key => $acf_value ) {

            $acf_obj = get_field_object( $acf_key );

            if( 'billing_country' == $acf_obj['name'] ) {
                $country = $acf_value;
                sakataifield()->countries->set_country( $country );
                break;
            }

        }
    }

    // $locale = 'BR' === $country ? 'pt-br' : 'es';

    // update_user_meta( $user_id, 'locale', $locale );

    remove_action( 'sakataifield_user_update_locale', 10 );

}

// add_action( 'profile_update', 'sakataifield_user_update_locale', 10, 3 );

/* [REPORTS] */

add_action( 'user_register', 'sakataifield_user_registration_report', 10, 1 );
 
function sakataifield_user_registration_report( $user_id ) {
 
    ifield_add_report_entry([
        'action' => 'create',
        'object_type' => 'user',
        'object_ID' => $user_id
    ]);
 
}

add_action( 'template_redirect', 'sakataifield_page_report', 10 );
 
function sakataifield_page_report() {

    global $post;
    
    if( is_page() ) {

        ifield_add_report_entry([
            'action' => 'view',
            'object_type' => 'page',
            'object_ID' => apply_filters( 'wpml_object_id', $post->ID, 'page', true  )
        ]);

        if( isset( $_GET['filter_exhibitor'] ) ) {

            $exhibitor_id = absint( $_GET['filter_exhibitor'] );
            
            ifield_add_report_entry([
                'action' => 'view_lechtures',
                'object_type' => 'stand',
                'object_ID' => $exhibitor_id,
                'entity' => 'exhibitor',
                'entity_ID' => $exhibitor_id
            ]);

        }

    }
 
}

/* [/REPORTS] */


add_filter( 'body_class', 'sakata_body_class' );

function sakata_body_class( $classes ) {
    return array_merge( $classes, array( 'wpml-lang-' . apply_filters( 'wpml_current_language', null ) ) );
}

add_filter( 'sakataifield_onboarding_videos', function( $videos ) {

    $customer_id = get_current_user_id();

    $distributors = sakataifield_get_distributors_for_customer( $customer_id );

    $opening_videos = get_field( 'opening_videos', 'options' );
    $user_market = ifield_get_current_user_market();

    if( ! empty( $opening_videos ) ) {

        foreach( $opening_videos as $ovideo ) {

            if( $ovideo['country'] == 'DEFAULT_' . $user_market ) {

                foreach( $ovideo['videos'] as $__videos ) {
                    $videos[] = $__videos['id'];
                }

            }

        }

    }

    if( ! empty( $distributors ) ) {

        foreach( $distributors as $distributor_id ) {
            
            $video = get_field( '_video', $distributor_id );

            if( ! empty( $video ) ) {

                $videos[] = $video;

            }

        }

    }

    $videos = array_unique( $videos );

    return $videos;

}, 1, 1 );

add_action( 'sakataifield_update_customer', function ( $customer_id ) {

    
    $user = get_user_by( 'id', $customer_id );
    $fields = ['first_name', 'last_name', 'display_name'];
    $headers = ['Content-Type: text/html; charset=UTF-8'];
    // $user_country = get_user_meta( $user->ID, 'billing_country', true );
    // $user_locale = 'BR' == $user_country ? 'pt_br' : 'es';

    $subject = get_field( 'email_pre_register_subject', 'options' );
    $message = get_field( 'email_pre_register_content', 'options' );

    // $subject = get_option( 'options_email_pre_register_subject_' . $user_locale );
    // $message = get_option( 'options_email_pre_register_content_' . $user_locale );
    $message = apply_filters( 'the_content', $message );

    $userdata = [];
    $userdata['first_name'] = get_user_meta( $user->ID, 'first_name', true );
    $userdata['last_name'] = get_user_meta( $user->ID, 'last_name', true );
    $userdata['display_name'] = $user->display_name;

    foreach( $fields as $field_key ) {
        $subject = str_replace( '{{' . $field_key . '}}', $userdata[$field_key], $subject );
        $message = str_replace( '{{' . $field_key . '}}', $userdata[$field_key], $message );
    }

    wp_mail( $user->user_email, $subject, $message, $headers );

}, 10, 1 );

add_filter( 'acf/fields/post_object/query/name=exhibitor_demo_field_products', 'sakata_filter_post_object_query_for_exhbitors', 10, 3 );

function sakata_filter_post_object_query_for_exhbitors( $args, $field, $post_id ) {

    $p = get_post( $post_id );

    $args['author'] = $p->post_author;

    return $args;

}

add_action( 'save_post', 'sakata_save_exhibitor_meta_to_product', 10, 3 );

function sakata_save_exhibitor_meta_to_product( $post_id, $post, $update ) {

    if( ! current_user_can( 'exhibitor' ) ) {
        return;
    }

    if ( ! in_array( $post->post_type, ['product'] ) ) {
        return;
    }

    if( isset( $_POST['acf']['field_623b9e858fa32'] ) ) {
        return;
    }

    $user_id = get_current_user_id();

    $exhibitor_parent = get_user_meta( $user_id, 'exhibitor_parent', true );

    if( empty( $exhibitor_parent ) ) {
        $author_id = $user_id;
    } else {
        $author_id = $exhibitor_parent;
    }

    $exhibitor_page = get_posts( 'post_type=exhibitor&posts_per_page=1&author=' . $author_id );

    if( ! empty( $exhibitor_page ) ) {

        update_post_meta( $post_id, 'exhibitor', $exhibitor_page[0]->ID );

    }

}

add_action( 'save_post', 'sakata_on_change_exhibitor_posts', 10, 3 );
 
function sakata_on_change_exhibitor_posts( $post_id, $post, $update ) {
    
    if ( ! $update ){
        return;
    }

    if( ! current_user_can( 'exhibitor' ) ) {
    	return;
    }
     
    if ( ! in_array( $post->post_type, ['product', 'exhibitor'] ) ) {
        return;
    }

    $email_subject = '';
    $email_body = '';

    $fields = [];
    $updated_fields = [];
    $changed_contents = [];
    $contents = [];
    $old_contents = get_post_meta( $post_id, 'old_contents', true );

    $post_type_labels = [
    	'product' => 'Produto',
    	'exhibitor' => 'Expositor'
    ];

    $fields['exhibitor'] = [
        'exhibitor_logo',
        'exhibitor_experts',
        'exhibitor_init_slider',
        'exhibitor_about_content',
        'exhibitor_demo_field_cta',
        'exhibitor_about_video_original',
        'exhibitor_about_slider',
        'exhibitor_products_content',
        'exhibitor_demo_field_content',
        'exhibitor_demo_field_gallery',
        'exhibitor_speeches',
        'exhibitor_stand_pic',
        'exhibitor_stand_product_pic',
        'exhibitor_stand_product_3d',
        'exhibitor_stand_video_original',
    ];

    $fields['product'] = [
        'product_content',
        'product_gallery'
    ];

    if( empty( $old_contents ) ) {

    	$old_contents = [];

    	foreach( $fields[$post->post_type] as $field_key ) {

            $field_obj = get_field_object( $field_key, $post_id );   

	    	$old_contents[$field_key] = [
	    		'key' => $field_key,
				'id' => $field_obj['key'],
				'prev_value' => '',
				'new_value' => '',
				'is_different' => 'no'
	    	];
	    }

    }

    if( isset( $fields[$post->post_type] ) ) {

    	foreach( $fields[$post->post_type] as $field_key ) {

    		$field_obj = get_field_object( $field_key, $post_id );
            $field_id = $field_obj['key'];
    				
			$is_different = $old_contents[$field_key]['new_value'] == $_POST['acf'][$field_id] ? 'no' : 'yes';

    		$contents[$field_key] = [
    			'key' => $field_key,
    			'id' => $field_id,
				'prev_value' => $old_contents[$field_key]['new_value'],
				'new_value' => $_POST['acf'][$field_id],
				'is_different' => $is_different
			];

    		if( 'yes' == $is_different ) {

    			$updated_fields[$field_key] = $field_obj ;
    			$changed_contents[] = $field_key;

    		}

    	}

    }

    if( ! empty( $changed_contents ) ) {

    	update_post_meta( $post_id, 'old_contents', $contents );

    	$saved_contents = get_field( 'exhibitor_approved_contents', $post_id );

    	foreach( $saved_contents as $i => $field_key ) {

    		if( in_array( $field_key, $changed_contents ) ) {
    			unset( $saved_contents[$i] );
    		}

    	}

    	update_field( 'exhibitor_approved_contents', $saved_contents, $post_id );

    	$headers = ['Content-Type: text/html; charset=UTF-8'];

    	$to = get_option( 'admin_email' );

    	$email_subject = "Aviso de revisão - {$post_type_labels[$post->post_type]} {$post->post_title}";	
    	
    	$email_body.= "<p>Os seguintes dados foram alterados:</p>";

    	foreach( $updated_fields as $field_key => $field_obj ) {

    		$email_body.= "<p>Nome do campo: {$field_obj['label']}</p>";

    	}

    	$item_edit_url = get_admin_url() . '/post.php?post=' . $post_id . '&action=edit';

    	$email_body.= '<p><a href="' . $item_edit_url . '">Clique aqui para editar o item</a></p>';

    	wp_mail( $to, $email_subject, $email_body, $headers );

    }
    
}

function sakata_limit_exhibitor_posts( $query ) {
    
    global $pagenow;
 
    if( 'edit.php' != $pagenow || !$query->is_admin ){
        return $query;
    }
 
    if( current_user_can( 'exhibitor' ) ) {
        
        global $user_ID;

        $query->set( 'author__in', sakata_get_related_exhibitor_users( $user_ID ) );

    }

    return $query;
}
add_filter( 'pre_get_posts', 'sakata_limit_exhibitor_posts' );

add_filter( 'ajax_query_attachments_args', 'sakata_show_current_user_attachments' );

function sakata_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if( current_user_can( 'exhibitor' ) ) {
        $query['author'] = implode( ',', sakata_get_related_exhibitor_users( $user_id ) );
    }
    return $query;
} 


add_filter( 'wp_insert_post_empty_content', 'sakata_limit_post_save', 999, 2 );

function sakata_limit_post_save( $maybe_empty, $postarr ) {

    if( 'product' != $postarr['post_type'] ) {

        return $maybe_empty;
        
    }

	$limit = 5;

	$current_user = wp_get_current_user();

    $exhibitor_parent = get_user_meta( $current_user->ID, 'exhibitor_parent', true );

    $author_id = empty( $exhibitor_parent ) ? $current_user->ID : $exhibitor_parent;

	if( current_user_can( 'exhibitor' ) ) {

		if( empty( $postarr['ID'] ) && ! ( isset( $_GET['action'] ) ) ) {

			$args = [
				'post_type' => 'product',
				'posts_per_page' => $limit + 1,
				'post_status' => ['publish', 'draft', 'trash', 'pending', 'future'],
				'author' => implode( ',', sakata_get_related_exhibitor_users( $author_id ) )
			];

			$products = new WP_Query( $args );

            // echo ifield_debug_var( $products );die;

			if( $products->post_count >= $limit ) {

				add_flash_notice( "Seu limite de cadastro de produtos foi atingido." ); 

				wp_redirect( get_admin_url() . '/edit.php?post_type=product' );

				exit;

			}

		}


	}

	return $maybe_empty;

}

/**
 * Add a flash notice to {prefix}options table until a full page refresh is done
 *
 * @param string $notice our notice message
 * @param string $type This can be "info", "warning", "error" or "success", "warning" as default
 * @param boolean $dismissible set this to TRUE to add is-dismissible functionality to your notice
 * @return void
 */
 
function add_flash_notice( $notice = "", $type = "warning", $dismissible = true ) {
    // Here we return the notices saved on our option, if there are not notices, then an empty array is returned
    $notices = get_option( "my_flash_notices", array() );
 
    $dismissible_text = ( $dismissible ) ? "is-dismissible" : "";
 
    // We add our new notice.
    array_push( $notices, array( 
            "notice" => $notice, 
            "type" => $type, 
            "dismissible" => $dismissible_text
        ) );
 
    // Then we update the option with our notices array
    update_option("my_flash_notices", $notices );
}
 
/**
 * Function executed when the 'admin_notices' action is called, here we check if there are notices on
 * our database and display them, after that, we remove the option to prevent notices being displayed forever.
 * @return void
 */
 
function display_flash_notices() {
    $notices = get_option( "my_flash_notices", array() );
     
    // Iterate through our notices to be displayed and print them.
    foreach ( $notices as $notice ) {
        printf('<div class="notice notice-%1$s %2$s"><p>%3$s</p></div>',
            $notice['type'],
            $notice['dismissible'],
            $notice['notice']
        );
    }
 
    // Now we reset our options to prevent notices being displayed forever.
    if( ! empty( $notices ) ) {
        delete_option( "my_flash_notices", array() );
    }
}
 
// We add our display_flash_notices function to the admin_notices
add_action( 'admin_notices', 'display_flash_notices', 12 );



add_action( 'user_register', 'create_exhibitor_pages_on_user_create', 10, 1 );
 
function create_exhibitor_pages_on_user_create( $user_id ) {

	if( ! ( 
		'createuser' == $_POST['action'] && 
		'exhibitor' == $_POST['role'] && 
		1 == $_POST['acf']['field_6255bcf1d3070'] ) ) {
		return;
	}

	$post_type = 'exhibitor';

	$args = [
		'post_type' => $post_type,
		'post_title' =>  $_POST['acf']['field_625ac1366646a'],
		'post_content' => '',
		'post_author' => $user_id,
		'post_status' => 'publish'
	];

	$post_id = wp_insert_post( $args );

	if( ! is_wp_error( $post_id ) && $post_id > 0 ) {

		update_post_meta( $post_id, 'exhibitor_stand_type', $_POST['acf']['field_6255bdaf6709e'] );
		update_post_meta( $post_id, 'exhibitor_logo', $_POST['acf']['field_625ab95026dc3'] );

		//Cria a traducão para o espanhol se este expositor tiver algum tipo de estande configurado
		if( 'none' != $_POST['acf']['field_625ab9c626dc5'] ) {

			$translated_post_id = wp_insert_post( $args );

			update_post_meta( $translated_post_id, 'exhibitor_stand_type', $_POST['acf']['field_625ab9c626dc5'] );
			update_post_meta( $translated_post_id, 'exhibitor_logo', $_POST['acf']['field_625ab95026dc3'] );

			$wpml_element_type = apply_filters( 'wpml_element_type', $post_type );
			$get_language_args = [
				'element_id' => $post_id, 
				'element_type' => $post_type
			];
			$original_post_language_info = apply_filters( 'wpml_element_language_details', null, $get_language_args );

			$set_language_args = array(
			    'element_id'    => $translated_post_id,
			    'element_type'  => $wpml_element_type,
			    'trid'   => $original_post_language_info->trid,
			    'language_code'   => 'es',
			    'source_language_code' => $original_post_language_info->language_code
			);
			  
			do_action( 'wpml_set_element_language_details', $set_language_args );

            //Alow user to translate own post
            $lang_pairs = [
                'pt-br' => [
                    'es' => 1
                ],
                'es' => [
                    'pt-br' => 1
                ]

            ];
            update_user_meta( $user_id, 'ifield_language_pairs', $lang_pairs );

		}

	} 
 
}

add_filter( 'wp_mail', 'new_exhibitor_custom_mail' );

function new_exhibitor_custom_mail( $args ) {

    $user = get_user_by( 'email', $args['to'] );

    update_option( 'email_etapa_1', 1 );

    if( user_can( $user->ID, 'exhibitor' ) && 
        ( 
            false !== strpos( $args['subject'], 'Datos de acceso' ) ||
            false !== strpos( $args['subject'], 'Detalhes de acesso' )
        ) 
    ) {

        update_option( 'email_etapa_2', 1 );

        $args['headers'] = ['Content-Type: text/html; charset=UTF-8'];

        $fields = ['first_name', 'last_name', 'display_name', 'access_data'];
        
        // $user_country = get_user_meta( $user->ID, 'billing_country', true );
        // $user_locale = 'BR' == $user_country ? 'pt_br' : 'es';
        //$user_locale = 'pt_br';

        // $subject = get_option( 'options_email_new_exhibitor_subject_' . $user_locale );
        // $message = get_option( 'options_email_new_exhibitor_content_' . $user_locale );

        $subject = get_field( 'email_new_exhibitor_subject', 'options' );
        $message = get_field( 'email_new_exhibitor_content', 'options' );

        $userdata = [];
        $userdata['first_name'] = get_user_meta( $user->ID, 'first_name', true );
        $userdata['last_name'] = get_user_meta( $user->ID, 'last_name', true );
        $userdata['display_name'] = $user->display_name;
        $userdata['access_data'] = $args['message'];

        foreach( $fields as $field_key ) {
            $subject = str_replace( '{{' . $field_key . '}}', $userdata[$field_key], $subject );
            $message = str_replace( '{{' . $field_key . '}}', $userdata[$field_key], $message );
        }

        $args['subject'] = $subject;
        $args['message'] = $message;
        $args['message'] = apply_filters( 'the_content', $args['message'] );

    }

    return $args;
}

add_filter( 'wpml_user_can_translate', function( $user_can_translate, $user ){
    
    if( in_array( 'exhibitor', ( array ) $user->roles, true ) && current_user_can( 'translate' ) ) {
        return true;
    }
       
    return $user_can_translate;

}, 10, 2);

function sakata_admin_body_class( $classes ) {
    
    if( current_user_can( 'exhibitor' ) ) {
        $classes .= ' user-can-exhibitor ';
    }
 
    return $classes;
}
add_filter( 'admin_body_class', 'sakata_admin_body_class' );

add_action( 'admin_footer', function(){

    if( ! current_user_can( 'exhibitor' ) ) {
        return;
    }

    ?>
        <script type="text/javascript">
            ( function( $ ) {
                $( '.user-can-exhibitor .wp-submenu a[href="edit.php?post_type=exhibitor"]' ).text( 'Editar' );
                $( '.user-can-exhibitor #menu-posts-exhibitor .wp-menu-name' ).text( 'Meu estande' );
                $( '.user-can-exhibitor div[data-name="exhibitor_stand_type"] input[type="radio"]' ).attr( 'readonly', true );
            })( jQuery );
        </script>
    <?php 

});


