<?php

add_shortcode( 'foryou', 'foryou_render' );

function foryou_render( $atts ) {

	$html = '';

	ob_start();
	
	get_template_part( 'template-parts/foryou/modal', 'foryou' );

	$html = ob_get_contents();

	ob_end_clean();

    return $html;
}

add_shortcode( 'ifield_preview', 'ifield_preview' );

function ifield_preview( $atts ) {

	global $post;

	$ver = '1.0.0';

	wp_enqueue_style( 'ifield', get_stylesheet_directory_uri() . '/assets/css/ifield.css', [], $ver );

    wp_enqueue_script( '3d-ifield-script', get_stylesheet_directory_uri() . '/3d/field/dist/script.bundle.js', [], $ver );
	wp_enqueue_script( '3d-ifield-import', get_stylesheet_directory_uri() . '/3d/field/dist/imports.bundle.js', [], $ver );

	ob_start();

	?>

	<canvas class="webgl" id="body"></canvas>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

add_shortcode( 'stand_preview', 'ifield_stand_preview' );

function ifield_stand_preview( $atts ) {

	if( ! is_user_logged_in() ) {
		return __( 'You must be logged in to see this content', 'hello-sakataifield' );
	}

	global $post;

	$ver = '1.0.0';

	wp_enqueue_style( 'ifield', get_stylesheet_directory_uri() . '/assets/css/ifield.css', [], $ver );

    wp_enqueue_script( '3d-stand-script', get_stylesheet_directory_uri() . '/3d/stand/dist/script.bundle.js', [], $ver );
    wp_enqueue_script( '3d-stand-import', get_stylesheet_directory_uri() . '/3d/stand/dist/imports.bundle.js', [], $ver );

    wp_localize_script(
		'3d-stand-script',
		'iFieldData',
		array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'wpnonce' => wp_create_nonce( 'iField' ),
			'isMobile' => wp_is_mobile()
		)
	);

	ob_start();

	?>

	<div class="exhibitor-stand-preview-msg">
		<?php _e( 'This is just a preview. Some aspects can be different in the final context.', 'hello-sakataifield' ) ?>
	</div>
	<canvas class="webgl" id="body"></canvas>
	<script type="text/javascript">
		( function( $ ){

			const script = document.createElement("script");
		    script.src = "https://embed.videodelivery.net/embed/sdk.latest.js"
		    document.head.appendChild( script );

			var stand = JSON.parse( '<?= json_encode( ifield_get_exhibitor_data( $post->ID ) ); ?>' );

			$( window ).on( 'load', function(){
				setTimeout( function(){
 					window.updateObjectsByJSON( stand );
				}, 3000 );
			});

			$( document ).on( 'click', '.ifield-stand-change-product', function( e ){

				e.preventDefault();

				var $btn = $( this );

				$( '.ifield-stand-products-product-content.active, .ifield-stand-products-product-slider.active' ).removeClass( 'active' );
				$( '.ifield-stand-products-product-content[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).addClass( 'active' );
				$( '.ifield-stand-products-product-slider[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).addClass( 'active' );

			});

			$( window ).on( 'standinterface3Dclickevent', function( e, index, action ){

				switch( action ) {
					case 'logo':

						ifieldLoadStandContent( 'about' );

						break;
					case 'main_img':

						ifieldLoadStandContent( 'demo-field' );

						break;
					case 'tv_img':

						ifieldLoadStandContent( 'video' );

						break;
					case 'specialist_img':

						window.open( stand.specialist_chat_url, '_blank' );

						break;
					default: //main_product

						ifieldLoadStandContent( 'products' );

						break;
				}

			});

			$( document ).on( 'click', '[data-load-content]', function( e ){

				e.preventDefault();

				var $btn = $( this );

				$( '#standModal .modal-body-content' ).addClass( 'loading' );
				$( 'body' ).addClass( 'ifield-loading' );

				$.ajax({
		            type: 'post',
		            url: iFieldData.ajaxurl,
		            data: {
		                '_ajax_nonce': iFieldData.wpnonce,
		                'post_id': $btn.data( 'stand' ),
		                'content' : $btn.data( 'load-content' ),
		                'bypass_approved_contents' : 1,
		                'action': 'sakata_ajax_stand_load_content'
		            },
		            success : function( content ) {

		            	$( '.modal-body' ).scrollTop( 0 );

		                $( '#standModal .modal-body-content' ).removeClass( 'loading' ).html( content );

		                $( '.ifield-stand-swiper' ).each( function(){

		                	var $swiperContainer = $( this );
		                	var swiperConfig = typeof $swiperContainer.data( 'swiper' ) === 'undefined' ? {} : $swiperContainer.data( 'swiper' );
		                	var swiperDefaults = {
								navigation: {
							    	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
							    	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
							  	}
							};

		                	var swiper = new Swiper( $swiperContainer.get( 0 ), {...swiperConfig, ...swiperDefaults } );
		                });

		                $( '.ifield-swiper-gallery-container' ).each( function(){

		                	var $swiperContainer = $( this );

		                	var swiper = new Swiper( '.ifield-swiper-gallery-thumbs', {
						        spaceBetween: 10,
						        slidesPerView: 4,
						        freeMode: true,
						        watchSlidesProgress: true,
						    });
						    var swiper2 = new Swiper( '.ifield-swiper-gallery', {
						        spaceBetween: 10,
						        navigation: {
						        	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
						          	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
						        },
						        thumbs: {
						        	swiper: swiper,
						        },
						    });
		                });

		                const swiper = new Swiper( '.ifield-stand-about-swiper', {
							navigation: {
						    	nextEl: '.swiper-button-next',
						    	prevEl: '.swiper-button-prev',
						  	}
						});

		                if( typeof $btn.data( 'product-id' ) !== 'undefined' ) {

		                	$( '.ifield-stand-change-product[data-product-id="' + $btn.data( 'product-id' ) + '"]' ).trigger( 'click' );

		                }

		                $( '#standModal .add_to_wishlist_button' ).tooltip();

		                $( 'body' ).removeClass( 'ifield-loading' );

		            }
		        });

			});

			function ifieldLoadStandContent( content ) {

				$( '#standModal' ).modal( 'dispose' ).remove();
				$( 'body' ).addClass( 'ifield-loading' );

				xhr = $.ajax({
		            type: 'post',
		            url: iFieldData.ajaxurl,
		            data: {
		                '_ajax_nonce': iFieldData.wpnonce,
		                'post_id': <?= $post->ID ?>,
		                'content' : content,
		                'bypass_approved_contents' : 1,
		                'action': 'sakata_ajax_stand_load'
		            },
		            success : function( content ) {

		            	$( '#standModal' ).modal( 'dispose' ).remove();

		            	$( 'body' ).append( content );

		            	$( '.ifield-stand-swiper' ).each( function(){

		                	var $swiperContainer = $( this );
		                	var swiperConfig = typeof $swiperContainer.data( 'swiper' ) === 'undefined' ? {} : $swiperContainer.data( 'swiper' );
		                	var swiperDefaults = {
								navigation: {
							    	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
							    	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
							  	}
							};

		                	var swiper = new Swiper( $swiperContainer.get( 0 ), {...swiperConfig, ...swiperDefaults } );
		                });

		                $( '.ifield-swiper-gallery-container' ).each( function(){

		                	var $swiperContainer = $( this );

		                	var swiper = new Swiper( '.ifield-swiper-gallery-thumbs', {
						        spaceBetween: 10,
						        slidesPerView: 4,
						        freeMode: true,
						        watchSlidesProgress: true,
						    });
						    var swiper2 = new Swiper( '.ifield-swiper-gallery', {
						        spaceBetween: 10,
						        navigation: {
						        	nextEl: $swiperContainer.find( '.swiper-button-next' ).get( 0 ),
						          	prevEl: $swiperContainer.find( '.swiper-button-prev' ).get( 0 ),
						        },
						        thumbs: {
						        	swiper: swiper,
						        },
						    });
		                });

		            	const swiper = new Swiper( '.ifield-stand-about-swiper', {
							navigation: {
						    	nextEl: '.swiper-button-next',
						    	prevEl: '.swiper-button-prev',
						  	}
						});

						$( '#standModalPreview' ).modal( 'hide' );

		                $( '#standModal' ).modal( 'show' );

		                $( '#standModal .add_to_wishlist_button' ).tooltip();

		                $( 'body' ).removeClass( 'ifield-loading' );

		            }
		        });
		    }

		})( jQuery );
		
	</script>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

add_shortcode( 'ifield_cloud_stream', 'ifield_cs_video_render' );

function ifield_cs_video_render( $atts ) {

	$a = shortcode_atts( array(
		'id' => '',
		'start_time' => 0,
		'preload' => 1,
		'controls' => 1,
		'autoplay' => 1,
		'muted' => 0,
		'loop' => 0,
		'html_id' => 'stream-player'
	), $atts );

	$video_src = 'https://iframe.videodelivery.net/';
	$video_src.= $a['id'];
	$video_src.= '?startTime=' . $a['start_time'];
	$video_src.= '&preload=' . $a['preload'];
	$video_src.= '&controls=' . $a['controls'];
	$video_src.= '&autoplay=' . $a['autoplay'];

	if( 1 == $a['muted'] ) {
		$video_src.= '&muted=' . $a['muted'];
	}

	if( 1 == $a['loop'] ) {
		$video_src.= '&loop=' . $a['loop'];
	}

	ob_start();

	?>

	<div class="video-player cs_video_render embed-responsive embed-responsive-16by9">
		<iframe
	        id="<?= $a['html_id'] ?>"
	        class="embed-responsive-item"
	        src="<?= $video_src; ?>"
	        style="border: none"
	        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
	        allowfullscreen="true"
	        data-js="video-iframe"
	    ></iframe>
	</div>

	<?php

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

add_shortcode( 'ifield_stand_modal', 'ifield_stand_modal_render' );

function ifield_stand_modal_render( $atts ) {

	$a = shortcode_atts( array(
		'id' => 0,
		'content' => 'about',
		'base' => 'base',
		'bypass_approved_contents' => 0
	), $atts );


	if( empty( $a['id'] ) ) {
		return ifield_render_template( 'template-parts/stands/modal', 'base' );
	}

	$html = '';
	$logo = '<img class="ifield-empty-logo" src="' . get_stylesheet_directory_uri() . '/assets/img/logo-placeholder.svg" width="108" height="108"/>';

	$stand = ifield_get_stand_data( $a['id'] );

	if( ! empty( $a['bypass_approved_contents'] ) ) {
		$stand['meta']['exhibitor_approved_contents'] = ifield_get_exhibitor_data_approvation_keys();
	}

	if( in_array( 'exhibitor_logo', $stand['meta']['exhibitor_approved_contents'] ) ) {
		$logo = wp_get_attachment_image( $stand['meta']['exhibitor_logo'], 'medium' );
	}
	

	$replaces = [
		'body' => ifield_render_template( 'template-parts/stands/content', $a['content'], $stand ),
		'title' => __( 'Stands', 'hello-sakataifield' ),
		'subtitle' => get_the_title( $stand['ID'] ),
		'logo' => $logo,
		'class' => 'ifield-stand-type-' . $stand['meta']['exhibitor_stand_type']
	];

	ob_start();

	get_template_part( 'template-parts/stands/modal', $a['base'] );

	$html = ob_get_contents();

	ob_end_clean();

	foreach( $replaces as $key => $value ) {
		$key = '%' . strtoupper( $key ) . '%';
		$html = str_replace( $key, $value, $html );
	}

    return $html;

}



add_shortcode( 'ifield', 'ifield_render' );

function ifield_render( $atts ) {

	$html = '';

	ob_start();

	?>

	<?php if( is_dev_mode() ) : ?>
		<div id="field-placeholder"></div>
	<?php else: ?>
		<canvas class="webgl" id="body"></canvas>
	 	<div id="modal" class="modal" style="opacity: 0; transform: translateX(-400px);"></div>
	<?php endif; ?>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

    return $html;

}

add_shortcode( 'ifield_header', 'ifield_header' );

function ifield_header( $atts ) {

	$html = '';

	ob_start();

	?>
	<div id="ifield-interactive-header">
		<div id="navbar-mobile">
			<nav id="ifield-logo">
				<?= get_custom_logo() ?>
			</nav>
			<nav id="ifield-nav">
				<?= ifield_interactive_menu_widget(); ?>
				<?= ifield_wishlist_widget(); ?>
			</nav>
		</div>
		<a class="return-btn" href="#"></a>
	</div>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

    return $html;

}

add_shortcode( 'sakatito', 'sakatito_func' );

function sakatito_func( $atts ) {

	$html = '';

	ob_start();

	?>

	<nav id="ifield-sakatito-help">
		<?= ifield_sakatito_widget(); ?>
	</nav>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

    return $html;

}

add_shortcode( 'tvifield', 'tvifield_render' );

function tvifield_render( $atts ) {

	$html = '';

	ob_start();

	?>

	<section id="load-page">
		<?= tv_ifield_load_page();?>
	</section>

	<section id="modal-tvifield">
		<?= get_template_part('template-parts/tvifield/modal','tvifield');?>
	</section>

	<section id="modal-agenda-tvifield">
		<?= get_template_part('template-parts/tvifield/modal','agenda');?>
	</section>	
	
	<?php 

	$html = ob_get_contents();

	ob_end_clean();

    return $html;

}