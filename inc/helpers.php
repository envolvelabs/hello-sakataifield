<?php

function ifield_get_user_missing_data( $user_id = 0 ) {

	$missing_data = [];

	$user_id = empty( $user_id ) ? get_current_user_id() : $user_id;

	$customer_data = [
		'billing_country' => get_user_meta( $user_id, 'billing_country', true ),
		'billing_city' => get_user_meta( $user_id, 'billing_city', true ),
		'billing_state' => get_user_meta( $user_id, 'billing_state', true ),
		'billing_region' => get_user_meta( $user_id, 'billing_region', true ),
		'species' => get_user_meta( $user_id, 'species', true ),
	];

	if( 'BR' == $customer_data['billing_country'] ) {

		// if( empty( $customer_data['billing_city'] ) ) {
		// 	$missing_data[] = 'billing_city';
		// 	$missing_data[] = 'missing_address';
		// }

		if( empty( $customer_data['billing_state'] ) ) {
			$missing_data[] = 'billing_state';
			$missing_data[] = 'missing_address';
		}

	} else {	

		$country_id = get_term_by( 'slug', strtolower( $customer_data['billing_country'] ), 'distributor_place');

		if( ! empty( $country_id ) ) {
			$country_id = $country_id->term_id;
		}

		$choices = get_terms([
			'taxonomy' => 'distributor_place',
			'parent' => $country_id
		]);

		if( empty( $customer_data['billing_region'] ) && ! empty( $choices ) ) {
			$missing_data[] = 'billing_region';
			$missing_data[] = 'missing_address';
		}

	}

	if( empty( $customer_data['species'] ) ) {
		$missing_data[] = 'species';
	}

	return array_unique( $missing_data );

}

function ifield_remove_report_entry( $data ) {

	global $post;

	$default_data = [
		'action' => 'view',
		'object_type' => empty( $post ) ? 'page' : $post->post_type,
		'object_ID' => empty( $post ) ? 0 : $post->ID,
		'entity' => 'sakata',
		'entity_ID' => 0,
		'user_ID' => is_user_logged_in() ? get_current_user_id() : 'anonymous'
	];

	$data = wp_parse_args( $data, $default_data );

	$meta_query = [];

	foreach( $data as $key => $value ) {
		$meta_query[] = [
			'key' => $key,
			'value' => $value
		];
	}

	$skip_args = [
		'post_type' => 'report',
		'post_status' => 'publish',
		'posts_per_page' => 1,
		'meta_query' => [
			'relation' => 'AND',
			$meta_query
		]
	];

	$report = get_posts( $skip_args );

	if( empty( $report ) ) {
		
		return false;

	} else {

		return wp_delete_post( $report[0]->ID, true );

	}	


}

function ifield_add_report_entry( $data, $skip_duplicated = false ) {

	global $post;

	$default_data = [
		'action' => 'view',
		'object_type' => empty( $post ) ? 'page' : $post->post_type,
		'object_ID' => empty( $post ) ? 0 : $post->ID,
		'entity' => 'sakata', //sakata or exhibitor, default: sakata
		'entity_ID' => 0, //exhibitor_ID 
		'user_ID' => is_user_logged_in() ? get_current_user_id() : 'anonymous'
	];

	$data = wp_parse_args( $data, $default_data );


	if( ! in_array( $data['entity'], ['sakata', 'exhibitor'] ) ) {
		return new WP_Error( 'failed', __( 'Entity must be either sakata or exhibitor', 'hello-sakataifield' ) );
	}

	if( 'exhibitor' == $data['entity'] && empty( $data['entity_ID'] ) ) {
		return new WP_Error( 'failed', __( 'Must provide an entity_ID when entity is exhibitor', 'hello-sakataifield' ) );
	}

	if( 'page' == $data['object_type'] && empty( $data['object_ID'] ) ) {
		return new WP_Error( 'failed', __( 'Page must be greather than 0', 'hello-sakataifield' ) );
	}

	if( $skip_duplicated ) {

		$meta_query = [];

		foreach( $data as $key => $value ) {
			$meta_query[] = [
				'key' => $key,
				'value' => $value
			];
		}

		$skip_args = [
			'post_type' => 'report',
			'post_status' => 'publish',
			'posts_per_page' => 1,
			'meta_query' => [
				'relation' => 'AND',
				$meta_query
			]
		];

		$report = get_posts( $skip_args );

		if( ! empty( $report ) ) {
			return $report[0]->ID;
		}

	}

	$args = [
		'post_type' => 'report',
		'post_status' => 'publish',
		'post_author' => 1
	];

	$report_ID = wp_insert_post( $args );

	if( ! is_wp_error( $report_ID ) && ! empty( $report_ID ) ) {


		update_post_meta( $report_ID, 'action', $data['action'] );
		update_post_meta( $report_ID, 'object_type', $data['object_type'] );
		update_post_meta( $report_ID, 'object_ID', $data['object_ID'] );
		update_post_meta( $report_ID, 'entity', $data['entity'] );
		update_post_meta( $report_ID, 'entity_ID', $data['entity_ID'] );
		update_post_meta( $report_ID, 'user_ID', $data['user_ID'] );

	}

	return $report_ID;

}

function ifield_get_current_user_market() {

	$user_id = get_current_user_id();

	$country = get_user_meta( $user_id, 'billing_country', true );

	if( 'BR' == $country ) {
		return 'MI';
	} else {
		return 'ME';
	}

}

function ifield_get_page_link( $type ) {

	switch ( $type ) {
		case 'ifield':
			$page_id = get_field( 'page_ifield', 'options' );
			break;
		case 'tvifield':
			$page_id = get_field( 'page_tv_ifield', 'options' );
			break;
		default:
			$page_id = get_field( 'page_ifield', 'options' );
			break;
	}

	if( is_object( $page_id ) ) {
		$page_id = $page_id->ID;
	}

	$page_id = apply_filters( 'wpml_object_id', $page_id, 'page', true );

	if( empty( $page_id ) ) {
		return get_permalink();
	} else {
		return get_permalink( $page_id );
	}

}

function ifield_get_stations_map(){

	$map = [];

	$stations = get_field( 'ifield_stations_3d', 'options' );

	if( ! empty( $stations ) ) {

		foreach( $stations as $index => $s ) {

			$station = get_post( $s['station'] );

			$url = get_permalink( $station->ID );
			$url = str_replace( '#station/', '#estacao/', $url );

			$data = [
				'index' => $index,
				'id' => $station->ID,
				'slug' => $station->post_name,
				'title' => get_the_title( $station->ID ),
				'url_frament' => parse_url( $url )['fragment']
			];

			$map['slug_' . $station->post_name] = $data;
			$map['index_' . $index] = $data;
			$map['id_' . $station->ID] = $data;

		}

	}

	return $map;

}

function ifield_get_stands_map(){

	$map = [];

	$exhibitors_gold = get_field( 'ifield_exhibitors_gold', 'options' );
	$exhibitors_silver = get_field( 'ifield_exhibitors_silver', 'options' );

	if( ! empty( $exhibitors_gold ) ) {

		foreach( $exhibitors_gold as $index => $e ) {

			$exhibitor = get_post( $e['exhibitor'] );

			$data = [
				'index' => $index,
				'id' => $exhibitor->ID,
				'slug' => $exhibitor->post_name,
				'title' => get_the_title( $exhibitor->ID ),
				'type' => 'gold',
				'stand' => ifield_get_exhibitor_data( $exhibitor->ID )
			];

			$map['slug_' . $exhibitor->post_name] = $data;
			$map['index_' . $index] = $data;
			$map['id_' . $exhibitor->ID] = $data;

		}

	}

	if( ! empty( $exhibitors_silver ) ) {

		foreach( $exhibitors_silver as $index => $e ) {

			$exhibitor = get_post( $e['exhibitor'] );

			$real_index = $index + count( $exhibitors_gold );

			$data = [
				'index' => $real_index,
				'id' => $exhibitor->ID,
				'slug' => $exhibitor->post_name,
				'title' => get_the_title( $exhibitor->ID ),
				'type' => 'silver',
				'stand' => ifield_get_exhibitor_data( $exhibitor->ID )
			];

			$map['slug_' . $exhibitor->post_name] = $data;
			$map['index_' . $real_index] = $data;
			$map['id_' . $exhibitor->ID] = $data;

		}

	}

	return $map;

}

function ifield_get_exhibitor_data_approvation_keys() {

	$keys = [
		'exhibitor_stand_on',
		'exhibitor_logo',
		'exhibitor_main_color',
		'exhibitor_experts',
		'exhibitor_init_slider',
		'exhibitor_about_content',
		'exhibitor_demo_field_cta',
		'exhibitor_about_video_original',
		'exhibitor_about_video_cover',
		'exhibitor_about_slider',
		'exhibitor_show_products_gallery',
		'exhibitor_products_content',
		'exhibitor_demo_field_content',
		'exhibitor_demo_field_gallery',
		'exhibitor_speeches',
		'exhibitor_stand_on',
		'exhibitor_stand_pic',
		'exhibitor_stand_tv_pic',
		'exhibitor_stand_product_pic',
		'exhibitor_stand_product_3d',
		'exhibitor_stand_video_original',
		'exhibitor_stand_video_cover',
	];

	return $keys;

}	

function ifield_get_exhibitor_data( $exhibitor_id ) {

	$data = [];

	// $placeholder_img = get_stylesheet_directory_uri() . '/assets/img/logo-placeholder.svg';
	$placeholder_img = '';

	$customer_data = sakataifield_customer_data();
    $whatsapp_text = __( 'My name is %s and I am from %s, I would like to talk to an expert', 'hello-sakataifield' ); 
    $whatsapp_text = sprintf( 
        $whatsapp_text, 
        $customer_data['first_name'],
        $customer_data['city']['formatted'],
    ); 

	$stand = ifield_get_stand_data( $exhibitor_id );

	$stand['meta']['exhibitor_approved_contents'] = ifield_get_exhibitor_data_approvation_keys();

	// if( in_array( 'exhibitor_stand_on', $stand['meta']['exhibitor_approved_contents'] ) ) {

		if( in_array( $stand['meta']['exhibitor_stand_type'], ['gold', 'silver'] ) ) {

			if( in_array( 'exhibitor_logo', $stand['meta']['exhibitor_approved_contents'] ) ) {
				$logo = wp_get_attachment_image_src( $stand['meta']['exhibitor_logo'], 'medium' );
				$logo = $logo[0];
			} else {
				$logo = $placeholder_img;
			}

			if( in_array( 'exhibitor_main_color', $stand['meta']['exhibitor_approved_contents'] ) ) {
				$main_color_hex = $stand['meta']['exhibitor_main_color'];
			} else {
				$main_color_hex = '#ED3A37';
			}

			if( in_array( 'exhibitor_stand_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
				$main_img = $stand['meta']['exhibitor_stand_pic']['sizes']['large'];
			} else {
				$main_img = $placeholder_img;
			}

			if( in_array( 'exhibitor_stand_tv_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
				$tv_img = $stand['meta']['exhibitor_stand_tv_pic']['sizes']['large'];
			} else {
				$tv_img = $placeholder_img;
			}

			if( in_array( 'exhibitor_experts', $stand['meta']['exhibitor_approved_contents'] ) && ! empty( $stand['meta']['exhibitor_experts'][0]['pic'] ) ) {
				$specialist_img = $stand['meta']['exhibitor_experts'][0]['pic']['sizes']['medium'];
				$chat_url = 'https://api.whatsapp.com/send?phone=' . $stand['meta']['exhibitor_experts'][0]['whatsapp'] . '&text=' . rawurlencode( $whatsapp_text );
				$specialist_name = $stand['meta']['exhibitor_experts'][0]['name'];
				$specialist_position = $stand['meta']['exhibitor_experts'][0]['position'];
			} else {
				$specialist_img = $placeholder_img;
				$specialist_name = '';
				$specialist_position = '';
				$chat_url = '';
			}

			if( in_array( 'exhibitor_stand_product_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
				$product_img = $stand['meta']['exhibitor_stand_product_pic']['sizes']['large'];
			}

			if( empty( $product_img ) ) {
				$product_img = '';
			}

			if( empty( $stand['products'] ) ) {
				$product_img = '';
			}

			$data = [
				'id' => $exhibitor_id,
	            'logo' => $logo,
	            'main_color_hex' => '0x' . ltrim( $main_color_hex, '#' ),
	            'main_img' => $main_img,
	            'tv_img' => $tv_img,
	            'tv_video_url' => 'https://sakataifield.com.br',
	            'specialist_img' => $specialist_img,
	            'specialist_chat_url' => $chat_url,
	            "specialist_message" => '<span class="specialist-message">' . __( 'You will be talking to:', 'hello-sakataifield' ) . '</span>',
				"specialist_name" => '<span class="specialist-name">' . $specialist_name . '</span>',
				"specialist_position" => '<span class="specialist-position">' . $specialist_position . '</span>',
	            'product_img' => $product_img,
	            'main_product' => $product_img
			];

		}

	// }

	return $data;

}

function ifield_get_stands_data() {

	$data = [];

	$data['hoverEvent_about_text'] = __( 'Click to know more', 'hello-sakataifield' );
	$data['hoverEvent_demField_text'] = __( 'Click to see the demonstration field', 'hello-sakataifield' );

	$data['gold_stands'] = [];
	$data['silver_stands'] = [];

	$exhibitors = [];

	$exhibitors_gold = get_field( 'ifield_exhibitors_gold', 'options' );
	$exhibitors_silver = get_field( 'ifield_exhibitors_silver', 'options' );

	// echo ifield_debug_var( [$exhibitors_gold, $exhibitors_silver] );die;

	if( ! empty( $exhibitors_gold ) ) {
		foreach( $exhibitors_gold as $k => $e ) {
			$exhibitors_gold[$k]['exhibitor_type'] = 'gold';
		}
		$exhibitors = array_merge( $exhibitors, $exhibitors_gold );
	}

	if( ! empty( $exhibitors_silver ) ) {
		foreach( $exhibitors_silver as $k => $e ) {
			$exhibitors_silver[$k]['exhibitor_type'] = 'silver';
		}
		$exhibitors = array_merge( $exhibitors, $exhibitors_silver );
	}

	// $placeholder_img = get_stylesheet_directory_uri() . '/assets/img/logo-placeholder.svg';
	$placeholder_img = '';

	$customer_data = sakataifield_customer_data();
    $whatsapp_text = __( 'My name is %s and I am from %s, I would like to talk to an expert', 'hello-sakataifield' ); 
    $whatsapp_text = sprintf( 
        $whatsapp_text, 
        $customer_data['first_name'],
        $customer_data['city']['formatted'],
    ); 



	if( ! empty( $exhibitors ) ) {

		foreach( $exhibitors as $exhibitor_key => $exhibitor ) {

			// echo ifield_debug_var( $exhibitor );die;

			if( ! empty( $exhibitor['is_placeholder'] ) ) {

				$data_key = $exhibitor['exhibitor_type'] . '_stands';

				$data[$data_key][] = [
					'id' => $exhibitor_key,
		            'logo' => 'default',
		            'main_color_hex' => '',
		            'main_img' => '',
		            'tv_img' => '',
		            'tv_video_url' => '',
		            'specialist_img' => '',
		            'specialist_chat_url' => '',
		            "specialist_message" => '',
					"specialist_name" => '',
					"specialist_position" => '',
		            'product_img' => '',
		            'main_product' => ''
				];
				continue;

			} else {
		
				$stand = ifield_get_stand_data( $exhibitor['exhibitor'] );

				// if( in_array( 'exhibitor_stand_on', $stand['meta']['exhibitor_approved_contents'] ) ) {

				if( in_array( $stand['meta']['exhibitor_stand_type'], ['gold', 'silver'] ) ) {

					$data_key = $stand['meta']['exhibitor_stand_type'] . '_stands';

					// if( null == $stand['meta']['exhibitor_approved_contents'] ) {
					// 	echo ifield_debug_var( $stand );die;
					// }

					$product_img = '';

					if( in_array( 'exhibitor_logo', $stand['meta']['exhibitor_approved_contents'] ) ) {
						$logo = wp_get_attachment_image_src( $stand['meta']['exhibitor_logo'], 'medium' );
						$logo = $logo[0];
					} else {
						$logo = $placeholder_img;
					}

					if( in_array( 'exhibitor_main_color', $stand['meta']['exhibitor_approved_contents'] ) ) {
						$main_color_hex = $stand['meta']['exhibitor_main_color'];
					} else {
						$main_color_hex = '#ED3A37';
					}

					if( in_array( 'exhibitor_stand_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
						$main_img = $stand['meta']['exhibitor_stand_pic']['sizes']['large'];
					} else {
						$main_img = $placeholder_img;
					}

					if( in_array( 'exhibitor_stand_tv_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
						$tv_img = $stand['meta']['exhibitor_stand_tv_pic']['sizes']['large'];
					} else {
						$tv_img = $placeholder_img;
					}

					if( in_array( 'exhibitor_experts', $stand['meta']['exhibitor_approved_contents'] ) && ! empty( $stand['meta']['exhibitor_experts'][0]['pic'] ) ) {
						$specialist_img = $stand['meta']['exhibitor_experts'][0]['pic']['sizes']['medium'];
						$chat_url = 'https://api.whatsapp.com/send?phone=' . $stand['meta']['exhibitor_experts'][0]['whatsapp'] . '&text=' . rawurlencode( $whatsapp_text );
						$specialist_name = $stand['meta']['exhibitor_experts'][0]['name'];
						$specialist_position = $stand['meta']['exhibitor_experts'][0]['position'];
					} else {
						$specialist_img = $placeholder_img;
						$specialist_name = '';
						$specialist_position = '';
						$chat_url = '';
					}

					if( in_array( 'exhibitor_stand_product_pic', $stand['meta']['exhibitor_approved_contents'] ) ) {
						$product_img = $stand['meta']['exhibitor_stand_product_pic']['sizes']['large'];
					} 

					if( empty( $product_img ) ) {
						$product_img = $placeholder_img;
					}

					if( empty( $stand['products'] ) ) {
						$product_img = '';
					}

					$data[$data_key][] = [
						'id' => $exhibitor_key,
			            'logo' => $logo,
			            'main_color_hex' => '0x' . ltrim( $main_color_hex, '#' ),
			            'main_img' => $main_img,
			            'tv_img' => $tv_img,
			            'tv_video_url' => 'https://sakataifield.com.br',
			            'specialist_img' => $specialist_img,
			            'specialist_chat_url' => $chat_url,
			            "specialist_message" => '<span class="specialist-message">' . __( 'You will be talking to:', 'hello-sakataifield' ) . '</span>',
						"specialist_name" => '<span class="specialist-name">' . $specialist_name . '</span>',
						"specialist_position" => '<span class="specialist-position">' . $specialist_position . '</span>',
			            'product_img' => $product_img,
			            'main_product' => $product_img,
			            'exhibitor' => $exhibitor['exhibitor']
					];


				} else {

					continue;

				}
			}

			// }

		}


	}

	// echo ifield_debug_var( $data );die;

	return $data;

}

function ifield_get_stand_data( $post_id ) {

	$stand = (array) get_post( $post_id );
	$stand['meta'] = [];
	$stand['products'] = [];
	$stand['tvifield'] = [];

	$metas = [
		'exhibitor_stand_type',
		'exhibitor_logo',
		'exhibitor_main_color',
		'exhibitor_experts',
		'exhibitor_has_intro_video',
		'exhibitor_intro_video',
		'exhibitor_init_slider',
		'exhibitor_about_content',
		'exhibitor_demo_field_cta',
		'exhibitor_has_about_video',
		'exhibitor_about_video',
		'exhibitor_about_video_original',
		'exhibitor_about_video_cover',
		'exhibitor_about_slider',
		'exhibitor_products_content',
		'exhibitor_show_products_gallery',
		'exhibitor_demo_field_content',
        'exhibitor_demo_field_gallery',
		'exhibitor_speeches',
		'exhibitor_stand_pic',
		'exhibitor_stand_tv_pic',
        'exhibitor_stand_product_pic',
        'exhibitor_stand_product_3d',
        'exhibitor_stand_video',
        'exhibitor_stand_video_original',
        'exhibitor_stand_video_cover',
		'exhibitor_approved_contents',
	];

	$product_metas = [
		'_sku',
		'_trade',
		'_trade_es',
		'_species',
		'_distributor_ids',
		'product_content',
		'product_gallery',
		'exhibitor',
		'exhibitor_approved_contents',
	];

	$tvifield_metas = [
		'users_subscriptions',
		'video_url',
		'video_duration',
		'speecher_name',
		'speecher_thumbnail',
		'expert',
		'tax',
	];

	foreach( $metas as $meta_key ) {
		$stand['meta'][$meta_key] = get_field( $meta_key, $post_id );
	}

	if( ! empty( $stand['meta']['exhibitor_experts'] ) ) {

		foreach( $stand['meta']['exhibitor_experts'] as $key => $value ) {
			$stand['meta']['exhibitor_experts'][$key]['whatsapp'] = sakataifield_parse_whatsapp( $stand['meta']['exhibitor_experts'][$key]['whatsapp'] );
		}

	}

	if( empty( $stand['meta']['exhibitor_approved_contents'] ) ) {
		$stand['meta']['exhibitor_approved_contents'] = [];
	}

	$args = [
		'post_type' => 'product',
		'posts_per_page' => 5,
		'orderby' => 'meta_value_num title',
		'order' => 'ASC',
		//'author' => $stand['post_author'],
		'author' => implode( ',', sakata_get_related_exhibitor_users( $stand['post_author'] ) ),
		'meta_key' => 'product_priority'
	];

	$products = get_posts( $args );

	if( ! empty( $products ) ) {
		
		foreach( $products as $product ) {

			$stand['products'][$product->ID] = (array) $product;
			$stand['products'][$product->ID]['meta'] = [];

			foreach( $product_metas as $meta_key ) {
				$stand['products'][$product->ID]['meta'][$meta_key] = get_field( $meta_key, $product->ID );
			}

		}

	}

	$video_args = [
		'post_type' => 'tvifield',
		'posts_per_page' => 1,
		'post_status' => 'future',
		'order' => 'ASC',
		'meta_key' => 'exhibitor_id',
		'meta_value' => $post_id
	];

	$videos = get_posts( $video_args );

	if( ! empty( $videos ) ) {

		foreach( $videos as $video ) {

			$stand['tvifield'][$video->ID] = (array) $video;
			$stand['tvifield'][$video->ID]['meta'] = [];

			foreach( $tvifield_metas as $meta_key ) {
				$stand['tvifield'][$video->ID]['meta'][$meta_key] = get_field( $meta_key, $video->ID );
			}

		}

	}

	$stand['links'] = [
		'lectures' => ifield_get_page_link( 'tvifield' ) . '?filter_exhibitor=' . $post_id
	];

	return apply_filters( 'ifield_get_stand_data', $stand );

}

function sakata_get_related_exhibitor_users( $user_id ) {

    $users_ids = [];

    $exhibitor_parent = get_user_meta( $user_id, 'exhibitor_parent', true );

    if( empty( $exhibitor_parent ) ) {
        $query_user_id = $user_id;
    } else {
        $query_user_id = $exhibitor_parent;
    }

    $users_ids[] = $query_user_id;

    $related = get_users( 'meta_key=exhibitor_parent&meta_value=' . $query_user_id );

    if( ! empty( $related ) ) {
        foreach( $related as $u ) {
            $users_ids[] = $u->ID;
        }
    }

    return $users_ids;
}

function ifield_expert_whatsapp( $whatsapp_text, $phone, $pic_src, $pic_width, $pic_height ) {

	$html = '';

	$whatsapp_link = 'https://api.whatsapp.com/send?phone=' . $phone . '&text=' . rawurlencode( $whatsapp_text );

	$html.= '<a target="_blank" href="' . $whatsapp_link . '" class="ifield-btn-expert">';

	$html.= '<button class="btn btn-primary" type="button">';

	$html.= '<span class="ifield-expert-image"><img src="' . $pic_src . '" width="' . $pic_width . '" height="' . $pic_height . '"></span>';

	$html.= '<span class="ifield-expert-text">' . __( 'Talk to expert', 'hello-sakataifield' ) . '</span>';

	$html.= '</button>';

	$html.= '</a>';

	return $html;

}	

function ifield_render_stand_content( $stand_id, $content = 'about', $bypass_approved_content = 0 ) {

	$html = '';
	
	$stand = ifield_get_stand_data( $stand_id );

	if( $bypass_approved_content ) {
		$stand['meta']['exhibitor_approved_contents'] = ifield_get_exhibitor_data_approvation_keys();
	}

	ob_start();

	get_template_part( 'template-parts/stands/content', $content, $stand );

	$html = ob_get_contents();

	ob_end_clean();

    return $html;

}

function ifield_debug_var( $var ) {

	ob_start();

	echo '<pre>';

	print_r( $var );

	echo '</pre>';

	$content = ob_get_contents();

	ob_end_clean();

	return $content;

}

function ifield_render_template() {
		
	ob_start();

	call_user_func_array( 'get_template_part', func_get_args() );

	$content = ob_get_contents();

	ob_end_clean();

	return $content;

}

function ifield_spinner( $width = 200, $height = 200, $type = 'svg' ) {

	return '<img 
				src="' . get_stylesheet_directory_uri() . '/assets/img/spinner.' . $type . '" 
				width="' . $width . '" 
				height="' . $height . '" 
				alt="Loading">';

}

function ifield_interactive_menu_widget() {

	ob_start();

	?>

	<nav class="ifield-interactive-menu ifield-widget-menu">
		<div class="ifield-widget-menu-wrapper">
			<button class="ifield-interactive-menu-btn">
				<img class="ifield-menu-open" alt="menu" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/menu.svg" width="61" height="61">
				<img class="ifield-menu-back" alt="menu" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/chevron-right-menu.svg" width="52" height="52">
				<img class="ifield-menu-close" alt="menu" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/x.svg" width="52" height="52">
			</button>
			<?php wp_nav_menu( 'theme_location=ifield' ); ?>
			<?php wp_nav_menu( 'theme_location=products' ); ?>
			<?php wp_nav_menu( 'theme_location=exhibitor' ); ?>
		</div>
	</nav>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

function ifield_wishlist_widget() {

	ob_start();

	?>

	<nav class="ifield-wishlist ifield-widget-menu">
		<div class="ifield-widget-menu-wrapper">
			<button>
				<img alt="wishlist" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/heart.svg" width="46" height="46">
				<span class="ifield-wishlist-counter">0</span>
			</button>
		</div>
	</nav>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

function ifield_sakatito_widget() {

	ob_start();
	
	$whatsapp_text = __( 'I need more information', 'sakataifield' ); 

	$salesagent = sakataifield()->wishlist->get_salesagent_data();

	$expert_link = 'https://api.whatsapp.com/send?phone=' . $salesagent['whatsapp'] . '&text=' . rawurlencode( $whatsapp_text );

	?>

	<nav class="ifield-sakatito ifield-widget-menu">
		<div class="ifield-widget-menu-wrapper">
			<div class="ifield-sakatito-options">
				<span class="sakatito-message"><?php _e( 'Get help', 'hello-sakataifield' ); ?></span>
				<ul class="sakatito-menu">
					<li class="">
						<a href="<?= $expert_link ?>" target="_blank" class=""><?= __( 'Talk to an expert', 'hello-sakataifield' ); ?></a>
					</li>
					<li class="">
						<a href="#" class="onboarding-link"><?= __( 'Watch tutorial again', 'hello-sakataifield' ); ?></a>
					</li>
					<li class="">
						<a href="#" class="faq-link"><?= __( 'Frequently asked questions', 'hello-sakataifield' ); ?></a>
					</li>
					<li class="">
						<a href="#" class="close-sakatito-link"><?= __( 'Close', 'hello-sakataifield' ); ?></a>
					</li>
				</ul>
			</div>
			<button id="the-sakatito">
				<span class="ifield-sakatito-img-wrapper">
					<img alt="sakatito" src="<?= bloginfo( 'stylesheet_directory' ) ?>/assets/img/sakatito.png" width="100" height="">
				</span>
			</button>
		</div>
	</nav>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}

function tv_ifield_load_page() {

	ob_start();

	?>
	<div class="bg-page-ifieldtv">
		<div id=img-load-ifieldtv class="img-logo-ifieldtv">
			<img  alt="tvifield logo" src="<?php bloginfo( 'stylesheet_directory' ) ?>/assets/img/logo-tv-ifield.png">
		</div>
	</div>
	<div class="footer-tvifield">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-12">
					<div class="logo-sakata">
						<img alt="logo sakata" class="img-logo-footer" src="<?php bloginfo( 'stylesheet_directory' ) ?>/assets/img/sakata-logo.png">
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 

	$html = ob_get_contents();

	ob_end_clean();

	return $html;

}



function get_sku_distributors_json() {

	$json = '{"H.1.4.2307":["16385","11542","1499","7236","10344","10343","2021"],"H.1.4.2214":["16385","11542","1499","7236","2021"],"H.1.1.957":["16385","13138","11374","1499","7236","10344","12522","10343","2021"],"H.1.1.990":["16385","13138","11542","1499","7236","10344","10343","2021"],"H.1.2.1201":["16385"],"H.1.4.2408":["16385"],"H.1.2.1225":["16385","13138","11542","1499","7236","10344","10343","2021"],"H.1.1.1991":["16385","13138","11374","1499","7236","10344","12522","10343","2021"],"H.1.2.1220":["16385","13138","11542"],"H.3.1.2096":["16385","13138","11542"],"H.3.1.2097":["16385","11542","7236","12522","2021"],"H.3.1.2135":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.1988":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.2117":["16385","13138","11374","11542","1499","7236","10344","10343","2021"],"H.3.1.2098":["16385","11374","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.2146":["16385","11374","11542","1499","7236","12522","2021"],"H.3.1.2081":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.7.1.120":["16385","13138","11542","1499","7236","10344","12522","10343","2021"],"H.8.1.1683":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.11.1.1992":["16385","13138","11374","11542","1499","7236","2021"],"H.14.1.1189":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.14.1.1204":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.14.1.1211":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.16.1.1376":["16385","13138","11374","1499","7236","10344","12522","10343","2021"],"H.16.1.1358":["16385","7236","2021"],"H.16.1.1356":["16385","13138","11542","7236","12522","2021"],"H.23.1.2014":["16385","13138","11542","1499","7236","10344","12522","10343","2021"],"H.23.1.1989":["16385","11542","1499"],"H.23.1.1938":["16385","13138","11542","1499","7236","2021"],"H.28.1.2003":["16385","13138","11374","11542","7236","10344","12522","10343","2021"],"H.28.1.810":["16385","11542"],"H.29.12.3":["16385","13138","11374","11542","1499","7236","10344","10343","2021"],"H.30.1.2340":["16385","13138","11542","1499","7236","10344","10343","2021"],"H.30.1.2435":["16385","13138","11374","11542","1499","7236","10344","10343","2021"],"H.30.1.2341":["16385","13138","11374","11542","1499","7236","10344","10343","2021"],"H.34.1.1943":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.34.1.992":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.307":["16385","13138","11374","11542","7236","17635","2021"],"H.37.2.2379":["16385","13138","11374","11542","7236","12522","2021"],"H.37.10.341":["16385","13138","11374","11542","1499","7236","12522","2021"],"H.37.10.323":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.2400":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.333":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.2.2370":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.2403":["16385","13138","11542","7236","12522","2021"],"H.37.10.303":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.2115":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.2157":["16385","13138","11374","11542","7236","10344","12522","10343","2021"],"H.30.1.2308":["16385","13138","11374","11542","1499","7236","10344","10343","2021"],"H.30.1.2211":["16385","13138","11542","7236","10344","10343","2021"],"H.30.1.2263":["16385","13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.30.1.2291":["16385","13138","11542","10344","10343"],"H.32.1.1864":["16385","16386","16387","11542","1499","7236","10344","10343","2021"],"H.1.1.1913":["13138","11542","7236","10344","10343","2021"],"H.3.1.2095":["13138","11374","11542","1499","7236","10344","10343","2021"],"H.3.1.2023":["13138","11542","1499","7236","10344","12522","10343","2021"],"H.3.1.2066":["13138","11374","11542","1499","7236","10344","10343","2021"],"H.3.1.1960":["13138","11374","11542","7236","10344","10343","2021"],"H.3.1.2032":["13138","11374","11542","1499","7236","10344","10343","2021"],"H.3.1.2084":["13138","11542","1499"],"H.3.1.1982":["13138","11374","11542","1499","7236","10344","10343","2021"],"H.3.1.1133":["13138","11542","1499","7236","10344","10343","2021"],"H.10.1.1866":["13138","11542","7236","10344","12522","10343","2021"],"H.10.1.1864":["13138","11542","7236","10344","12522","10343","2021"],"H.13.1.1890":["13138","11542","7236","12522","2021"],"H.14.1.1221":["13138","11542","7236","10344","10343","2021"],"H.14.1.1197":["13138","11374","11542","7236","10344","10343","2021"],"H.14.1.1216":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.15.3.13":["13138","11542"],"H.15.3.11":["13138","11542"],"H.15.2.18":["13138","11374","11542"],"H.16.1.1287":["13138","11542","7236","2021"],"H.23.1.1953":["13138","11374","7236","10344","12522","10343","2021"],"H.24.1.2109":["13138","11542","7236","10344","12522","10343","2021"],"H.24.1.2107":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.24.1.2153":["13138","11374","1499","7236","10344","12522","10343","2021"],"H.24.1.2396":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.30.1.2203":["13138","11542","7236","10344","10343","2021"],"H.30.1.2328":["13138","11542","7236","10344","10343","2021"],"H.30.1.2106":["13138","11542","7236","2021"],"H.30.1.2110":["13138","11374","11542"],"H.30.1.1984":["13138","11542","7236","10344","10343","2021"],"H.30.1.2272":["13138","11542","7236","10344","10343","2021"],"H.30.1.2357":["13138","11542","7236","10344","10343","2021"],"H.34.1.1941":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.301":["13138","11374","11542","7236","2021"],"H.37.2.2232":["13138","11374","11542","7236","12522","2021"],"H.37.2.2040":["13138","11374","11542","7236","2021"],"H.37.2.2068":["13138","11542"],"H.37.10.184":["13138","11542","7236","12522","2021"],"H.37.10.86":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.2.2229":["13138","11542","7236","10344","12522","10343","2021"],"H.37.10.279":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.85":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.37.10.183":["13138","11374","11542","1499","7236","10344","12522","10343","2021"],"H.1.2.1224":["13138","11542","1499","7236","10344","10343","2021"],"H.3.1.2151":["11374"],"H.3.1.2126":["11374","11542","1499","7236","12522","2021"],"H.3.1.2144":["11374","11542","1499","7236","2021"],"H.3.1.2082":["11374","11542","1499","7236","10344","10343","2021"],"H.3.1.2014":["11374","11542"],"H.3.1.2142":["11374","11542","1499","7236","10344","10343","2021"],"H.16.1.1387":["11374"],"H.24.1.2169":["11374","7236","10344","12522","10343","2021"],"H.24.1.2250":["11374","1499","7236","10344","12522","10343","2021"],"H.37.10.2436":["11374"],"H.1.4.2249":["11374","12522"],"H.1.4.1994":["11542","1499","7236","2021"],"H.1.2.804":["11542"],"H.1.4.1686":["11542","7236","10344","10343","2021"],"H.3.1.75":["11542"],"H.13.1.1891":["11542","7236","10344","12522","10343","2021"],"H.13.1.1255":["11542","7236","10344","12522","10343","2021"],"H.16.1.1280":["11542"],"H.16.1.1256":["11542"],"H.28.1.1996":["11542","7236","2021"],"H.29.1.1851":["11542"],"H.30.1.800":["11542","7236","10344","10343","2021"],"H.32.1.752":["11542","10344","10343"],"H.34.1.806":["11542"],"H.35.1.466":["11542","1499"],"H.35.1.464":["11542"],"H.36.1.1926":["11542","1499","7236","10344","12522","10343","2021"],"H.37.2.2263":["11542","7236","2021"],"H.37.10.320":["11542","7236","12522","2021"],"H.37.2.2224":["11542","7236","2021"],"H.37.2.2353":["11542","1499","7236","10344","10343","2021"],"H.37.10.319":["11542","7236","10344","10343","2021"],"H.37.2.2131":["11542"],"H.37.10.18":["11542","1499","7236","10344","12522","10343","2021"],"H.37.10.220":["11542","7236","10344","10343","2021"],"H.37.10.364":["11542","7236","12522","2021"],"H.1.2.788":["1499"],"H.2.1.44":["1499"],"H.3.1.2038":["1499"],"H.15.3.14":["1499"],"H.27.2.352":["1499"],"H.1.4.2051":["7236","10344","10343","2021"],"H.3.1.2087":["7236","12522","2021"],"H.16.1.1332":["7236","2021"],"H.30.1.2033":["7236","2021"],"H.37.10.1997":["7236","10344","10343","2021"],"H.1.4.2120":["7236","10344","10343","2021"],"H.19.1.1926":["10344","10343"],"H.29.1.1900":["17635"],"H.29.1.1895":["17635"],"H.29.1.1896":["17635"],"H.3.1.2028":["7236"],"H.3.1.2025":["7236"],"H.9.1.1968":["7236"],"H.9.1.2055":["7236"],"H.23.1.2009":["7236"],"H.1.1.2064":["7236"]}';

	return json_decode( $json, true );

}