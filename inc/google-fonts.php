<?php

function sakataifield_font_families() {
	$font_families = array(
		'sans-serif' => array(
			'name'    => __( 'Sans serif', 'sakataifield' ),
			'generic' => 'sans-serif',
		),
		'serif'      => array(
			'name'    => __( 'Serif', 'sakataifield' ),
			'generic' => 'serif',
		),
		'monospace'      => array(
			'name'    => __( 'Monospace', 'sakataifield' ),
			'generic' => 'monospace',
		),
		'montserrat'    => array(
			'name'    => 'Montserrat',
			'generic' => 'sans-serif',
			'styles'  => array(
				array(
					'weight' => 400,
					'name'   => __( 'Regular', 'sakataifield' ),
				),
				array(
					'weight' => 500,
					'name'   => __( 'Medium', 'sakataifield' ),
				),
				array(
					'weight' => 600,
					'name'   => __( 'Semi-bold', 'sakataifield' ),
				),
			),
		),
		'poppins'    => array(
			'name'    => 'Poppins',
			'generic' => 'sans-serif',
			'styles'  => array(
				array(
					'weight' => 400,
					'name'   => __( 'Regular', 'sakataifield' ),
				),
				array(
					'weight' => 500,
					'name'   => __( 'Medium', 'sakataifield' ),
				),
				array(
					'weight' => 600,
					'name'   => __( 'Semi-bold', 'sakataifield' ),
				),
			),
		),
		'inria-serif'    => array(
			'name'    => 'Inria Serif',
			'generic' => 'serif',
			'styles'  => array(
				array(
					'weight' => 700,
					'name'   => __( 'Bold', 'sakataifield' ),
					'italic' => true,
				),
			),
		),
		'lekton'     => array(
			'name'    => 'Lekton',
			'generic' => 'monospace',
			'styles'  => array(
				array(
					'weight' => 400,
					'name'   => __( 'Regular', 'sakataifield' ),
				),
				array(
					'weight' => 700,
					'name'   => __( 'Bold', 'sakataifield' ),
				),
			),
		),
	);
	return $font_families;
}

/**
 * Enqueue style font.
 */
function sakataifield_style_font() {
	if ( function_exists( 'sakataifield_google_fonts_url' ) ) {
		$google_fonts_url = sakataifield_google_fonts_url();
		if ( $google_fonts_url ) {
			wp_enqueue_style( 'google-fonts', sakataifield_google_fonts_url(), array(), null );
		}
	}

}
add_action( 'wp_enqueue_scripts', 'sakataifield_style_font', 9 );

function sakataifield_google_fonts_url() {
	$url = false;

	$fonts = array(
		'poppins',
		'montserrat',
		'inria-serif',
		'lekton',
	);

	$google_fonts = array();
	if ( function_exists( 'sakataifield_font_families' ) ) {
		$font_families = sakataifield_font_families();
		foreach ( $fonts as $font ) {
			if ( isset( $font_families[ $font ] ) ) {
				$font_family = $font_families[ $font ];

				$slug = sanitize_title( $font_family['name'] );
				$name = preg_replace( '/\s+/', '+', $font_family['name'] );

				$styles = array();
				foreach ( $font_family['styles'] as $style ) {
					$italic = isset( $style['italic'] ) && $style['italic'];

					$styles[] = $style['weight'] . ( $italic ? 'italic' : '' );
				}

				$google_fonts[ $slug ] = $name . ':' . implode( ',', $styles );
			}
		}
	}

	foreach ( $google_fonts as $slug => &$google_font ) {
		list( $name, $styles ) = explode( ':', $google_font );
		$styles = (array) explode( ',', $styles );
		$normal = array();
		$has_italic = false;

		$normal = array();
		$italic = array();
		foreach ( $styles as $style ) {
			$is_italic = preg_match( '/italic$/', $style );
			if ( ! $is_italic ) {
				$normal[] = '0,' . $style;
			} else {
				if ( ! $has_italic ) {
					$has_italic = true;
				}
				$style = preg_replace( '/italic$/', '', $style );

				$italic[] = '1,' . $style;
			}
		}
		if ( ! $has_italic ) {
			foreach ( $normal as $i => $style ) {
				$normal[ $i ] = preg_replace( '/^0,/', '', $style );
			}
		}
		$style_param = array_filter(
			array_merge(
				$normal,
				$italic
			)
		);
		$font_param  = $name . ':';
		if ( $has_italic ) {
			$font_param .= 'ital,';
		}
		$font_param .= 'wght@' . implode( ';', $style_param );
		$google_font = 'family=' . $font_param;
	}

	if ( $google_fonts ) {
		$url = sprintf(
			'https://fonts.googleapis.com/css2?%s&display=swap',
			implode( '&', array_values( $google_fonts ) )
		);
	}

	return $url;
}

function sakataifield_google_fonts_clean_url( $good_protocol_url ) {
	$is_google_fonts_css2 = preg_match(
		'#^https://fonts.googleapis.com/css2#',
		$good_protocol_url
	);
	if ( $is_google_fonts_css2 ) {
		$good_protocol_url = preg_replace(
			'/(family)%5B\d+%5D/',
			'$1',
			$good_protocol_url
		);
	}
	return $good_protocol_url;
}
add_filter( 'clean_url', 'sakataifield_google_fonts_clean_url' );
