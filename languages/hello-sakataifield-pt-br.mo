��    H      \  a   �            !     '     6     ?     O     V     v     �     �     �  -   �  @   �  .   <     k  
   q     |  !   �     �     �     �  	   �     �     �     �                )  (   =  2   f     �  "   �     �     �      	     %	     ?	  
   R	     ]	     e	     k	     o	     {	     �	     �	     �	     �	     �	     �	     �	     �	  %   
     7
     N
     \
     m
     t
     z
     �
     �
     �
     �
     �
  K   �
     9     N  	   ^     h     {  "   �  )   �     �  �   �     �     �     �             1   '     Y     t     �     �  0   �  B   �  4   *     _  	   f     p  "   �     �     �     �     �     �       
             1     =  $   N  =   s     �  ,   �     �       '   ,     T     m     �     �     �     �     �     �     �     �     �     �          +  (   :  )   c      �     �     �     �     �     �        !        1     J     i     �  Q   �      �       	   $     .     C  ,   P  7   }     �            6   /   %   G            @   $       3   &                 >      +   .      ;       =   2   	       A                      7   5       ?      C          '       B                  
   #      <                   (   -      9   8                :          )   H          0         !   ,   F   "   E      4       1   *                          D       About Access my list Activate Add to wishlist Agenda All set, click to start, enjoy! Are you still have questions? Browse our products Cancel scheduling Change preferences Check out more information about our products Check out some products of your interest with special conditions Click to see the schedule of upcoming lectures Close Deactivate Discover our products Don't forget to submit your list! Enter Exciting song Exhibitor menu Farm song Field settings Filtering by For you Frequently asked questions Get help I want to know more I want to know more about this product:  I want to know more about your demo field products I want to talk to expert In your wishlist. Click to remove. Interactive menu Learn more about this product Lectures that might interest you Let me know when to start Loading experience Loading... Log Out Music New Next events Nothing to show On view now Previous episodes Products Products menu Products used in this field Remove filter Scheduled lectures Scheduled lectures: See what we've got especially for you See your interest list Sound effects Sounds of nature Stands Start Station menu Stay on top of the schedule Talk to an expert Talk to an expert now Talk to expert Talk to the expert This is just a preview. Some aspects can be different in the final context. Visit our demo field Visit our stand Watch now Watch our lectures Watch tutorial again You can change this settings later You must be logged in to see this content You will be talking to: Project-Id-Version: WPML_EXPORT_hello-sakataifield
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
 Sobre Acessar minha lista Ativar Adicionar a lista de desejos Agenda Tudo pronto, clique no botão abaixo para iniciar Você ainda tem perguntas? Navegue pelos produtos Cancelar agendamento Alterar preferências Confira mais informações sobre nossos produtos Confira alguns produtos de seu interesse com condições especiais Clique para ver o calendário dos próximos webinars Fechar Desativar Descubra nossos produtos Não esqueça de enviar sua lista! Entrar Música animada Menu de expositores Música da fazenda Configurações do campo Filtrando por Para você Perguntas frequentes Obter ajuda Quero saber mais Quero saber mais sobre esse produto: Quero saber mais sobre os produtos do campo de demonstração Quero falar com um especialista Adicionado a sua lista. Clique para remover. Menu interativo Saiba mais sobre este produto Webinars que podem ser do seu interesse Me avise quando começar Carregando experiência Carregando... Sair Música Novidade Próximos eventos Nada para exibir Assistindo agora Webinars anteriores Produtos Menu de produtos Produtos utilizados neste campo Remover filtro Webinars que você agendou para assistir Webinars que você agendou para assistir: Veja o que preparamos para você Veja sua lista de interesses Efeitos sonoros Sons da natureza Expositores Iniciar Menu estação Fique por dentro da programação Falar com o especialista Fale com um especialista agora Falar com o especialista Falar com o especialista Este é apenas um preview. Alguns aspectos podem ser diferentes na versão final. Visite nosso campo demonstrativo Visitar nosso estande Ver agora Veja nossos webinars Ver tutorial Você pode alterar as configurações depois Você deve estar logado para visualizar este conteúdo. Você irá falar com: 