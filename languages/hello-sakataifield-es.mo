��    H      \  a   �            !     '     6     ?     O     V     v     �     �     �  -   �  @   �  .   <     k  
   q     |  !   �     �     �     �  	   �     �     �     �                )  (   =  2   f     �  "   �     �     �      	     %	     ?	  
   R	     ]	     e	     k	     o	     {	     �	     �	     �	     �	     �	     �	     �	     �	  %   
     7
     N
     \
     m
     t
     z
     �
     �
     �
     �
     �
  K   �
     9     N  	   ^     h     {  "   �  )   �     �  �   �     �     �     �     �  
     9   '     a     z     �     �  3   �  D   �  6   )     `  
   g     r     �     �     �     �     �     �                    /     >  &   P  ?   w     �  -   �            &   4     [     s     �     �     �     �     �     �     �     �  	   �     �  "        +  (   9  )   b     �     �     �     �     �     �                <     R     m     �  [   �  %   �       	   0     :     P  ,   ]  ,   �     �            6   /   %   G            @   $       3   &                 >      +   .      ;       =   2   	       A                      7   5       ?      C          '       B                  
   #      <                   (   -      9   8                :          )   H          0         !   ,   F   "   E      4       1   *                          D       About Access my list Activate Add to wishlist Agenda All set, click to start, enjoy! Are you still have questions? Browse our products Cancel scheduling Change preferences Check out more information about our products Check out some products of your interest with special conditions Click to see the schedule of upcoming lectures Close Deactivate Discover our products Don't forget to submit your list! Enter Exciting song Exhibitor menu Farm song Field settings Filtering by For you Frequently asked questions Get help I want to know more I want to know more about this product:  I want to know more about your demo field products I want to talk to expert In your wishlist. Click to remove. Interactive menu Learn more about this product Lectures that might interest you Let me know when to start Loading experience Loading... Log Out Music New Next events Nothing to show On view now Previous episodes Products Products menu Products used in this field Remove filter Scheduled lectures Scheduled lectures: See what we've got especially for you See your interest list Sound effects Sounds of nature Stands Start Station menu Stay on top of the schedule Talk to an expert Talk to an expert now Talk to expert Talk to the expert This is just a preview. Some aspects can be different in the final context. Visit our demo field Visit our stand Watch now Watch our lectures Watch tutorial again You can change this settings later You must be logged in to see this content You will be talking to: Project-Id-Version: WPML_EXPORT_hello-sakataifield
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0.1
 Sobre Accede a mi lista Activar Añadir a la lista de deseos Calendario Todo listo, haga clic en el botón de abajo para comenzar ¿Aún tienes preguntas? Explorar productos Cancelar cita Cambiar preferencias Consulta más información sobre nuestros productos Consulta algunos productos de tu interés con condiciones especiales Haga clic para ver el calendario de próximos webinars Cerrar Desactivar Descubre nuestros productos ¡No olvides enviar tu lista! Iniciar sesión Musica animada Menú expositor Musica de granja Configuración de campo Filtrando por Para ti Preguntas frecuentes Consigue ayuda Quiero saber más Quiero saber más sobre este producto: Quiero saber más sobre sus productos de campo de demostración Quiero hablar con un experto Añadido a tu lista. Haga clic para eliminar. Menú interactivo Conozca mas sobre este producto Webinars que pueden ser de tu interés Avísame cuando empiece Cargando interactivo Cargando... Salir Musica Nuevo Próximos eventos Nada que mostrar Viendo ahora Webinars anteriores Productos Menú de productos Productos utilizados en este campo Quitar filtro Webinars a las que ha programado asistir Webinars a las que ha programado asistir: Mira lo que preparamos para ti 
Ver tu lista de intereses
 Efectos sonoros Sonidos de la naturaleza Expositores Comenzar Menú de la estación Manténgase al tanto del horario Hablar con un experto Habla con un experto ahora Hablar con un experto Hablar con el experto Esta es solo una vista previa. Algunos aspectos pueden ser diferentes en la versión final. Visite nuestro campo de demostración Visita nuestro stand Ver ahora Vea nuestros webinars Ver tutorial Puedes cambiar la configuración más tarde. Debes estar logueado para ver este contenido Hablarás con: 